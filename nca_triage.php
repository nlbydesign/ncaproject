<?php
    session_start();
    require_once('getdata.php');
    require_once('header.php');

    if(!isset($_SESSION['userlogin'])){
        header("Location: login.php");
    }

?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/vendor/fonts/circular-std/style.css" >
    <link rel="stylesheet" href="assets/libs/css/style.css">
    <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="assets/vendor/vector-map/jqvmap.css">
    <link rel="stylesheet" href="assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
    <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">

    
    <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <title>National Circuit Assembly Dashboard</title>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <?php header1(); ?>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        
        <?php leftnav(); ?>
                
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
                <!-- ============================================================== -->
                <!-- pagehader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h3 class="mb-2">Theatro Dashboard</h3>
                            <p class="pageheader-text"></p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Triage</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- pagehader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <span class="hdr1">Total Triage Units:  </span>
                    <span class="hdr2"><?php echo getTotTriageCount();?></span>
                </div>
                <div class="row">
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                        <div class="chart-container">
                            <canvas id="chartjs_triage"></canvas>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- metric -->
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <h5 class="text-muted">Passed</h5>
                                </div>
                                <div class="row">
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1 text-primary"><?php echo getTriage('status','PASSED');?></h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="metric-label d-inline-block float-right text-success">
                                        <!--<i class="fa fa-fw fa-caret-up"></i>--><span id="perc_Good"><?php echo number_format((getTriage('status','PASSED')/gettotrows('battery_triage')*100), 2, '.','').'%';?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /. metric -->
                    <!-- metric -->
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <h5 class="text-muted">Switch 1</h5>
                                </div>
                                <div class="row">
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1 text-primary"><?php echo getTriage('status','SW1');?></h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="metric-label d-inline-block float-right text-success">
                                        <!--<i class="fa fa-fw fa-caret-up"></i>--><span id="perc_Swollen"><?php echo number_format((getTriage('status','SW1')/gettotrows('battery_triage')*100), 2, '.','').'%';?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /. metric -->
                    <!-- metric -->
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <h5 class="text-muted">Switch 2</h5>
                                </div>
                                <div class="row">
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1 text-primary"><?php echo getTriage('status','SW2');?></h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="metric-label d-inline-block float-right text-success">
                                        <!--<i class="fa fa-fw fa-caret-up"></i>--><span id="perc_Punctured"><?php echo number_format((getTriage('status','SW2')/gettotrows('battery_triage')*100), 2, '.','').'%';?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /. metric -->
                    <!-- metric -->
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <h5 class="text-muted">Switch 3</h5>
                                </div>
                                <div class="row">
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1 text-primary"><?php echo getTriage('status','SW3');?></h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="metric-label d-inline-block float-right text-success">
                                        <!--<i class="fa fa-fw fa-caret-up"></i>--><span id="perc_Wrinkled"><?php echo number_format((getTriage('status','SW3')/gettotrows('battery_triage')*100), 2, '.','').'%';?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /. metric -->
                    <!-- metric -->
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <h5 class="text-muted">Switch 4</h5>
                                </div>
                                <div class="row">
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1 text-primary"><?php echo getTriage('status','SW4');?></h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="metric-label d-inline-block float-right text-success">
                                        <!--<i class="fa fa-fw fa-caret-up"></i>--><span id="perc_2Yrs"><?php echo number_format((getTriage('status','SW4')/gettotrows('battery_triage')*100), 2, '.','').'%';?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /. metric -->
                    <!-- metric -->
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <h5 class="text-muted">Switch 2 & 4</h5>
                                </div>
                                <div class="row">
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1 text-primary"><?php echo getTriage('status','SW2/SW4');?></h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="metric-label d-inline-block float-right text-success">
                                        <!--<i class="fa fa-fw fa-caret-down"></i>--><span id="perc_BrokenWire"><?php echo number_format((getTriage('status','SW2/SW4')/gettotrows('battery_triage')*100), 2, '.','').'%';?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /. metric -->
                    <!-- metric -->
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <h5 class="text-muted">Audio</h5>
                                </div>
                                <div class="row">
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1 text-primary"><?php echo getTriage('status','AUDIO');?></h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="metric-label d-inline-block float-right text-success">
                                    <!-- use text-danger to turn span text red -->
                                        <!--<i class="fa fa-fw fa-caret-down"></i>--><span id="perc_NoBattery"><?php echo number_format((getTriage('status','AUDIO')/gettotrows('battery_triage')*100), 2, '.','').'%';?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /. metric -->
                    <!-- metric -->
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <h5 class="text-muted">LED</h5>
                                </div>
                                <div class="row">
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1 text-primary"><?php echo getTriage('status','LED');?></h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="metric-label d-inline-block float-right text-success">
                                    <!-- use text-danger to turn span text red -->
                                        <!--<i class="fa fa-fw fa-caret-down"></i>--><span id="perc_NoBattery"><?php echo number_format((getTriage('status','LED')/gettotrows('battery_triage')*100), 2, '.','').'%';?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /. metric -->
                    <!-- metric -->
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <h5 class="text-muted">Power</h5>
                                </div>
                                <div class="row">
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1 text-primary"><?php echo getTriage('status','POWER');?></h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="metric-label d-inline-block float-right text-success">
                                        <!--<i class="fa fa-fw fa-caret-up"></i>--><span id="perc_CannotCharge"><?php echo number_format((getTriage('status','POWER')/gettotrows('battery_triage')*100), 2, '.','').'%';?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /. metric -->
                    <!-- metric -->
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <h5 class="text-muted">Flashing Red</h5>
                                </div>
                                <div class="row">
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1 text-primary"><?php echo getTriage('status','FLR');?></h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="metric-label d-inline-block float-right text-success">
                                        <!--<i class="fa fa-fw fa-caret-up"></i>--><span id="perc_CannotCharge"><?php echo number_format((getTriage('status','FLR')/gettotrows('battery_triage')*100), 2, '.','').'%';?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /. metric -->
                    <!-- metric -->
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <h5 class="text-muted">Battery</h5>
                                </div>
                                <div class="row">
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1 text-primary"><?php echo getTriage('status','BAT');?></h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="metric-label d-inline-block float-right text-success">
                                        <!--<i class="fa fa-fw fa-caret-up"></i>--><span id="perc_CannotCharge"><?php echo number_format((getTriage('status','BAT')/gettotrows('battery_triage')*100), 2, '.','').'%';?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /. metric -->
                    <!-- metric -->
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <h5 class="text-muted">WIFI</h5>
                                </div>
                                <div class="row">
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1 text-primary"><?php echo getTriage('status','WIFI');?></h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="metric-label d-inline-block float-right text-success">
                                        <!--<i class="fa fa-fw fa-caret-up"></i>--><span id="perc_CannotCharge"><?php echo number_format((getTriage('status','WIFI')/gettotrows('battery_triage')*100), 2, '.','').'%';?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /. metric -->
                    <!-- metric -->
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <h5 class="text-muted">Fault Found</h5>
                                </div>
                                <div class="row">
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1 text-primary"><?php echo getTriage('status','FF');?></h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="metric-label d-inline-block float-right text-success">
                                        <!--<i class="fa fa-fw fa-caret-up"></i>--><span id="perc_CannotCharge"><?php echo number_format((getTriage('status','FF')/gettotrows('battery_triage')*100), 2, '.','').'%';?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /. metric -->
                    <!-- metric -->
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <h5 class="text-muted">Flashing Green</h5>
                                </div>
                                <div class="row">
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1 text-primary"><?php echo getTriage('status','FLASH GREEN');?></h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="metric-label d-inline-block float-right text-success">
                                        <!--<i class="fa fa-fw fa-caret-up"></i>--><span id="perc_CannotCharge"><?php echo number_format((getTriage('status','Flashing Green')/gettotrows('battery_triage')*100), 2, '.','').'%';?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /. metric -->
                </div>
               
                </div>
                <div class="row">
                </div>
                <div class="row">
                    <div class="col-xl-5 col-lg-6 col-md-6 col-sm-12 col-12">
                        <!-- ============================================================== -->
                        <!-- social source  -->
                        <!-- ============================================================== -->
                        <div class="card">
                        
                        </div>
                        <!-- ============================================================== -->
                        <!-- end social source  -->
                        <!-- ============================================================== -->
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                        <!-- ============================================================== -->
                        <!-- sales traffice source  -->
                        <!-- ============================================================== -->
                        <div class="card">
                            
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end sales traffice source  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- sales traffic country source  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-3 col-lg-12 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            
                        </div>
                        <!-- ============================================================== -->
                        <!-- end sales traffice country source  -->
                        <!-- ============================================================== -->
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- footer -->
                <!-- ============================================================== -->
                <div class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                Copyright © 2020 National Circuit Assembly. All rights reserved.
                            </div>
                        
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end footer -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- end wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end main wrapper  -->
        <!-- ============================================================== -->
        <!-- Optional JavaScript -->
        <!-- jquery 3.3.1 js-->
        <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
        <!-- bootstrap bundle js-->
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
        <!-- slimscroll js-->
        <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
        <!-- chartjs js-->
        <script src="assets/vendor/charts/charts-bundle/Chart.bundle.js"></script>

        <!-- main js-->
        <script src="assets/libs/js/main-js.js"></script>
        <!-- jvactormap js-->
        <script src="assets/vendor/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
        <script src="assets/vendor/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- sparkline js-->
        <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
        <script src="assets/vendor/charts/sparkline/spark-js.js"></script>
        <!-- morris-chart js -->
        <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
        <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
        
        <!-- dashboard sales js-->
        <script src="assets/libs/js/dashboard-sales.js"></script>
        <script src="assets/libs/js/triagecharts.js"></script>
</body>

</html>