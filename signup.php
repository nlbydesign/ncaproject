<?php
    session_start();
    require_once('getdata.php');

    //if(isset($_POST["submit"])){
    //    $var = $_POST["productline"];
    //    $newvar = implode(",", $var);
    //}
?>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NCA - THEATRO TESTING</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" async defer></script>
        
        <script src="https://kit.fontawesome.com/1e6ad500ad.js" crossorigin="anonymous"></script>
        <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
    <body>
        <div class="container h-100">
            <div class="d-flex justify-content-center h-100">
                <div class="user_card2">
                    <div class="d-flex justify-content-center">
                        <div class="brand_logo_container">
                            <img src="img/nca_main_logo.png" class="brand_logo" alt="National Circuit Assembly Logo">
                        </div>
                    </div>
                    <div class="d-flex justify-content-center form_container">
                        <div class="col-md-10">
                            <label class="errorcode"></label>
                            <form>
                                <div class="input-group mb-3">
                                    <input type="text" name="empcode" id="empcode" class="form-control input_user" placeholder="Employee Code" required>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" name="fname" id="fname" class="form-control input_user" placeholder="First Name" style="margin-right:2px;" required>
                                
                                    <input type="text" name="lname" id="lname" class="form-control input_user" placeholder="Last Name" style="margin-left:2px;" required>
                                </div>
                                <!--<div class="input-group mb-3">
                                    <input type="password" name="position" id="position" class="form-control input_pass" placeholder="Position" required>
                                </div>-->
                                <div class="input-group mb-3">
                                    <input type="password" name="password" id="password" class="form-control input_user" placeholder="Password" style="margin-right:2px;" required>
                                    <input type="password" name="verifypassword" id="verifypassword" class="form-control input_user" placeholder="Verify Password" style="margin-left:2px;" required>
                                </div>
                                <div class="input-group mb-3 signupinput sulabel">
                                    <h5>Role</h5>
                                </div>
                                <div class="input-group mb-3">
                                    <select id="getemptype" name="getemptype" class="select-css">
                                        <option value="">--Select--</option>
                                        <option value="Admin">Administrative</option>
                                        <option value="employee">Production</option>
                                    </select>
                                </div>
                                <!-- <div class="input-group mb-3 signupinput sulabel">
                                    <h5>Product Line</h5>
                                </div> -->
                                <?php //getproductlines(); ?>
                                
                                <!--<div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="rememberme" class="custom-control-input" id="customControlInline">
                                        <label class="custom-control-label" for="customControlInline">Remember me</label>
                                    </div>
                                </div>-->
                                <div class="d-flex justify-content-center mt-3 login_container" style="padding-bottom: 25px;">
                                    <button type="button" name="button" id="signup" class="btn login_btn">
                                        SignUp
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function(){
                $('#signup').click(function(e){
                    
                    var valid = this.form.checkValidity();

                    var d = new Date(); 
                    var month = d.getMonth()+1;
                    var day = d.getDate();
                    
                    var datetime = d.getFullYear() + "-"  
                                + ((''+month).length<2 ? '0' : '') + month + "-" 
                                + ((''+day).lenth<2 ? '0' : '') + day + " "
                                + d.getHours() + ":"  
                                + d.getMinutes() + ":" 
                                + d.getSeconds();


                    
                    var empcode = $.trim($('#empcode').val());
                    var fname = $.trim($('#fname').val());
                    var lname = $.trim($('#lname').val());
                    var password = $.trim($('#password').val());
                    var verifypassword = $.trim($('#verifypassword').val());
                    var emptype = $.trim($('#getemptype').val());
                    var prodline = '';
                    var creationdate = datetime;
                    
                    console.log(prodline);
                    
                    if(empcode.length <1){
                        document.getElementById("empcode").focus();
                        $('#empcode').addClass( "inputerror" );
                        alert("Please Enter Your Employee Code");
                    }else if(fname.length <1){
                        document.getElementById("fname").focus();
                        $('#fname').addClass( "inputerror" );
                        alert("Please Enter Your First Name");
                    }else if(lname.length <1){
                        document.getElementById("lname").focus();
                        $('#lname').addClass( "inputerror" );
                        alert("Please Enter Your Last Name");
                    }else if(emptype.length <1){
                        $('#getemptype').addClass( "inputerror" );
                        alert("Please Select Employee Type");
                    }else if(password.length <1){
                        document.getElementById("password").focus();
                        $('#password').addClass( "inputerror" );
                        alert("Password Cannot Be Blank");
                    }else if(password != verifypassword){
                        $('#password').addClass( "inputerror" );
                        document.getElementById("verifypassword").focus();
                        $('#verifypassword').addClass( "inputerror" );
                        alert("Passwords Do Not Match");
                    }else{
                    
                        e.preventDefault();

                        $.ajax({
                            type: 'POST',
                            url: 'jssignup.php',
                            data: {
                                empcode: empcode, 
                                fname: fname, 
                                lname: lname, 
                                password: password,
                                verifypassword: verifypassword,
                                emptype: emptype,
                                creationdate: creationdate
                            },
                            success: function(data) {
                                alert(data);
                                window.location.href = "login.php";
                            },
                            error: function(data){
                                alert(data);
                            }
                        });
                    }
                });
            });
        </script>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    </body>
</html>