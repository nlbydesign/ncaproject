<?php
    require_once('config.php');
    
    $msg = "";

	$empcode=$_POST['empcode'];
	$fname=$_POST['fname'];
	$lname=$_POST['lname'];
    $password=$_POST['password'];
    $verifypassword=$_POST['verifypassword'];
    $emptype = $_POST['emptype'];
    $date=$_POST['creationdate'];
    
    $hash = password_hash($password, PASSWORD_BCRYPT);

    $sql = "SELECT * FROM users WHERE empcode = ?";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute([$empcode]);

    if($result){
        if ($password != $verifypassword){
            $msg = "Passwords do not match!";
            echo $msg;
        }else{
            $user = $stmtselect->fetch(PDO::FETCH_ASSOC);
            if($stmtselect->rowCount() > 0){
                $msg = "User Already Exist";
                echo $msg;
            }else{
                $insertsql = "INSERT INTO users (empcode, fname, lname, password, position, datecreated) VALUES (?, ?, ?, ?, ?, ?)";
                $stmt= $db->prepare($insertsql);
                $stmt->execute([$empcode, $fname, $lname, $hash, $emptype, $date]);
                $msg = "User Successfully Created!";
                echo $msg;
            } 
        }
    }else{
        $msg = 'There were errors connecting to the database.';
        echo $msg;
    }
    
?>
 