<?php
    session_start();
    require_once('getdata.php');
    require_once('header.php');

    if(!isset($_SESSION['userlogin'])){
        header("Location: login.php");
    }

    if(isset($_GET['logout'])){
        session_destroy();
        unset($_SESSION);
        header("Location: login.php");
    }
?>
    <!doctype html>
    <html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/libs/css/style.css">
        <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="assets/vendor/vector-map/jqvmap.css">
        <link href="assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/vendor/charts/chartist-bundle/chartist.css">
        <link rel="stylesheet" href="assets/vendor/charts/c3charts/c3.css">
        <link rel="stylesheet" href="assets/vendor/charts/morris-bundle/morris.css">
        <link rel="stylesheet" type="text/css" href="assets/vendor/daterangepicker/daterangepicker.css" />
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <title>National Circuit Assembly Dashboard</title>
    </head>

    <body>
        <!-- ============================================================== -->
        <!-- main wrapper -->
        <!-- ============================================================== -->
        <div class="dashboard-main-wrapper">
            <!-- ============================================================== -->
            <!-- navbar -->
            <!-- ============================================================== -->
            <?php header2(); ?>
            <!-- ============================================================== -->
            <!-- end navbar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- left sidebar -->
            <!-- ============================================================== -->
            <div class="nav-left-sidebar sidebar-dark">
                <div class="menu-list">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav flex-column">
                                <li class="nav-divider">
                                    DASHBOARD
                                </li>
                                <li class="nav-item ">
                                    <!-- <a class="nav-link active" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-1" aria-controls="submenu-1"><i class="fa fa-fw fa-user-circle"></i>Analytics<span class="badge badge-success">6</span></a> -->
                                    <a class="nav-link" href="nca_bi.php"><i class="fa fa-fw fa-battery-half"></i>Battery Inspection</a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link active" href="nca_rma.php"><i class="fa fa-fw fa-tag"></i>RMA</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="nca_export.php"><i class="fas fa-fw fa-table"></i>View/Export Data</a>
                                </li>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="nca_admin.php"><i class="fas fa-fw fa-user-circle"></i>Admin</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end left sidebar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- wrapper  -->
            <!-- ============================================================== -->
            <div class="dashboard-wrapper">
                <div class="container-fluid  dashboard-content">
                    <!-- ============================================================== -->
                    <!-- pagehader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h3 class="mb-2">Theatro Dashboard</h3>
                                <p class="pageheader-text"></p>
                                <div class="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Analytic Dashboard </li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- pagehader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <span class="hdr1">Total RMA Units:  </span>
                        <span class="hdr2"><?php echo getTotRMACount();?></span>
                    </div>
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">EBIT (Earnings Before Interest & Tax)</h5>
                                <div class="card-body">
                                    <div id="ebit_morris"></div>
                                    <div class="text-center">
                                        <span class="legend-item mr-3">
                                                <span class="fa-xs text-secondary mr-1 legend-tile"><i class="fa fa-fw fa-square-full"></i></span>
                                        <span class="legend-text">EBIT (Earnings Before Interest & Tax)</span>
                                        </span>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /. metric -->
                        <!-- metric -->
                    </div>
                    
                    <!-- ============================================================== -->
                    <!-- revenue  -->
                    <!-- ============================================================== 
                    <div class="row">
                        <div class="col-xl-8 col-lg-12 col-md-8 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Revenue</h5>
                                <div class="card-body">
                                    <canvas id="revenue" width="400" height="150"></canvas>
                                </div>
                                <div class="card-body border-top">
                                    <div class="row">
                                        <div class="offset-xl-1 col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 p-3">
                                            <h4> Today's Earning: $2,800.30</h4>
                                        </div>
                                        <div class="offset-xl-1 col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 p-3">
                                            <h2 class="font-weight-normal mb-3"><span>$48,325</span> </h2>
                                            <div class="mb-0 mt-3 legend-item">
                                                <span class="fa-xs text-primary mr-1 legend-title "><i class="fa fa-fw fa-square-full"></i></span>
                                                <span class="legend-text">Current Week</span></div>
                                        </div>
                                        <div class="offset-xl-1 col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 p-3">
                                            <h2 class="font-weight-normal mb-3">

                                                <span>$59,567</span>
                                            </h2>
                                            <div class="text-muted mb-0 mt-3 legend-item"> <span class="fa-xs text-secondary mr-1 legend-title"><i class="fa fa-fw fa-square-full"></i></span><span class="legend-text">Previous Week</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- ============================================================== -->
                        <!-- end reveune  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- total sale  -->
                        <!-- ============================================================== 
                        <div class="col-xl-4 col-lg-12 col-md-4 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Total Sale</h5>
                                <div class="card-body">
                                    <canvas id="total-sale" width="220" height="155"></canvas>
                                    <div class="chart-widget-list">
                                        <p>
                                            <span class="fa-xs text-primary mr-1 legend-title"><i class="fa fa-fw fa-square-full"></i></span><span class="legend-text"> Direct</span>
                                            <span class="float-right">$300.56</span>
                                        </p>
                                        <p>
                                            <span class="fa-xs text-secondary mr-1 legend-title"><i class="fa fa-fw fa-square-full"></i></span>
                                            <span class="legend-text">Affilliate</span>
                                            <span class="float-right">$135.18</span>
                                        </p>
                                        <p>
                                            <span class="fa-xs text-brand mr-1 legend-title"><i class="fa fa-fw fa-square-full"></i></span> <span class="legend-text">Sponsored</span>
                                            <span class="float-right">$48.96</span>
                                        </p>
                                        <p class="mb-0">
                                            <span class="fa-xs text-info mr-1 legend-title"><i class="fa fa-fw fa-square-full"></i></span> <span class="legend-text"> E-mail</span>
                                            <span class="float-right">$154.02</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- ============================================================== -->
                        <!-- end total sale  -->
                        <!-- ============================================================== -->
                    </div>
                    <div class="row">
                        <!-- ============================================================== -->
                        <!-- top selling products  -->
                        <!-- ============================================================== -->
                       
                        <!-- ============================================================== -->
                        <!-- end top selling products  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- revenue locations  -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- end revenue locations  -->
                        <!-- ============================================================== -->
                    </div>
                    <div class="row">
                        <div class="col-xl-5 col-lg-6 col-md-6 col-sm-12 col-12">
                            <!-- ============================================================== -->
                            <!-- social source  -->
                            <!-- ============================================================== -->
                            <div class="card">
                               
                            </div>
                            <!-- ============================================================== -->
                            <!-- end social source  -->
                            <!-- ============================================================== -->
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <!-- ============================================================== -->
                            <!-- sales traffice source  -->
                            <!-- ============================================================== -->
                            <div class="card">
                                
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end sales traffice source  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- sales traffic country source  -->
                        <!-- ============================================================== -->
                        <div class="col-xl-3 col-lg-12 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                
                            </div>
                            <!-- ============================================================== -->
                            <!-- end sales traffice country source  -->
                            <!-- ============================================================== -->
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- footer -->
                    <!-- ============================================================== -->
                    <div class="footer">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    Copyright © 2020 National Circuit Assembly. All rights reserved.
                                </div>
                            
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end footer -->
                    <!-- ============================================================== -->
                </div>
                <!-- ============================================================== -->
                <!-- end wrapper  -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- end main wrapper  -->
            <!-- ============================================================== -->
            <!-- Optional JavaScript -->
            <!-- jquery 3.3.1  -->
            <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
            <!-- bootstap bundle js -->
            <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
            <!-- slimscroll js -->
            <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
            <!-- chart chartist js -->
            <script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
            <script src="assets/vendor/charts/chartist-bundle/Chartistjs.js"></script>
            <script src="assets/vendor/charts/chartist-bundle/chartist-plugin-threshold.js"></script>
            <!-- chart C3 js -->
            <script src="assets/vendor/charts/c3charts/c3.min.js"></script>
            <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
            <!-- chartjs js -->
            <script src="assets/vendor/charts/charts-bundle/Chart.bundle.js"></script>
            <script src="assets/vendor/charts/charts-bundle/chartjs.js"></script>
            <!-- sparkline js -->
            <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
            <!-- dashboard finance js -->
            <script src="assets/libs/js/dashboard-shipping.js"></script>
            <!-- main js -->
            <script src="assets/libs/js/main-js.js"></script>
            <!-- gauge js -->
            <script src="assets/vendor/gauge/gauge.min.js"></script>
            <!-- morris js -->
            <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
            <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
            <script src="assets/vendor/charts/morris-bundle/morrisjs.html"></script>
            
            <!-- daterangepicker js -->
            <script src="../../../../cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
            <script src="../../../../cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
            <script>
                $(function() {
                    $('input[name="daterange"]').daterangepicker({
                        opens: 'left'
                    }, function(start, end, label) {
                        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                    });
                });
            </script>
    </body>

    </html>