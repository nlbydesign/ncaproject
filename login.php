<?php
    session_start();

    if(isset($_SESSION['userlogin'])){
        $_SESSION = header("Location: index.php");
    }

    if(isset($_GET['signup'])){
        header("Location: signup.php");
    }
    
?>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NCA - THEATRO ACCOUNT S</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" async defer></script>
        
        <script src="https://kit.fontawesome.com/1e6ad500ad.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
    <body>
        <div class="container h-100">
            <div class="d-flex justify-content-center h-100">
                <div class="login_card">
                    <div class="d-flex justify-content-center">
                        <div class="brand_logo_container">
                            <img src="img/nca_main_logo.png" class="brand_logo" alt="National Circuit Assembly Logo">
                        </div>
                    </div>
                    <div class="d-flex justify-content-center form_container">
                        <form>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="text" name="username" id="username" class="form-control input_user" required>
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                                </div>
                                <input type="password" name="password" id="password" class="form-control input_pass" required>
                            </div>
                            <!--<div class="input-group mb-2">
                                <select id="getlocation" name="getlocation" class="select-css">
                                    <option value="">--Select--</option>
                                    <option value="batteryinspection">Battery Inspection</option>
                                    <option value="triage">Triage</option>
                                    <option value="R121Repair">R121 Repair</option>
                                    <option value="rmaintakedata">RMA Intake</option>
                                    <option value="rmatriage">RMA Triage</option>
                                    <option value="shippingdata">Units to Ship</option>
                                    <option value="ncadashboard">Dashboard</option>
                                </select>
                            </div> -->
                            <div class="d-flex justify-content-center mt-3 login_container">
                                <button name="button" id="login" class="btn login_btn">
                                    Login
                                </button>
                            </div>
                        </form>
                    </div>
                   <!-- <div class="mt-4 logintext">
                        <div class="d-flex justify-content-center links">
                            Don't have an account? <a href="login.php?signup=true" class="ml-2">Sign Up</a>
                        </div>
                        <div class="d-flex justify-content-center">
                            <a href="#">Forgot your password?</a>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        <script>
        $( document ).ready(function() {
            
            $(function(){
                //$('#login').click(function(){
                $('form').submit(function(){    
                    //var valid = this.form.checkValidity();

                    
                    var username = $.trim($('#username').val());
                    var password = $.trim($('#password').val());
                    //var complocation = $.trim($('#getlocation').val());
                    

                    //e.preventDefault();

                    if(username.length <1){
                        document.getElementById("username").focus();
                        $('#username').addClass( "inputerror" );
                        alert("Please Enter Employee Badge Number");
                    }else if(password.length <1){
                        document.getElementById("password").focus();
                        $('#password').addClass( "inputerror" );
                        alert("Please Enter Your Password");
                    }else{

                        $.ajax({
                            type: 'POST',
                            url: 'jslogin.php',
                            data: {username: username, password: password},
                            success: function(data) {
                                var newdata = $.parseJSON(data);
                                var comp1 = "Theatro";
                                var comp2 = "UltraVision";
                                var prodline = $.trim(newdata['prodline']);
                                //alert(prodline);
                                if($.trim(newdata['position']) === "Super"){
                                    window.location.href = "nca_bi.php";
                                }else if($.trim(newdata['position']) === "Admin" && (prodline.indexOf(comp1) != -1)) {
                                    window.location.href = "nca_bi.php";
                                }else if($.trim(newdata['position']) === "employee") {
                                    //window.location.href = '../theatro/' + complocation + '.php';
                                    window.location.href = 'index.php';
                                }else{
                                    alert(newdata);
                                }
                            },
                            error: function(data){
                                alert('There was an issue logging in. Please check your Employee Code and Password.');
                            }
                        });
                    }
                });
            });
            
            window.onload = function() {
                document.getElementById("username").focus();
            };
        });    
        </script>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    </body>
</html>