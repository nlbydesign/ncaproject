<?php
    session_start();
    require_once('../config.php');

    $checkval = $_GET['pid']; 

    //$sql = "SELECT * FROM uvshippingdata ORDER BY id DESC LIMIT 500";
    $sql = "SELECT * FROM uvshippingdata WHERE palletID = ?";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute([$checkval]);
    $data1 = $stmtselect->fetch(PDO::FETCH_ASSOC);
    $table_data = array();
        if($stmtselect->rowCount() > 0){
            while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) {
                $date = date_create($rowitems['shippingdate']);
                $date = date_format($date,"m/d/Y");
                $table_data[] = array(
                    'Row ID' => $rowitems['id'],
                    'Pallet ID'  => $rowitems['palletID'],
                    'Shipping Date'   => $date,
                    'Serial Num'    => $rowitems['serialnumber'],
                    'User ID' => $rowitems['userid']
                );
            }
        }
    echo json_encode($table_data);
?>