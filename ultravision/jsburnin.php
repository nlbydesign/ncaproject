<?php
    session_start();
    require_once('../config.php');

    $serialnum  = strtoupper($_POST['serialnum']);
    $passed = $_POST['passed'];
    $creationdate = date('Y-m-d H:i:s');
    $client     ="UltraVision";
    $uid        =$_SESSION['userlogin'];

    $sql1 = "SELECT * FROM uvburnindata WHERE serialnumber = ? LIMIT 1";
    $stmtselect1 = $db->prepare($sql1);
    $result1 = $stmtselect1->execute([$serialnum]);
    $data1 = $stmtselect1->fetch(PDO::FETCH_ASSOC);
    if($stmtselect1->rowCount() > 0){
        $sql = "UPDATE uvburnindata SET bicomplete = ?, bicompleteuser = ?, pass = ? WHERE serialnumber = ?";
        $stmtselect = $db->prepare($sql);
        $stmtselect->execute([$creationdate, $uid, $passed, $serialnum]);
    }else{
        $passed = '';
        $insertsql = "INSERT INTO uvburnindata (serialnumber, bistart, bistartuser, client) VALUES (?, ?, ?, ?)";
        $stmt= $db->prepare($insertsql);
        $stmt->execute([$serialnum, $creationdate, $uid, $client]);
    }

?>