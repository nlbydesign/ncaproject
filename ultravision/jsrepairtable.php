<?php 
    session_start();
    require_once('../config.php');

    //$sql = "SELECT * FROM uvrepairdata ORDER BY dateinspected DESC LIMIT 500";
    $sql = "SELECT d.*, c.description as status FROM uvrepairdata as d LEFT JOIN uvrepaircodes as c ON d.statuscode = c.codeid ORDER BY dateinspected DESC";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();
    $table_data = array();
    if($stmtselect->rowCount() > 0){
        while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) {
            $sdate = date_create($rowitems['dateinspected']);
            $sdate = date_format($sdate,"m/d/Y");
            $table_data[] = array(
                'Row ID' => $rowitems['id'],
                'Serial Number' => $rowitems['serialnum'],
                'Status Code'  => $rowitems['status'],
                'Failed BurnIn' => $rowitems['failedBurnIn'],
                'Quantity' => $rowitems['qty'],
                'Date Entered'  => $sdate,
                'User ID' => $rowitems['user']
            );
        }
    }
    echo json_encode($table_data);
?>