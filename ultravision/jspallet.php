<?php
    session_start();
    require_once('../config.php');

    $wo = $_POST['wo'];
    $palletid  = $_POST['palletid'];
    $serialnum  = strtoupper($_POST['serialnum']);
    $stat = $_POST['unitstatus'];
    $status      = json_decode($stat);
    $creationdate=date('Y-m-d H:i:s');
    $client     ="UltraVision";
    $uid        =$_SESSION['userlogin'];

    if(empty($status)){
        $status = "{'choice':'Good'}";
    }
    for ($i=0, $len=count($status); $i<$len; $i++){
        $newstatus = $status[$i]->choice;
        $insertsql = "INSERT INTO uvpalletdata (wo, palletID, serialnum, status, datescanned, user, client) VALUES (?, ?, ?, ?, ?, ?, ?)";
        $stmt= $db->prepare($insertsql);
        $stmt->execute([$wo, $palletid, $serialnum, $newstatus, $creationdate, $uid, $client]);
    }
?>