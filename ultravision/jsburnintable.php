<?php
    session_start();
    require_once('../config.php');

    $sql = "SELECT * FROM uvburnindata ORDER BY bicomplete, bistart DESC";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();
    $table_data = array();
    
    if($stmtselect->rowCount() > 0){
        while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) {
            $sdate = date_create($rowitems['bistart']);
            $sdate = date_format($sdate,"m/d/Y");
            if($rowitems['bicomplete'] > ''){
                $cdate = date_create($rowitems['bicomplete']);
                $cdate = date_format($cdate,"m/d/Y");
            }else {
                $cdate = '';
            }
            $table_data[] = array(
                'Row ID' => $rowitems['id'],
                'Serial Number'  => $rowitems['serialnumber'],
                'Intake Date'   => $sdate,
                'Completion Date'   => $cdate,
                'Passed'    => $rowitems['pass']
            );
        }
    }
    echo json_encode($table_data);
?>