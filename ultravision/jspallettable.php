<?php 
    session_start();
    require_once('../config.php');

    $sql = "SELECT d.*, c.description as dsc FROM uvpalletdata as d LEFT JOIN uvrepaircodes as c ON d.status = c.codeid ORDER BY datescanned DESC";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();

    //$sql = "SELECT * FROM uvpalletdata ORDER BY palletID DESC LIMIT 500";
    //$stmtselect = $db->prepare($sql);
    //$result = $stmtselect->execute();
    $table_data = array();
    if($stmtselect->rowCount() > 0){
        while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) {
            $sdate = date_create($rowitems['datescanned']);
            $sdate = date_format($sdate,"m/d/Y");
            $table_data[] = array(
                'Row ID' => $rowitems['id'],
                'Work Order' => $rowitems['wo'],
                'Pallet ID'  => $rowitems['palletID'],
                'Date Scanned'  => $sdate,
                'Serial Number' => $rowitems['serialnum'],
                'Status'  => $rowitems['dsc'],
                'User ID' => $rowitems['user']
            );
        }
    }
    echo json_encode($table_data);
?>