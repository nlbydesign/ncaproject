<?php
    session_start();
    require_once('../config.php');

	$serialnum  = strtoupper($_POST['serialnum']);
    $creationdate = $_POST['creationdate'];
    $uid        = $_SESSION['userlogin'];
    $stat       = $_POST['poststatus'];
    $failval    = $_POST['failval'];
    $status      = json_decode($stat);
    
    
    
    for ($i=0, $len=count($status); $i<$len; $i++){
        $newstatus = $status[$i]->choice;
        $newqty = $status[$i]->qty;
        $sql = "SELECT * FROM uvrepaircodes WHERE description = ?";
        $stmtselect = $db->prepare($sql);
        $result = $stmtselect->execute([$newstatus]);
        if($stmtselect->rowCount() > 0){
            while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)){
                $code = $rowitems['codeid'];
                $insertsql = "INSERT INTO uvrepairdata (serialnum, statuscode, failedBurnIn, qty, dateinspected, user) VALUES (?, ?, ?, ?, ?, ?)";
                $stmt= $db->prepare($insertsql);
                $stmt->execute([$serialnum, $code, $failval, $newqty, $creationdate, $uid]);
            }
        }
        
    }
    
   

?>