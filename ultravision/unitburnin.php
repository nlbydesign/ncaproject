<?php
    session_start();
    require_once('../getdata.php');
    require_once('../header.php');

    if(!isset($_SESSION['userlogin'])){
        header("Location: ../login.php");
    }

?>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NCA - THEATRO DATA AQUISITION</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/normalize.css">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" async defer></script>
        
        <script src="https://kit.fontawesome.com/1e6ad500ad.js" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="../assets/vendor/fonts/circular-std/style.css" >
        <link rel="stylesheet" href="../assets/libs/css/style.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="../assets/vendor/vector-map/jqvmap.css">
        <link rel="stylesheet" href="../assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <!--<link href="DataTables/datatables.min.css" rel="stylesheet">
        <script type="text/javascript" src="DataTables/datatables.min.js"></script>-->
        <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <style>
            /* The container */
            .container {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-top: 12px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            
            }

            /* Hide the browser's default checkbox */
            .container input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
            
            }

            /* Create a custom checkbox */
            .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
            border-radius: 0.3rem 0.3rem 0.3rem 0.3rem !important;
            }

            /* On mouse-over, add a grey background color */
            .container:hover input ~ .checkmark {
            background-color: #ccc;
            }

            /* When the checkbox is checked, add a blue background */
            .container input:checked ~ .checkmark {
            background-color: #ee6534;
            }

            /* Create the checkmark/indicator (hidden when not checked) */
            .checkmark:after {
            content: "";
            position: absolute;
            display: none;
            }

            /* Show the checkmark when checked */
            .container input:checked ~ .checkmark:after {
            display: block;
            }

            /* Style the checkmark/indicator */
            .container .checkmark:after {
            left: 9px;
            top: 4px;
            width: 8px;
            height: 15px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
            
            }
        </style>
    </head>
    <body>
        <?php header2(); ?>
        <!-- BREADCRUM -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header" style="margin-top: 2%; padding-left: 25px;">
                    <h3 class="mb-2">UltraVision</h3>
                    <p class="pageheader-text"></p>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="../index.php" class="breadcrumb-link">UltraVision</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Burn In</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!------------------------------------------------------->
        <!----              MAIN PAGE AREA                  ----->
        <!------------------------------------------------------->
        <div id="page-container">
            <div id="content-wrap" class="container-fluid batinspect">
                    <div class="d-flex" style="margin-top:25px;"></div>
                    <form id="intakeform" autocomplete="off" style="margin-bottom: 20px;">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-9">
                                <div class="row justify-content-center">
                                    <div class="col-md-10 d-flex justify-content-center">
                                        <h1>BURN IN UNITS</h1>
                                    </div>
                                    <!--<div class="col-md-2">
                                        <div class="col-md-12">
                                            <div id="target" type="button">Reset</div>
                                        </div>
                                    </div>-->
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-4 sectiontype">
                                        <div class="row">
                                            <div class="col-md-12 sectionhdr">
                                                <label for="serialnum" class="d-flex justify-content-center inputlabel sectionhdrlabel">Serial Number</label>
                                            </div>
                                        </div>    
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="text" name="serialnum" id="serialnum" class="form-control input_user serialnuminput">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 d-flex justify-content-center">
                                                <div class='pgbtn3'>
                                                    <button id='submitrecord' name='submitrecord' class='submitrecord'>SUBMIT</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="ks-cboxtags" style="padding-top:60px; padding-bottom: 0px !important;">
                                                    <li style="width:200px;">
                                                        <input type="checkbox" id="pass" name="pass">
                                                        <label for="pass" >Passed Burn In?</label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>  
                <div class="row d-flex justify-content-center">
                    <div class="col-md-8">
                        <div id="shippedunits" class="table-responsive table-wrapper-scroll-y my-custom-scrollbar" >
                            <table id="burnintbl" class="table table-striped table-bordered" cellspacing="0" width="100%" >
                                <thead>
                                    <tr role="row">
                                        <th class="th-sm">Row ID</th>
                                        <th class="th-sm">Serial Number</th>
                                        <th class="th-sm">Intake Date</th>
                                        <th class="th-sm">Completion Date</th>
                                        <th class="th-sm">Passed</th>
                                    </tr>
                                </thead>
                                
                                <tfoot>
                                    <tr>
                                        <th>Row ID</th>
                                        <th>Serial Number</th>
                                        <th>Intake Date</th>
                                        <th>Completion Date</th>
                                        <th>Passed</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            Copyright © 2020 National Circuit Assembly. All rights reserved.
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!------------------------------------------------------->
        <!----            END MAIN PAGE AREA                ----->
        <!------------------------------------------------------->
        
        <script>
            $( document ).ready(function() {
                        
                $("#burnintbl").DataTable({
                    "destroy": true,
                    "scrollY"       : "400px",
                    "scrollCollapse": true,
                    "order"         : [[ 3, "desc" ],[2,"desc"]],
                    "pagingType"    : "first_last_numbers",
                    "ajax" : {
                        "url": '../ultravision/jsburnintable.php',
                        "dataSrc" : "" 
                    },
                    "columns" : [
                        {"data": "Row ID"},
                        {"data": "Serial Number"},
                        {"data": "Intake Date"},
                        {"data": "Completion Date"},
                        {"data": "Passed"}
                    ]
                });
                $('.dataTables_length').addClass('bs-select');
                
                
                document.getElementById("serialnum").focus();
                $('form input').on('keypress', function(e) {
                    return e.which !== 13;
                });
                
                $( "#serialnum" ).change(function() {
                    $('#serialnum').removeClass( "inputerror" );
                });

                //$('#pass').click(function() {
                    //if ($("#pass").is(":checked")) {
                            //var failval = $("#pass").val();
                       // }else{
                           // var failval = "NO";
                   // }
                    //alert(failval);
                //});


                $('form').submit(function(){
                    //if(e.which == 13){

                        //var valid = this.form.checkValidity();

                        if ($("#pass").is(":checked")) {
                                var failval = "YES";
                            }else{
                                var failval = "NO";
                        }

                        var d = new Date(); 
                        var month = d.getMonth()+1;
                        var day = d.getDate();
                        
                        var datetime = d.getFullYear() + "-"  
                                    + ((''+month).length<2 ? '0' : '') + month + "-" 
                                    + ((''+day).lenth<2 ? '0' : '') + day + " "
                                    + d.getHours() + ":"  
                                    + d.getMinutes() + ":" 
                                    + d.getSeconds();
                        
                        var serialnum = $.trim($('#serialnum').val());
                        var creationdate = datetime;
                        var passed = failval;

                        //$(".inputerror").remove();
                        
                        if(serialnum.length <1){
                            document.getElementById("serialnum").focus();
                            $('#serialnum').addClass( "inputerror" );
                            alert("Please enter a Serial Number");
                        }else{
                            
                            $.ajax({
                                type: 'POST',
                                url:  '../ultravision/jsburnin.php',
                                data: {
                                    serialnum: serialnum, 
                                    passed: passed
                                },
                                success: function(data) {
                                    //alert('SUCCESSFULLY ADDDED RECORD');
                                    location.reload();  
                                    document.getElementById("serialnum").focus();
                                    //$('#serialnum').val('');
                                    $('#serialnum').removeClass( "inputerror" );
                                    //$('#burnintbl').DataTable().ajax.reload();
                                    if(data != ""){
                                        alert(data);
                                    }
                                    //$('#output').html(function(i, val) { return val*1+1 });
                                    
                                },
                                error: function(data){
                                    alert(data);
                                }
                            });
                        }
                        return false;
                    //}
                });
            });
        </script>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    </body>
</html>