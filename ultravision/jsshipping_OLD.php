<?php
    session_start();
    require_once('../config.php');

    $palletID = $_POST['palletid'];
    //$workorder = $_POST['workorder'];
    $sdate   = strtotime($_POST['shipdate']);
    $shipdate = date('Y-m-d', $sdate);
    $serialnum  = strtoupper($_POST['serialnum']);
    $creationdate = $_POST['creationdate'];//date('Y-m-d H:i:s');
    $client     ="UltraVision";
    $uid        =$_SESSION['userlogin'];

    $sql1 = "SELECT * FROM uvrepairdata WHERE serialnum = ? LIMIT 1";
    $stmtselect1 = $db->prepare($sql1);
    $result1 = $stmtselect1->execute([$serialnum]);
    $data1 = $stmtselect1->fetch(PDO::FETCH_ASSOC);
    if($stmtselect1->rowCount() > 0){
        $sql = "SELECT * FROM uvpalletdata WHERE serialnum = ? and palletID = ? LIMIT 1";
        $stmtselect = $db->prepare($sql);
        $result = $stmtselect->execute([$serialnum, $palletID]);

        if(!$result){
            $msg = "Serial Number does not match Pallet ID";
            echo $msg;
        }else{
            $data = $stmtselect->fetch(PDO::FETCH_ASSOC);
            if($stmtselect->rowCount() > 0){
                $insertsql = "INSERT INTO uvshippingdata (palletID, datescanned, shippingdate, serialnumber, client, userid) VALUES (?, ?, ?, ?, ?, ?)";
                $stmt= $db->prepare($insertsql);
                $stmt->execute([$palletID, $creationdate, $shipdate, $serialnum, $client, $uid]);
            }else{
                $msg = "Serial Number does not match Pallet ID";
                echo $msg;
            }
        }
    }else{
        $msg = "Repair data has not been entered for this Serial Number";
        echo $msg;
    }

?>