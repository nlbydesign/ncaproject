<?php
session_start();
require_once('../getdata.php');
require_once('../header.php');

if(!isset($_SESSION['userlogin'])){
    header("Location: ../login.php");
}

?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NCA - ULTRAVISION DATA AQUISITION</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="../css/normalize.css">
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" async defer></script>
        
        <script src="https://kit.fontawesome.com/1e6ad500ad.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/circular-std/style.css" >
        <link rel="stylesheet" href="../assets/libs/css/style.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="../assets/vendor/vector-map/jqvmap.css">
        <link rel="stylesheet" href="../assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
        <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
        
    </head>
    <body>
        <?php header2(); ?>
        <!-- BREADCRUM -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header" style="margin-top: 2%; padding-left: 25px;">
                    <h3 class="mb-2">UltraVision</h3>
                    <p class="pageheader-text"></p>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="../index.php" class="breadcrumb-link">UltraVision</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Intake</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- MAIN PAGE -->
        <div id="page-container">
            <div id="content-wrap" class="container-fluid batinspect">
                <form id="intakeform" autocomplete="off" style="margin-bottom: 20px;">
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-9">
                            <div class="row justify-content-center">
                                <div class="col-md-10 d-flex justify-content-center">
                                    <h1>ULTRAVISION UNIT INTAKE</h1>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-3 sectiontype" style="margin-right: 10px;">
                                    <div class="row">
                                        <div class="col-md-12 sectionhdr">
                                            <label for="palletID" class="d-flex justify-content-center inputlabel sectionhdrlabel">Work Order</label>
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="text" name="workorder" id="workorder" class="form-control input_user serialnuminput">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 sectiontype" style="margin-right: 10px;">
                                    <div class="row">
                                        <div class="col-md-12 sectionhdr">
                                            <label for="palletID" class="d-flex justify-content-center inputlabel sectionhdrlabel">Pallet ID</label>
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="text" name="palletID" id="palletID" class="form-control input_user serialnuminput">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 sectiontype">
                                    <div class="row">
                                        <div class="col-md-12 sectionhdr">
                                            <label for="serialnum" class="d-flex justify-content-center inputlabel sectionhdrlabel">Serial Number</label>
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="text" name="serialnum" id="serialnum" class="form-control input_user serialnuminput">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center" style="margin-top: 15px;">
                                <div class="col-md-3 sectiontype" style="margin-right: 10px;">
                                    <div class="row">
                                        <div class="col-md-12 sectionhdr">
                                            <label for="serialnum" class="d-flex justify-content-center inputlabel sectionhdrlabel2">Water Damage</label>
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class="col-md-12 d-flex justify-content-center">
                                        <table style="margin-top: 10px;">
                                            <tr>
                                                <td>NO</td>
                                                <td>
                                                    <label class="switch">
                                                        <input type="checkbox" name="wd" id="wd" value="WD">
                                                            <span class="slider round"></span>
                                                    </label>
                                                </td>
                                                <td>YES</td>
                                            </tr>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 sectiontype" style="margin-right: 10px;">
                                    <div class="row">
                                        <div class="col-md-12 sectionhdr">
                                            <label for="serialnum" class="d-flex justify-content-center inputlabel sectionhdrlabel2">Damaged Board</label>
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class="col-md-12 d-flex justify-content-center">
                                        <table style="margin-top: 10px;">
                                            <tr>
                                                <td>NO</td>
                                                <td>
                                                    <label class="switch">
                                                        <input type="checkbox" name="db" id="db" value="DB">
                                                            <span class="slider round"></span>
                                                    </label>
                                                </td>
                                                <td>YES</td>
                                            </tr>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 sectiontype" style="margin-right: 10px;">
                                    <div class="row">
                                        <div class="col-md-12 sectionhdr">
                                            <label for="serialnum" class="d-flex justify-content-center inputlabel sectionhdrlabel2">Damaged Housing</label>
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class="col-md-12 d-flex justify-content-center">
                                        <table style="margin-top: 10px;">
                                            <tr>
                                                <td>NO</td>
                                                <td>
                                                    <label class="switch">
                                                        <input type="checkbox" name="dh" id="dh" value="DH">
                                                            <span class="slider round"></span>
                                                    </label>
                                                </td>
                                                <td>YES</td>
                                            </tr>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-2 d-flex justify-content-center">
                                   
                                    <div class="pgbt2">
                                            <button id="submitrecord" name="submitrecord" class="submitrecord">SUBMIT</button>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </form>
            </div>         
            <div class="row d-flex justify-content-center batinspect">
                <div class="col-md-8">
                    <div id="shippedunits" class="table-responsive table-wrapper-scroll-y my-custom-scrollbar">
                        <table id="pallettbl" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr role="row">
                                    <th class="th-sm">Row ID</th>
                                    <th class="th-sm">Work Order</th>
                                    <th class="th-sm">Pallet ID</th>
                                    <th class="th-sm">Date Scanned</th>
                                    <th class="th-sm">Serial Number</th>
                                    <th class="th-sm">Status</th>
                                    <th class="th-sm">User ID</th>
                                </tr>
                            </thead>
                            
                            <tfoot>
                                <tr>
                                    <th>Row ID</th>
                                    <th>Work Order</th>
                                    <th>Pallet ID</th>
                                    <th>Date Scanned</th>
                                    <th>Serial Number</th>
                                    <th>Status</th>
                                    <th>User ID</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            Copyright © 2020 National Circuit Assembly. All rights reserved.
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!------------------------------------------------------->
        <!----            END MAIN PAGE AREA                ----->
        <!------------------------------------------------------->
        
        <script>
            $( document ).ready(function() {
                $("#pallettbl").DataTable({
                    "scrollY"       : "400px",
                    "scrollCollapse": true,
                    "order"         : [[ 0, "desc" ]],
                    "pagingType"    : "first_last_numbers",
                    "ajax" : {
                        "url": "../ultravision/jspallettable.php",
                        "dataSrc" : "" 
                    },
                    "columns" : [
                        {"data": "Row ID"},
                        {"data": "Work Order"},
                        {"data": "Pallet ID"},
                        {"data": "Date Scanned"},
                        {"data": "Serial Number"},
                        {"data": "Status"},
                        {"data": "User ID"}
                    ]
                });
                $('.dataTables_length').addClass('bs-select');

                //$('.input-daterange').datepicker({
                //    todayBtn:'linked',
                //    format: "yyyy-mm-dd",
                //    autoclose:true
                //});
                
                //$('#target').click(function() {
                //    $('#output').text('0');
                //});
                
                document.getElementById("workorder").focus();
                $('form input').on('keypress', function(e) {
                    return e.which !== 13;
                });
                
                $( "#serialnum" ).change(function() {
                    $('#serialnum').removeClass( "inputerror" );
                });

                $('form').submit(function(){
                    //var valid = this.form.checkValidity();
                    var selected = [];
                    
                    $(" input[type=checkbox]:checked").each(function() {
                        //alert('here');
                        var x = this.value;
                        //alert(x);
                        selected.push({
                            "choice": x
                        });
                    });
                    var obj = JSON.stringify(selected);
                    //alert(obj);

                    var d = new Date(); 
                    var month = d.getMonth()+1;
                    var day = d.getDate();
                    
                    var datetime = d.getFullYear() + "-"  
                                + ((''+month).length<2 ? '0' : '') + month + "-" 
                                + ((''+day).lenth<2 ? '0' : '') + day + " "
                                + d.getHours() + ":"  
                                + d.getMinutes() + ":" 
                                + d.getSeconds();

                    var wo = $.trim($('#workorder').val());
                    var palletid = $.trim($('#palletID').val());
                    var serialnum = $.trim($('#serialnum').val());
                    var creationdate = datetime;
                    
                    if(wo.length <1){
                        document.getElementById("workorder").focus();
                        $('#wo').addClass( "inputerror" );
                        alert("Please enter a Work Order Number");
                    }else if(palletid.length <1){
                        document.getElementById("palletID").focus();
                        $('#palletid').addClass( "inputerror" );
                        alert("Please enter a Pallet ID");
                    
                    }else if(serialnum.length <1){
                        document.getElementById("serialnum").focus();
                        $('#serialnum').addClass( "inputerror" );
                        alert("Please enter a Serial Number");
                    
                    }else{
                        
                        $.ajax({
                            type: 'POST',
                            url:  '../ultravision/jspallet.php',
                            data: {
                                wo: wo,
                                palletid: palletid,
                                serialnum: serialnum,
                                unitstatus: obj
                            },
                            success: function(data) {
                                //alert(data);
                                document.getElementById("serialnum").focus();
                                $('#serialnum').val('');
                                $('#wd').prop("checked", false);
                                $('#db').prop("checked", false);
                                $('#dh').prop("checked", false);
                                $('#serialnum').removeClass( "inputerror" );
                                $('#palletID').removeClass( "inputerror" );
                                $('#workorder').removeClass( "inputerror" );
                                $('#pallettbl').DataTable().ajax.reload();
                                //$('#output').html(function(i, val) { return val*1+1 });
                            },
                            error: function(data){
                                alert('Data failed to post.');
                            }
                        });
                        return false;
                    }
                });
            });
        </script>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    </body>
</html>