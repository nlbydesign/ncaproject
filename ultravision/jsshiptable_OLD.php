<?php
    session_start();
    require_once('../config.php');

    $checkval = $_GET['pid']; 

    //$sql = "SELECT * FROM uvshippingdata ORDER BY id DESC LIMIT 500";
    $sql = "SELECT A.id, A.palletID as pallet1, A.wo, A.datescanned, A.serialnum, B.palletID as pallet2, B.serialnumber, B.shippingdate, B.userid FROM uvpalletdata AS A LEFT JOIN uvshippingdata AS B ON B.serialnumber = A.serialnum WHERE A.palletID = '$checkval'";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();
    $table_data = array();
    if($stmtselect->rowCount() > 0){
        while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) {
            if(!empty($rowitems['shippingdate'])){
                $sdate = date_create($rowitems['shippingdate']);
                $sdate = date_format($sdate,"m/d/Y");
            }else{
                $sdate = "";
            }
            $date = date_create($rowitems['datescanned']);
            $date = date_format($date,"m/d/Y");
            $table_data[] = array(
                'Row ID' => $rowitems['id'],
                'Work Order' => $rowitems['wo'],
                'Pallet ID'  => $rowitems['pallet1'],
                'Shipping Date'   => $sdate,
                'Intake Serial Num'    => $rowitems['serialnum'],
                'Shipping Serial Num'    => $rowitems['serialnumber'],
                'User ID' => $rowitems['userid']
            );
        }
    }
    echo json_encode($table_data);
?>