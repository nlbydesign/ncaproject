<?php
require_once('config.php');

function getTheatroprodmenu() {
    ?>
    <a class="dropdown-item" href="../theatro/batteryinspection.php"><i class="fas fa-battery-three-quarters mr-2"></i>Battery Inspection</a>
    <a class="dropdown-item" href="../theatro/triage.php"><i class="fas fa-heartbeat mr-2"></i>Triage</a>
    <a class="dropdown-item" href="../theatro/R121Repair.php"><i class="fas fa-cog mr-2"></i>R121 Repair</a>
    <a class="dropdown-item" href="../theatro/rmaintakedata.php"><i class="fas fa-plus mr-2"></i>RMA Intake</a>
    <a class="dropdown-item" href="../theatro/rmatriage.php"><i class="fas fa-heartbeat mr-2"></i>RMA Triage</a>
    <a class="dropdown-item" href="../theatro/shippingdata.php"><i class="fas fa-truck mr-2"></i>Shipping</a>
    <?php
}

function getUltraVisionprodmenu() {
    ?>
    <a class="dropdown-item" href="../ultravision/unitintake.php"><i class="fas fa-plus mr-2"></i>UltraVision Intake</a>
    <a class="dropdown-item" href="../ultravision/unitrepair.php"><i class="fas fa-heartbeat mr-2"></i>UltraVision Repair</a>
    <a class="dropdown-item" href="../ultravision/unitburnin.php"><i class="fa fa-sun-o mr-2" aria-hidden="true"></i>UltraVision Burn In</a>
    <a class="dropdown-item" href="../ultravision/shipping.php"><i class="fas fa-truck mr-2"></i>UltraVision Shipping</a>
    <?php
}

function getEagleprodmenu() {
    ?>

    <a class="dropdown-item" href="../smd/engineering.php"><i class="fas fa-boxes mr-2" style="margin-right: 10px;"></i>Engineering</a>
    <a class="dropdown-item" href="../smd/kitting.php"><i class="fas fa-boxes mr-2" style="margin-right: 10px;"></i>Kitting</a>
    <a class="dropdown-item" href="../smd/staging.php"><i class="fas fa-boxes mr-2" style="margin-right: 10px;"></i>Staging</a>
    <a class="dropdown-item" href="../smd/smdline.php"><i class="fas fa-window-minimize mr-2" style="margin-right: 10px;"></i>SMD Lines</a>
    <?php
}

function leftnav() {
    ?>
    <div class="nav-left-sidebar sidebar-dark">
        <div class="menu-list">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav flex-column">
                        <li class="nav-divider">
                            DASHBOARD
                        </li>
                        <?php 
                        $getprodline = $_SESSION['prodline'];
                        $getcomp1 = "Theatro";
                        $getcomp2 = "UltraVision";
                        $getcomp3 = "Eagle";
                        $getcomp4 = "All";
                        $getstring1 = strpos($getprodline, $getcomp1);
                        $getstring2 = strpos($getprodline, $getcomp2);
                        $getstring3 = strpos($getprodline, $getcomp3);
                        $getstring4 = strpos($getprodline, $getcomp4);

                        if($getstring1 !== false || $getstring4 !== false) { ?>
                            <li class="nav-item ">
                                <!-- <a class="nav-link active" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-1" aria-controls="submenu-1"><i class="fa fa-fw fa-user-circle"></i>Analytics<span class="badge badge-success">6</span></a> -->
                                <a class="nav-link" href="nca_bi.php"><i class="fa fa-fw fa-battery-half"></i>Battery Inspection</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="nca_rma.php"><i class="fa fa-fw fa-tag"></i>RMA</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="nca_triage.php"><i class="fa fa-fw fa-heartbeat"></i>Triage</a>
                            </li>
                        <?php } ?>
                            <li class="nav-item">
                                <a class="nav-link" href="nca_export.php"><i class="fas fa-fw fa-table"></i>View/Export Data</a>
                            </li>
                            </li>
                        <?php if($getstring4 !== false) { ?>
                            <li class="nav-item ">
                                <a class="nav-link" href="nca_admin.php"><i class="fa fa-fw fa-tag"></i>Admin</a>
                            </li>
                        <?php } ?>
                            <!--<li class="nav-item">
                                <a class="nav-link" href="nca_admin.php"><i class="fas fa-fw fa-user-circle"></i>Admin</a>
                            </li>-->
                            <!--<li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2" aria-controls="submenu-2"><i class="fa fa-fw fa-rocket"></i>OPT 2</a>

                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3" aria-controls="submenu-3"><i class="fas fa-fw fa-chart-pie"></i>OPT 3</a>

                            </li>-->
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <?php
}

function getproductlines() {
    global $db;
    $sql = "SELECT * FROM productlines";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();
    if($stmtselect->rowCount() > 0){
        while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) { ?>
            <div class="input-group mb-3 signupinput">
                <label class="checkbxlbl"><input type = "checkbox" class="plcheckbox" id="productline" name="productline[]" value="<?php echo $rowitems['productline']; ?>" /><?php echo $rowitems['productline']; ?></label>
            </div>
        <?php }
    }
}

function getTotRMACount(){
    global $db;
    $sql = "SELECT * FROM rmaintake";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();

    $count = $stmtselect->rowCount();

    return($count);
}

function getRMA($usefield,$batstat){

    global $db;
    $fieldid = $usefield;  

    //$sql1 = "SELECT * FROM battery_inspection";
    //$stmtselect1 = $db->prepare($sql);
    //$result1 = $stmtselect1->execute();

    //$totrows = $stmtselect1->rowCount();

    $sql = "SELECT * FROM rmaintake WHERE $fieldid = ?";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute([$batstat]);

    $battery = $stmtselect->rowCount();

    return($battery);
}

function getTotBatCount(){
    global $db;
    $sql = "SELECT * FROM battery_inspection";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();

    $count = $stmtselect->rowCount();

    return($count);
}

function getTotTriageCount(){
    global $db;
    $sql = "SELECT * FROM battery_triage";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();

    $count = $stmtselect->rowCount();

    return($count);
}

function getBattery($usefield,$batstat){

    global $db;
    $fieldid = $usefield;

    $sql = "SELECT * FROM battery_inspection WHERE $fieldid = ?";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute([$batstat]);

    $battery = $stmtselect->rowCount();

    return($battery);
}

function getTriage($usefield,$stat){

    global $db;
    $fieldid = $usefield;

    $sql = "SELECT * FROM battery_triage WHERE $fieldid = ?";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute([$stat]);

    $triage = $stmtselect->rowCount();

    return($triage);
}

function gettotrows($table){
    global $db;
    $tablename = $table;
    
    $sql2 = "SELECT * FROM $tablename";
    $stmtselect2 = $db->prepare($sql2);
    $result2 = $stmtselect2->execute();

    $totrows = $stmtselect2->rowCount();

    return($totrows);
}

function getUserInfo($user){
    global $db;
    
    $sql = "SELECT * FROM users WHERE empcode = ? LIMIT 1";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute([$user]);

    if($result){
        $userinfo = $stmtselect->fetch(PDO::FETCH_ASSOC);
        if($stmtselect->rowCount() > 0){
            $fname = $userinfo['fname'];
            $lname = $userinfo['lname'];
            $position = $userinfo['position'];
            $prodline = $userinfo['productline'];
       }
    }

    return array($fname, $lname, $position, $prodline);
}

function getItems($stage,$col,$minwidth){
    global $db;
    
    $sql = "SELECT * FROM status_codes WHERE stage = ?";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute([$stage]);
    //$data = $stmtselect->fetch(PDO::FETCH_ASSOC);
    if($stmtselect->rowCount() > 0){
        while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC))
            {
            //echo $rowitems['statusid']." " . $rowitems['statusdescription']." " .$rowitems['filename']." " .$rowitems['fieldname']."<br />";
            ?>
            <div class="col-md-<?php echo $col; ?> d-flex justify-content-center" style="min-width:<?php echo $minwidth; ?>px;padding: 0 2px;">
                <div id="<?php echo $rowitems['fieldname']; ?>" class='pgbtn'>
                    <div class="col-md-12 <?php echo $rowitems['fieldname']; ?>"></div>
                    <?php
                        if($rowitems['filename'] == ''){
                            ?>
                                <div class="nopic">
                            <?php
                        }else{
                            ?>
                                <div class="d-flex justify-content-center">
                            <?php
                        }
                    ?>
                    <?php echo $rowitems['statusdescription']; ?></div>
                </div>
            </div>
            <?php
            }
    }
}

function getRepairItems($category,$col,$minwidth,$client){
    global $db;
    
    $sql = "SELECT * FROM status_codes WHERE stage = ? and client = ?";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute([$category, $client]);
    //$data = $stmtselect->fetch(PDO::FETCH_ASSOC);
    
    if($stmtselect->rowCount() > 0){
        ?><ul class="ks-cboxtags"><?php
        while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC))
            {
            ?>
                <li>
                    <input type="checkbox" id="<?php echo $rowitems['statusdescription']; ?>" value="<?php echo $rowitems['description']; ?>" name="statuscode[]">
                    
                    <label for="<?php echo $rowitems['statusdescription']; ?>"><?php echo $rowitems['description']; ?>
                    
                    <div class="row d-flex justify-content-center"><span class="optlbl" id="lbl_<?php echo $rowitems['statusdescription']; ?>" hidden>QTY</span><input type="text" id="text_<?php echo $rowitems['statusdescription']; ?>" name="text_<?php echo $rowitems['statusdescription']; ?>" style="width:35px;" hidden/></div>
                    </label>
                </li>
            <?php
            }
        ?></ul><?php
    }
}

function tableview($start, $stop, $tablename){
    global $db;

    $startdate = strtotime($start);
    $enddate = strtotime($stop);

    $sdate = date('Y-m-d', $startdate);
    $edate = date('Y-m-d', $enddate);
    //get records from database
    $sql = "SELECT * FROM battery_inspection WHERE datescanned BETWEEN ? AND ? ORDER BY datescanned ASC";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute([$sdate, $edate]);

    if($result){
        $data = $stmtselect->fetch(PDO::FETCH_ASSOC);
        $totrows = $stmtselect->rowCount();
        if($stmtselect->rowCount() > 0){
            echo "<h2>".$totrows." Records Found</h2>";
            echo "<table border='1'>
            <tr>
            <th>Date Inspected</th>
            <th>User ID</th>
            <th>Serial Num</th>
            <th>Status</th>
            <th>Type</th>
            <th>+2 Yrs</th>
            <th>Date Code</th>
            <th>R-Date Code</th>
            </tr>";
        
            while($row=$stmtselect->fetch()){
                echo "<tr>";
                echo "<td style='padding:0 5px;'>".$row['datescanned']."</td>";
                echo "<td style='padding:0 5px;'>".$row['userid']."</td>";
                echo "<td style='padding:0 5px;'>".$row['serialnum']."</td>";
                echo "<td style='padding:0 5px;'>".$row['batstatus']."</td>";
                echo "<td style='padding:0 5px;'>".$row['batterytype']."</td>";
                echo "<td style='padding:0 5px;'>".$row['battery2yrs']."</td>";
                echo "<td style='padding:0 5px;'>".$row['datecode']."</td>";
                echo "<td style='padding:0 5px;'>".$row['rpmtdatecode']."</td>";
                echo "</tr>";
            }
            echo "</table>";
        }
    }

}

function getDefaultData(){
    global $db;
    
    $sql = "SELECT * FROM nca_defaults";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();

    if($result){
        $default = $stmtselect->fetch(PDO::FETCH_ASSOC);
        if($stmtselect->rowCount() > 0){
            $rmadate = date_create($default['rmastartdate']); 
            $rmadate = date_format($rmadate, 'Y-m-d');
       }
    }

    return array($rmadate);
}

function getsmdlines() {
    global $db;
    $sql = "SELECT * FROM line";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();
    if($stmtselect->rowCount() > 0){ ?>
        <select id="smtline" style="margin-top:12px;"> 
            <option value="">--Select--</option>
        <?php    
        while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) { ?>
                <option  value="<?php echo $rowitems['machinetype']; ?>"><?php echo $rowitems['machinetype']; ?></option>
        <?php } ?>
        </select> <?php
    }
}

function getclientlist($x) {
    global $db;
    $sql = "SELECT * FROM client";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();
    if($stmtselect->rowCount() > 0){ ?>
        <select id="client" style="margin-top:8px; width: 80%;" <?php echo $x; ?>>
            <option value="">--Select Client--</option> 
            <?php
            while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) { ?>
                <option  value="<?php echo $rowitems['idclient']; ?>"><?php echo $rowitems['clientname']; ?></option>
        <?php } ?>
        </select> <?php
    }
}

function getvisittype() {
    global $db;
    $sql = "SELECT * FROM visitreasons";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();
    if($stmtselect->rowCount() > 0){ ?>
        <select id="reason1" name="reason1" class="select-css">
            <option value="">--Select--</option> 
            <?php
            while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) { ?>
                <option  value="<?php echo $rowitems['value']; ?>"><?php echo $rowitems['description']; ?></option>
        <?php } ?>
        </select> <?php
    }
}

function gettestinginfo($x, $y) { 
    global $db;
    $sql = "SELECT * FROM testcodes WHERE testid = ? AND fieldtype = ?";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute([$x, $y]);
    if($stmtselect->rowCount() > 0){ ?>
        <div class="col-md-11 d-flex flex-wrap tabular" style="column-count: 3;">
        <?php    
        while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) { ?>

            <div class="col-md-4 newframe" style="margin-bottom:8px;padding-left: 0px;">
                <div class="col-md-12 d-flex">
                    <div class="col-md-8" style="font-size:16px; padding-top: 8px;">
                        <span><?php echo $rowitems['item']; ?></span>
                    </div>
                    <div class="col-md-1">
                        <table>
                            <tr>
                                <td style="font-size: 16px;">NO</td>
                                <td>
                                    <label class="switch2">
                                        <input type="checkbox" name="<?php echo $rowitems['fieldid']; ?>" id="<?php echo $rowitems['fieldid']; ?>">
                                            <span class="slider2 round"></span>
                                    </label>
                                </td>
                                <td style="font-size: 16px;">YES</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        <?php 
        } ?>
        </div>
<?php 
    }
} 

