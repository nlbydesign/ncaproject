<?php
    session_start();
    require_once('config.php');

    $sql = "SELECT * FROM visitors AS V WHERE V.timeout IS NULL";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();
    $table_data = array();
    if($stmtselect->rowCount() > 0){
        while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) {
            $test = substr($rowitems['timein'], 11);
            if(!$test){
                $test = '';
            }else{
                $test = date('h:i a', strtotime($test));
            }
            $table_data[] = array(
                'ID' => $rowitems['id'],
                //'Time In' => $rowitems['timein'],
                'Time In' => $test,
                'Name' => $rowitems['visitorname'],
                'Time Out' => $rowitems['timeout'],

            );
        }
    }
    echo json_encode($table_data);
?>
