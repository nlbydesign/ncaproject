$(document).ready(function() {
    $.ajax({
        url: "../theatro/rmachartdata.php",
        datatype: 'json',
        success: function(data) {
            console.log(data);
            var obj = jQuery.parseJSON(data);
            //console.log(obj);
            var len = obj.length;
            //console.log(len);
            for (i = 0; i < obj.length; i++) {

                var abat = [];
                var bbat = [];
                var tbat = [];

                tbat.push(obj[i].rma);
                abat.push(obj[i].oow);
                bbat.push(obj[i].nru)

                //console.log(tbat);
                //console.log(abat);
                //console.log(bbat);

                var chartdata = {
                    labels: '',
                    datasets: [{
                            label: "RMA",
                            backgroundColor: "rgba(102, 204, 255,0.5)",
                            borderColor: "rgba(102, 204, 255,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: tbat
                        },
                        {
                            label: "Out Of Warranty",
                            backgroundColor: "rgba(120, 105, 255,0.5)",
                            borderColor: "rgba(120, 105, 255,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: abat
                        },
                        {
                            label: "Non-Repaired Unit",
                            backgroundColor: "rgba(255, 204, 153,0.5)",
                            borderColor: "rgba(255, 204, 153,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: bbat
                        }
                    ]
                };

                Chart.scaleService.updateScaleDefaults('linear', {
                    ticks: {
                        min: 0
                    }
                });
                const ctx = $('#chartjs_RMA');

                const barGraph = new Chart(ctx, {
                    type: 'bar',
                    data: chartdata,
                    options: {
                        legend: {
                            display: true,
                            position: 'right'
                        }
                    }
                });

            };

        },
        error: function(data) {
            console.log(data);
            //console.log('there');
        }
    })
})