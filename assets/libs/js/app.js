$(document).ready(function() {
    $.ajax({
        url: "../theatro/chartdata.php",
        datatype: 'json',
        success: function(data) {

            var obj = jQuery.parseJSON(data);

            var len = obj.length;
            //console.log(len);
            for (i = 0; i < obj.length; i++) {

                var abat = [];
                var bbat = [];
                var tbat = [];

                tbat.push(obj[i].type);
                abat.push(obj[i].yok);
                bbat.push(obj[i].as);

                //console.log(tbat);
                //console.log(abat);
                //console.log(bbat);

                var chartdata = {
                    labels: '',
                    datasets: [{
                            label: "YOK",
                            backgroundColor: "rgba(86, 105, 255,0.5)",
                            borderColor: "rgba(86, 105, 255,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: abat
                        },
                        {
                            label: "A&S",
                            backgroundColor: "rgba(200, 105, 255,0.5)",
                            borderColor: "rgba(200, 105, 255,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: bbat
                        }
                    ]
                };

                if (tbat == "Broken Wire") {
                    tbat = 'BrokenWire';
                } else if (tbat == "No Battery - NCA") {
                    tbat = 'NoBatteryNCA';
                } else if (tbat == "No Battery - Theatro") {
                    tbat = 'NoBatteryTheatro';
                } else if (tbat == "Cannot Charge") {
                    tbat = 'CannotCharge';
                } else if (tbat == "Battery 2+ yrs") {
                    tbat = '2Yrs';
                }

                Chart.scaleService.updateScaleDefaults('linear', {
                    ticks: {
                        min: 0
                    }
                });
                const ctx = $('#chartjs_' + tbat);

                const barGraph = new Chart(ctx, {
                    type: 'bar',
                    data: chartdata,
                    options: {
                        legend: {
                            display: false,
                            position: 'bottom'
                        }
                    }
                });

            };

        },
        error: function(data) {
            console.log(data);
        }
    })
})