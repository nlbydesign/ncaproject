$(document).ready(function() {
    $.ajax({
        url: "../theatro/triagechartdata.php",
        datatype: 'json',
        success: function(data) {
            console.log(data);
            var obj = jQuery.parseJSON(data);
            //console.log(obj);
            var len = obj.length;
            //console.log(len);
            for (i = 0; i < obj.length; i++) {

                var abat = [];
                var bbat = [];
                var cbat = [];
                var dbat = [];
                var ebat = [];
                var fbat = [];
                var gbat = [];
                var hbat = [];
                var ibat = [];
                var jbat = [];
                var kbat = [];
                var lbat = [];
                var mbat = [];
                var nbat = [];

                abat.push(obj[i].pass);
                bbat.push(obj[i].wifi);
                cbat.push(obj[i].led);
                dbat.push(obj[i].audio);
                ebat.push(obj[i].sw1);
                fbat.push(obj[i].sw2);
                gbat.push(obj[i].sw3);
                hbat.push(obj[i].sw4);
                ibat.push(obj[i].sw24);
                jbat.push(obj[i].power);
                kbat.push(obj[i].flr);
                lbat.push(obj[i].bat);
                mbat.push(obj[i].ff);
                nbat.push(obj[i].fg);

                //console.log(abat);
                //console.log(bbat);
                //console.log(cbat);

                var chartdata = {
                    labels: '',
                    datasets: [{
                            label: "Passed",
                            backgroundColor: "rgba(102, 204, 255,0.5)",
                            borderColor: "rgba(102, 204, 255,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: abat
                        },
                        {
                            label: "WIFI",
                            backgroundColor: "rgba(120, 105, 255,0.5)",
                            borderColor: "rgba(120, 105, 255,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: bbat
                        },
                        {
                            label: "LED",
                            backgroundColor: "rgba(255, 204, 153,0.5)",
                            borderColor: "rgba(255, 204, 153,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: cbat
                        },
                        {
                            label: "Audio",
                            backgroundColor: "rgba(255, 204, 255,0.5)",
                            borderColor: "rgba(255, 204, 255,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: dbat
                        },
                        {
                            label: "Switch 1",
                            backgroundColor: "rgba(153, 255, 153,0.5)",
                            borderColor: "rgba(153, 255, 153,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: ebat
                        },
                        {
                            label: "Switch 2",
                            backgroundColor: "rgba(153, 153, 255,0.5)",
                            borderColor: "rgba(153, 153, 255,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: fbat
                        },
                        {
                            label: "Switch 3",
                            backgroundColor: "rgba(255, 153, 51,0.5)",
                            borderColor: "rgba(255, 153, 51,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: gbat
                        },
                        {
                            label: "Switch 4",
                            backgroundColor: "rgba(255, 102, 102,0.5)",
                            borderColor: "rgba(255, 102, 102,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: hbat
                        },
                        {
                            label: "Switch 2 & 4",
                            backgroundColor: "rgba(0, 0, 255,0.5)",
                            borderColor: "rgba(0, 0, 255,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: ibat
                        },
                        {
                            label: "Power",
                            backgroundColor: "rgba(0, 102, 0,0.5)",
                            borderColor: "rgba(0, 102, 0,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: jbat
                        },
                        {
                            label: "Flash Red",
                            backgroundColor: "rgba(255, 0, 0,0.5)",
                            borderColor: "rgba(255, 0, 0,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: kbat
                        },
                        {
                            label: "Battery",
                            backgroundColor: "rgba(102, 0, 0,0.5)",
                            borderColor: "rgba(102, 0, 0,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: lbat
                        },
                        {
                            label: "Fault Found",
                            backgroundColor: "rgba(255, 0, 127,0.5)",
                            borderColor: "rgba(255, 0, 127,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: mbat
                        },
                        {
                            label: "Flashing Green",
                            backgroundColor: "rgba(102, 204, 0,0.5)",
                            borderColor: "rgba(102, 204, 0,0.7)",
                            borderWidth: 2,
                            barPercentage: 0.5,
                            barThickness: 6,
                            maxBarThickness: 8,
                            minBarLength: 2,
                            data: nbat
                        }
                    ]
                };

                Chart.scaleService.updateScaleDefaults('linear', {
                    ticks: {
                        min: 0
                    }
                });
                const ctx = $('#chartjs_triage');

                const barGraph = new Chart(ctx, {
                    type: 'bar',
                    data: chartdata,
                    options: {
                        legend: {
                            display: true,
                            position: 'right'
                        }
                    }
                });

            };

        },
        error: function(data) {
            console.log(data);
            //console.log('there');
        }
    })
})