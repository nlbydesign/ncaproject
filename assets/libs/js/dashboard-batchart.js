$(function() {
    "use strict";
    //Better to construct options first and then pass it as a parameter
    var ctx = document.getElementById('chartjs').getContext('2d');
    var options = {
        title: {
            text: "Results By Battery Type"
        },
        data: [{
            // Change type to "doughnut", "line", "splineArea", etc.
            type: "column",
            dataPoints: [
                { label: "apple", y: 10 },
                { label: "orange", y: 15 },
                { label: "banana", y: 25 },
                { label: "mango", y: 30 },
                { label: "grape", y: 28 }
            ]
        }]
    };

});