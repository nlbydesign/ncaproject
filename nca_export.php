<?php
    session_start();
    require_once('getdata.php');
    require_once('header.php');

    if(!isset($_SESSION['userlogin'])){
        header("Location: login.php");
    }

?>
    <!doctype html>
    <html lang="en">


    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/libs/css/style.css">
        <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="assets/vendor/vector-map/jqvmap.css">
        <link rel="stylesheet" href="assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
        <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
        <!-- <script type="text/javascript" src="DataTables/datatables.min.js"></script> -->
        <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css">
     
        
        <title>National Circuit Assembly Dashboard</title>
    </head>
    

    <body>
        <!-- ============================================================== -->
        <!-- main wrapper -->
        <!-- ============================================================== -->
        <div class="dashboard-main-wrapper">
            <!-- ============================================================== -->
            <!-- navbar -->
            <!-- ============================================================== -->
            <?php header1(); ?>
            <!-- ============================================================== -->
            <!-- end navbar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- left sidebar -->
            <!-- ============================================================== -->
            <?php leftnav(); ?>
            <!-- ============================================================== -->
            <!-- end left sidebar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- wrapper  -->
            <!-- ============================================================== -->
            <div class="dashboard-wrapper">
                <div class="container-fluid  dashboard-content">
                    <!-- ============================================================== -->
                    <!-- pagehader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h3 class="mb-2">Theatro Dashboard</h3>
                                <p class="pageheader-text"></p>
                                <div class="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Export Data</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- pagehader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <!-- metric -->
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="text-muted">Select Data Range</h5>
                                    <!--<form id="exportform" autocomplete="off" action="exportData.php?startdate=<?php //echo $_GET['sdate'];?>&enddate=<?php //echo $_GET['edate'];?>&tn=<?php //echo $_GET['gettablename'];?>&type=<?php //echo $_GET['downloadtype'];?>"
                                    onsubmit="setTimeout(function () { window.location.reload(); }, 8000)">-->
                                    <form id="exportform" autocomplete="off">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <label for="sdate">Data Table</label>
                                                <select id="gettablename" name="gettablename" class="select-css">
                                                <?php 
                                                $getprodline = $_SESSION['prodline'];
                                                $getcomp1 = "Theatro";
                                                $getcomp2 = "UltraVision";
                                                $getcomp3 = "Eagle";
                                                $getcomp4 = "All";
                                                $getstring1 = strpos($getprodline, $getcomp1);
                                                $getstring2 = strpos($getprodline, $getcomp2);
                                                $getstring3 = strpos($getprodline, $getcomp3);
                                                $getstring4 = strpos($getprodline, $getcomp4);

                                                if($getstring1 !== false || $getstring4 !== false) { ?>
                                                    <option value="battery_inspection">Inspection</option>
                                                    <option value="battery_triage">Triage</option>
                                                    <option value="shippingdata">Shipping</option>
                                                    <option value="rmaintake">RMA Data</option>
                                                    <option value="unit_r121">R121 Mods</option>
                                                <?php }else if($getstring2 !== false || $getstring4 !== false) { ?> 
                                                    <option value="uvpalletdata">Intake Data</option>
                                                    <option value="uvrepairdata">Repair Data</option>
                                                    <option value="uvburnindata">Burn In Data</option>
                                                    <option value="uvshippingdata">Shipping Data</option>
                                                <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <label for="sdate">Start Date</label>
                                                <input type="text" id='sdate' name='sdate' class="form-control input-daterange"/>
                                            </div>
                                            <div class="col-lg-2">   
                                                <label for="edate">End Date</label>
                                                <input type="text" id='edate' name='edate' class="form-control input-daterange"/>
                                            </div>
                                            <div class="col-lg-2">  
                                                <label for="downloadtype">View Type</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="downloadtype" id="complete" value="all" >
                                                    <label class="form-check-label" for="complete">
                                                    Complete History
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="downloadtype" id="recent" value="recent">
                                                    <label class="form-check-label" for="recent">
                                                    Most Recent Action
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class='pgbtn3' style="margin-top:20px;">
                                                    <button id='exprecord' name='exprecord' class='form-control exprecord'>View Data</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <table id="demotbl" class="table table-striped table-bordered" cellspacing="0" width="100%" hidden>
                                            <thead>
                                                <tr>
                                                    <th class="th-sm"></th>
                                                    <th class="th-sm"></th>
                                                    <th class="th-sm"></th>
                                                    <th class="th-sm"></th>
                                                    <th class="th-sm"></th>
                                                    <th class="th-sm"></th>
                                                    <th class="th-sm"></th>
                                                    <th class="th-sm"></th>
                                                    <th class="th-sm"></th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- ============================================================== -->
                    <!-- footer -->
                    <!-- ============================================================== -->
                    <div class="footer">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    Copyright © 2020 National Circuit Assembly. All rights reserved.
                                </div>
                            
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end footer -->
                    <!-- ============================================================== -->
                </div>
                <!-- ============================================================== -->
                <!-- end wrapper  -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- end main wrapper  -->
            <!-- ============================================================== -->
            <!-- Optional JavaScript -->
            <!-- jquery 3.3.1 js-->
            <!--<script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>-->
            <!-- bootstrap bundle js-->
            <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
            <!-- slimscroll js-->
            <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
            <!-- chartjs js-->
            <script src="assets/vendor/charts/charts-bundle/Chart.bundle.js"></script>
            <!-- main js-->
            <script src="assets/libs/js/main-js.js"></script>
            <script type="text/javascript" language="javascript">
                
                
                $(document).ready(function() {

                    $('.input-daterange').datepicker({
                        todayBtn:'linked',
                        format: "yyyy-mm-dd",
                        autoclose:true
                    });
                    
      
                    $('#exportform').submit(function(e){
                        
                        e.preventDefault();

                        var radioValue = $("input[name='downloadtype']:checked").val();
                        var tablename = $.trim($('#gettablename').val());
                        var start_date = $.trim($('#sdate').val());
                        var end_date = $.trim($('#edate').val());
                        alert(radioValue + ', ' + tablename + ', ' + start_date.length + ', ' + end_date.length)

                        if(start_date.length < 1){
                            document.getElementById("sdate").focus();
                            $('#sdate').addClass( "inputerror" );
                            alert("Please enter a Start Date");
                        }else if(end_date.length < 1){
                            document.getElementById("edate").focus();
                            $('#edate').addClass( "inputerror" );
                            alert("Please select an End Date");
                        }else{
                            $("#demotable").DataTable({
                            destroy: true,
                            "scrollY"       : "600px",
                            "scrollCollapse": true,
                            "order"         : [[ 0, "desc" ],],
                            "pagingType"    : "first_last_numbers",
                            "pageLength": 50,
                            "ajax" : {
                                "url": "exportdata2.php",
                                "data" : {
                                    tablename: tablename, 
                                    start_date: start_date, 
                                    end_date: end_date,
                                    radioValue: radioValue
                                    },
                                "dataSrc" : "" 
                            },
                            "columns" : [
                                
                            ],
                            
                            });
                            $('.dataTables_length').addClass('bs-select');

                            /*$.ajax({
                                type: 'POST',
                                url: 'exportdata2.php',
                                data: {
                                    tablename: tablename, 
                                    start_date: start_date, 
                                    end_date: end_date,
                                    radioValue: radioValue
                                },
                                success: function(data) {
                                    alert(data);
                                
                                },
                                error: function(data){
                                    alert('Data failed to post.');
                                }
                            });*/
                        }
                    });
                });
            </script>
    </body>

    </html>