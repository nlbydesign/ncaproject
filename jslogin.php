<?php
session_start();
require_once('config.php');

$empcode = $_POST['username'];
$password = $_POST['password'];

$sql = "SELECT * FROM users WHERE empcode = ? LIMIT 1";
$stmtselect = $db->prepare($sql);
$result = $stmtselect->execute([$empcode]);

if($result){
    $user = $stmtselect->fetch(PDO::FETCH_ASSOC);
    if($stmtselect->rowCount() > 0){
        if(password_verify($password, $user['password'])){
            
            $empid = $user['empcode'];
            $fname = $user['fname'];
            $lname = $user['lname'];
            $position = $user['position'];
            $prodline = $user['productline'];

            $_SESSION['userlogin'] = $empid;
            $_SESSION['prodline'] = $prodline;
            $_SESSION['position'] = $position;

            $userinfo = array('fname'=>$fname, 'lname'=>$lname, 'position'=>$position, 'prodline'=>$prodline);
            echo json_encode($userinfo);
        }else{
           echo "Please check your Employee Code and Password.";
        }
    } 
}else{
    echo 'There were errors connecting to the database.';
}

return array($fname, $lname, $position, $prodline);