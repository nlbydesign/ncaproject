<?php
    session_start();
    require_once('../config.php');

    $workorder  =$_POST['workorder'];
    $sdate   = strtotime($_POST['shipdate']);
    $shipdate = date('Y-m-d', $sdate);
    $serialnum  = strtoupper($_POST['serialnum']);
    $status  =$_POST['status'];
    $creationdate=date('Y-m-d H:i:s');
    $client     ="Theatro";
    $uid        =$_SESSION['userlogin'];
    
    $insertsql = "INSERT INTO shippingdata (wo, datescanned, shippingdate, serialnumber, client, userid, status) VALUES (?, ?, ?, ?, ?, ?, ?)";
    $stmt= $db->prepare($insertsql);
    $stmt->execute([$workorder, $creationdate, $shipdate, $serialnum, $client, $uid, $status]);

?>