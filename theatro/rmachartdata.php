<?php
session_start();
require_once('../config.php');

    global $db;
    $rmatypes = ['RMA'];

    $arraylength = count($rmatypes);

    $data = array();

    $type= "";
    $rma = "";
    $oow = "";
    $nru = "";
    for ($i = 0; $i < count($rmatypes); $i++) {

        //$rmastat = $rmatypes[$i];

        $sql1 = "SELECT * FROM rmaintake WHERE status = 'RMA'";
        $stmtselect1 = $db->prepare($sql1);
        $result1 = $stmtselect1->execute();
        
        $rma = $stmtselect1->rowCount();

        $sql2 = "SELECT * FROM rmaintake WHERE status = 'Out Of Warranty'";
        $stmtselect2 = $db->prepare($sql2);
        $result2 = $stmtselect2->execute();
        
        $oow = $stmtselect2->rowCount();

        $sql3 = "SELECT * FROM rmaintake WHERE status = 'Non-Repaired Unit'";
        $stmtselect3 = $db->prepare($sql3);
        $result3 = $stmtselect3->execute();
        
        $nru = $stmtselect3->rowCount();

        $data[$i] = ['rma'=>$rma, 'oow'=>$oow, 'nru'=>$nru];
    }

    print json_encode($data);

?>
