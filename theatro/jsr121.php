<?php
    session_start();
    require_once('../config.php');

	$serialnum  = strtoupper($_POST['serialnum']);
    $creationdate=date('Y-m-d H:i:s');
    $client     ="Theatro";
    $uid        =$_SESSION['userlogin'];
    $status     ="";

    $sql = "SELECT * FROM unit_r121 WHERE serialnum = ? LIMIT 1";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute([$serialnum]);

    if($result){
        $user = $stmtselect->fetch(PDO::FETCH_ASSOC);
        if($stmtselect->rowCount() > 0){
            echo "Serial Number already Added";
        }else{
            $insertsql = "INSERT INTO unit_r121 (datescanned, userid, serialnum, status, client) VALUES (?, ?, ?, ?, ?)";
            $stmt= $db->prepare($insertsql);
            $stmt->execute([$creationdate, $uid, $serialnum, $status, $client]);
        }
    }
            
?>