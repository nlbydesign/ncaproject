<?php
session_start();
require_once('../config.php');

    global $db;
    $rmatypes = ['triage'];

    $arraylength = count($rmatypes);

    $data = array();

    $pass = "";
    $wifi = "";
    $led = "";
    $audio = "";
    $sw1 = "";
    $sw2 = "";
    $sw3 = "";
    $sw4 = "";
    $sw24 = "";
    $power = "";
    $flr = "";
    $bat = "";
    $ff = "";
    $fg = "";

    ['pass'=>$pass, 'wifi'=>$wifi, 'led'=>$led, 'sw1'=>$sw1, 'sw2'=>$sw2, 'sw3'=>$sw3, 'sw4'=>$sw4, 'sw24'=>$sw24, 'audio'=>$audio, 'power'=>$power, 'flr'=>$flr, 'bat'=>$bat, 'ff'=>$ff, 'fg'=>$fg];

    for ($i = 0; $i < count($rmatypes); $i++) {

        //$rmastat = $rmatypes[$i];

        $sql1 = "SELECT * FROM battery_triage WHERE status = 'PASSED'";
        $stmtselect1 = $db->prepare($sql1);
        $result1 = $stmtselect1->execute();
        
        $pass = $stmtselect1->rowCount();

        $sql2 = "SELECT * FROM battery_triage WHERE status = 'WIFI'";
        $stmtselect2 = $db->prepare($sql2);
        $result2 = $stmtselect2->execute();
        
        $wifi = $stmtselect2->rowCount();

        $sql3 = "SELECT * FROM battery_triage WHERE status = 'LED'";
        $stmtselect3 = $db->prepare($sql3);
        $result3 = $stmtselect3->execute();
        
        $led = $stmtselect3->rowCount();

        $sql3 = "SELECT * FROM battery_triage WHERE status = 'SW1'";
        $stmtselect3 = $db->prepare($sql3);
        $result3 = $stmtselect3->execute();
        
        $sw1 = $stmtselect3->rowCount();

        $sql3 = "SELECT * FROM battery_triage WHERE status = 'SW2'";
        $stmtselect3 = $db->prepare($sql3);
        $result3 = $stmtselect3->execute();
        
        $sw2 = $stmtselect3->rowCount();

        $sql3 = "SELECT * FROM battery_triage WHERE status = 'SW3'";
        $stmtselect3 = $db->prepare($sql3);
        $result3 = $stmtselect3->execute();
        
        $sw3 = $stmtselect3->rowCount();

        $sql3 = "SELECT * FROM battery_triage WHERE status = 'SW4'";
        $stmtselect3 = $db->prepare($sql3);
        $result3 = $stmtselect3->execute();
        
        $sw4 = $stmtselect3->rowCount();

        $sql3 = "SELECT * FROM battery_triage WHERE status = 'SW24'";
        $stmtselect3 = $db->prepare($sql3);
        $result3 = $stmtselect3->execute();
        
        $sw24 = $stmtselect3->rowCount();

        $sql3 = "SELECT * FROM battery_triage WHERE status = 'AUDIO'";
        $stmtselect3 = $db->prepare($sql3);
        $result3 = $stmtselect3->execute();
        
        $audio = $stmtselect3->rowCount();

        $sql3 = "SELECT * FROM battery_triage WHERE status = 'POWER'";
        $stmtselect3 = $db->prepare($sql3);
        $result3 = $stmtselect3->execute();
        
        $power = $stmtselect3->rowCount();

        $sql3 = "SELECT * FROM battery_triage WHERE status = 'FLR'";
        $stmtselect3 = $db->prepare($sql3);
        $result3 = $stmtselect3->execute();
        
        $flr = $stmtselect3->rowCount();

        $sql3 = "SELECT * FROM battery_triage WHERE status = 'BAT'";
        $stmtselect3 = $db->prepare($sql3);
        $result3 = $stmtselect3->execute();
        
        $bat = $stmtselect3->rowCount();

        $sql3 = "SELECT * FROM battery_triage WHERE status = 'FF'";
        $stmtselect3 = $db->prepare($sql3);
        $result3 = $stmtselect3->execute();
        
        $ff = $stmtselect3->rowCount();

        $sql3 = "SELECT * FROM battery_triage WHERE status = 'FLASH GREEN'";
        $stmtselect3 = $db->prepare($sql3);
        $result3 = $stmtselect3->execute();
        
        $fg = $stmtselect3->rowCount();

        $data[$i] = ['pass'=>$pass, 'wifi'=>$wifi, 'led'=>$led, 'sw1'=>$sw1, 'sw2'=>$sw2, 'sw3'=>$sw3, 'sw4'=>$sw4, 'sw24'=>$sw24, 'audio'=>$audio, 'power'=>$power, 'flr'=>$flr, 'bat'=>$bat, 'ff'=>$ff, 'fg'=>$fg];
    }

    print json_encode($data);

?>