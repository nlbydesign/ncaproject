<?php
    session_start();
    require_once('../config.php');

	$serialnum  = strtoupper($_POST['serialnum']);
	$datecode   = $_POST['datecode'];
	$batrepdate =$_POST['batrepdate'];
    $creationdate=date('Y-m-d H:i:s');
    $client     ="Theatro";
    $uid        =$_SESSION['userlogin'];
    $status     =$_POST['poststatus'];
    $batterytype=$_POST['postbattype'];
    $battery2yrs=$_POST['postbat2yr'];

    //format A&S DateCodes
    $asyr = substr($datecode, 0, 4);
    if($asyr == '2019' || $asyr == '1833' || $asyr == '1848' || $asyr == '1901' || $asyr == '1920'){
        $batterytype = "A&S";
    }
    if($batterytype == 'A&S'){
        $datecode = preg_replace("/[^a-zA-Z0-9]/", "", $datecode);
    }

    //Verify Date Codes are correct
    $sql = "SELECT * FROM battery_codes WHERE batterycode = ? LIMIT 1";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute([$datecode]);

    $sql2 = "SELECT * FROM battery_codes WHERE batterycode = ? LIMIT 1";
    $stmtselect2 = $db->prepare($sql2);
    $result2 = $stmtselect2->execute([$batrepdate]);


    if($result){
        $batdata = $stmtselect->fetch(PDO::FETCH_ASSOC);
        if(($stmtselect->rowCount() > 0) || (substr($status, 0, 10) == 'No Battery')){
            $batdata2 = $stmtselect2->fetch(PDO::FETCH_ASSOC);
            if($stmtselect2->rowCount() > 0 || ($status == 'NFF') || ($status == 'MFG')) {
                $insertsql = "INSERT INTO battery_inspection (datescanned, userid, serialnum, batstatus, batterytype, datecode, rpmtdatecode, client, blank) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                $stmt= $db->prepare($insertsql);
                $stmt->execute([$creationdate, $uid, $serialnum, $status, $batterytype, $datecode, $batrepdate, $client, $battery2yrs]);
                echo '1';
            }else{
                echo '2';
            }
        }else{
            echo '0';
        }
    }

?>