<?php
    session_start();
    require_once('../getdata.php');
    require_once('../header.php');

    if(!isset($_SESSION['userlogin'])){
        header("Location: ../login.php");
    }

?>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NCA - THEATRO DATA AQUISITION</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="../css/normalize.css">
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" async defer></script>
        
        <script src="https://kit.fontawesome.com/1e6ad500ad.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="../assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="../assets/libs/css/style.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="../assets/vendor/vector-map/jqvmap.css">
        <link rel="stylesheet" href="../assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
        <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script>
            
        </script>
    </head>
    <body>
        <?php header2(); ?>
        <div id="page-container">
            <div class="row">
            <div id="content-wrap" class="container-fluid batinspect">
                <div class="d-flex" style="margin-top:75px;"></div>
                    <form id="inspectionform" autocomplete="off">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-5">
                                <div class="row">
                                    <div class="col-md-12 sectiontype">
                                        <div class="row">
                                            <div class="col-md-12 sectionhdr">
                                                <label for="serialnum" class="d-flex justify-content-center inputlabel sectionhdrlabel">Serial Number</label>
                                            </div>
                                        </div>    
                                        <div class="row">
                                            <div class="col-md-11">
                                                <input type="text" name="serialnum" id="serialnum" class="form-control input_user serialnuminput">
                                            </div>
                                            <div class="col-md-1">
                                                <span id="eraseserialnum" class="clearbutton1"><i class="fa fa-eraser seticon" aria-hidden="true"></i></span>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 25px;">
                                    <div class="col-md-12 sectiontype">
                                        <div class="row" >
                                            <div class="col-md-12 sectionhdr">
                                                <label for="batterytype" class="d-flex justify-content-center inputlabel sectionhdrlabel">Battery Information</label>
                                                <input type="text" name="batterytype" id="batterytype" class="form-control input_user" hidden>
                                            </div>
                                        </div>
                                        <div id="batdetailrow" class="row">
                                            <div class="col-md-6 justify-content-center">
                                                <div id="battypeyok" class='pgtypebtn'>
                                                    <div class="col-md-12 yok">
                                                    </div>
                                                    <div class="d-flex justify-content-center">YOK</div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 justify-content-center">
                                                <div id="battypeas" class='pgtypebtn'>
                                                    <div class="col-md-12 as">
                                                    </div>
                                                    <div class="d-flex justify-content-center">A&S</div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12 d-flex justify-content-center">
                                            <div class='pgbtn2'>
                                                <div class="row">
                                                    <div class="justify-content-center labeltag">Date Code</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-11">
                                                        <input type="text" name="datecode" id="datecode" class="form-control input_user batrepdateinput">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <span id="erasedatecode" class="clearbutton2"><i class="fa fa-eraser seticon" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 d-flex justify-content-center">
                                            <div class='pgbtn2'>
                                                <div class="row">
                                                    <div class="justify-content-center labeltag">Replacement Date Code</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-11 justify-content-center">
                                                        <input type="text" name="batrepdate" id="batrepdate" class="form-control input_user batrepdateinput">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <span id="eraserpmtdatecode" class="clearbutton2"><i class="fa fa-eraser seticon" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 d-flex justify-content-center">
                                                <div class='pgbtn2'>
                                                    <button id='submitrecord' name='submitrecord' class='submitrecord'>SUBMIT</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="col-md-5">
                            <div id="statustype" class="col-md-12 sectiontype">
                                    <div class="row" >
                                        <div class="col-md-12 sectionhdr">
                                            <label for="status" class="d-flex justify-content-center inputlabel sectionhdrlabel">Status</label>
                                            <input type="text" name="status" id="status" class="form-control input_user" hidden>
                                        </div>
                                    </div>
                                    <div class="row" >
                                        <?php
                                            $itemsback = getItems('batteryinspection','4','180');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-top:20px;">
                            <div class="col-md-2 d-flex align-items-end flex-column">
                                <input id='poststatus' name='poststatus' style="width:150px;" hidden>
                                <input id='postbattype' name='postbattype' style="width:150px;" hidden>
                                <input id='postbat2yr' name='postbat2yr' style="width:150px;" hidden>
                                <input id='compID' name='compid' style="width:300px;" value="<?php echo $compVar;?>" hidden>
                            </div>
                        </div>
                    </form>   
            </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            Copyright © 2020 National Circuit Assembly. All rights reserved.
                        </div>
                    
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end main area -->
        <!-- ============================================================== -->
        
        <script>
            $( document ).ready(function() {
                document.getElementById("serialnum").focus();
                $('form input').on('keypress', function(e) {
                    return e.which !== 13;
                });
                
                //$( "#serialnum" ).bind("keypress", function() {
                //    setTimeout(function(){
                //        document.getElementById("datecode").focus();
                //    }, 500);
                //});

                //$( "#datecode" ).bind("keypress", function() {
                //    setTimeout(function(){
                //        document.getElementById("batrepdate").focus();
                //    }, 500);
                //});

                $( "#serialnum" ).change(function() {
                    $('#serialnum').removeClass( "inputerror" );
                });

                $( "#eraseserialnum" ).click(function() {
                    $('#serialnum').val("");
                    document.getElementById("serialnum").focus();
                });

                $( "#datecode" ).change(function() {
                    $('#datecode').removeClass( "inputerror" );
                });

                $( "#erasedatecode" ).click(function() {
                    $('#datecode').val("");
                    document.getElementById("datecode").focus();
                });

                $( "#batrepdate" ).change(function() {
                    $('#batrepdate').removeClass( "inputerror" );
                });

                $( "#eraserpmtdatecode" ).click(function() {
                    $('#batrepdate').val("");
                    document.getElementById("batrepdate").focus();
                });

                $( "#battypeyok" ).click(function() {
                    if ($(this).hasClass('statusselected')) {
                        $(this).toggleClass( "statusselected" );
                        $("#postbattype").val("");
                    }else{
                        $( this ).addClass( "statusselected" );
                        $("#postbattype").val("YOK");
                        $("#battypeas").removeClass( "statusselected" );
                        $('#batdetailrow').removeClass( "inputerror" );
                    };
                });

                $( "#battypeas" ).click(function() {
                    if ($(this).hasClass('statusselected')) {
                        $(this).toggleClass( "statusselected" );
                        $("#postbattype").val("");
                    }else{
                        $( this ).addClass( "statusselected" );
                        $("#postbattype").val("A&S");
                        $("#battypeyok").removeClass( "statusselected" );
                        $('#batdetailrow').removeClass( "inputerror" );
                    };
                });

                $( "#bat2yr" ).click(function() {
                    if ($(this).hasClass('statusselected')) {
                        $(this).toggleClass( "statusselected" );
                        $("#poststatus").val("");
                    }else{
                        $( this ).toggleClass( "statusselected" );
                        $("#swollen").removeClass( "statusselected" );
                        $("#punctured").removeClass( "statusselected" );
                        $("#wrinkled").removeClass( "statusselected" );
                        $("#brokenwire").removeClass( "statusselected" );
                        $("#nobatterytheatro").removeClass( "statusselected" );
                        $("#nobatterynca").removeClass( "statusselected" );
                        $("#cantcharge").removeClass( "statusselected" );
                        $("#lockup").removeClass( "statusselected" );
                        $("#NFF").removeClass( "statusselected" );
                        $("#mfg").removeClass( "statusselected" );
                        $("#poststatus").val("Battery 2+ yrs");
                        document.getElementById("batrepdate").focus();
                    };
                });

                $( "#swollen" ).click(function() {
                    if ($(this).hasClass('statusselected')) {
                        $(this).toggleClass( "statusselected" );
                        $("#poststatus").val("");
                    }else{
                        $( this ).addClass( "statusselected" );
                        $("#bat2yr").removeClass( "statusselected" );
                        $("#punctured").removeClass( "statusselected" );
                        $("#wrinkled").removeClass( "statusselected" );
                        $("#brokenwire").removeClass( "statusselected" );
                        $("#nobatterytheatro").removeClass( "statusselected" );
                        $("#nobatterynca").removeClass( "statusselected" );
                        $("#cantcharge").removeClass( "statusselected" );
                        $("#lockup").removeClass( "statusselected" );
                        $("#NFF").removeClass( "statusselected" );
                        $("#mfg").removeClass( "statusselected" );
                        $("#poststatus").val("Swollen");
                        $('#statustype').removeClass( "inputerror" );
                    };
                    
                });

                $( "#punctured" ).click(function() {
                    if ($(this).hasClass('statusselected')) {
                        $(this).toggleClass( "statusselected" );
                        $("#poststatus").val("");
                    }else{
                        $( this ).addClass( "statusselected" );
                        $("#bat2yr").removeClass( "statusselected" );
                        $("#swollen").removeClass( "statusselected" );
                        $("#wrinkled").removeClass( "statusselected" );
                        $("#brokenwire").removeClass( "statusselected" );
                        $("#nobatterytheatro").removeClass( "statusselected" );
                        $("#nobatterynca").removeClass( "statusselected" );
                        $("#cantcharge").removeClass( "statusselected" );
                        $("#lockup").removeClass( "statusselected" );
                        $("#NFF").removeClass( "statusselected" );
                        $("#mfg").removeClass( "statusselected" );
                        $("#poststatus").val("Punctured");
                        $('#statustype').removeClass( "inputerror" );
                    };
                });

                $( "#wrinkled" ).click(function() {
                    if ($(this).hasClass('statusselected')) {
                        $(this).toggleClass( "statusselected" );
                        $("#poststatus").val("");
                    }else{
                        $( this ).addClass( "statusselected" );
                        $("#bat2yr").removeClass( "statusselected" );
                        $("#punctured").removeClass( "statusselected" );
                        $("#swollen").removeClass( "statusselected" );
                        $("#brokenwire").removeClass( "statusselected" );
                        $("#nobatterytheatro").removeClass( "statusselected" );
                        $("#nobatterynca").removeClass( "statusselected" );
                        $("#cantcharge").removeClass( "statusselected" );
                        $("#lockup").removeClass( "statusselected" );
                        $("#NFF").removeClass( "statusselected" );
                        $("#mfg").removeClass( "statusselected" );
                        $("#poststatus").val("Wrinkled");
                        $('#statustype').removeClass( "inputerror" );
                    };
                });

                $( "#brokenwire" ).click(function() {
                    if ($(this).hasClass('statusselected')) {
                        $(this).toggleClass( "statusselected" );
                        $("#poststatus").val("");
                    }else{
                        $( this ).addClass( "statusselected" );
                        $("#bat2yr").removeClass( "statusselected" );
                        $("#punctured").removeClass( "statusselected" );
                        $("#wrinkled").removeClass( "statusselected" );
                        $("#swollen").removeClass( "statusselected" );
                        $("#nobatterytheatro").removeClass( "statusselected" );
                        $("#nobatterynca").removeClass( "statusselected" );
                        $("#cantcharge").removeClass( "statusselected" );
                        $("#lockup").removeClass( "statusselected" );
                        $("#NFF").removeClass( "statusselected" );
                        $("#mfg").removeClass( "statusselected" );
                        $("#poststatus").val("Broken Wire");
                        $('#statustype').removeClass( "inputerror" );
                    };
                });

                $( "#nobatterynca" ).click(function() {
                    if ($(this).hasClass('statusselected')) {
                        $(this).toggleClass( "statusselected" );
                        $("#poststatus").val("");
                    }else{
                        $( this ).addClass( "statusselected" );
                        $("#bat2yr").removeClass( "statusselected" );
                        $("#punctured").removeClass( "statusselected" );
                        $("#wrinkled").removeClass( "statusselected" );
                        $("#brokenwire").removeClass( "statusselected" );
                        $("#swollen").removeClass( "statusselected" );
                        $("#cantcharge").removeClass( "statusselected" );
                        $("#NFF").removeClass( "statusselected" );
                        $("#lockup").removeClass( "statusselected" );
                        $("#mfg").removeClass( "statusselected" );
                        $("#nobatterytheatro").removeClass( "statusselected" );
                        $("#poststatus").val("No Battery - NCA");
                        $('#statustype').removeClass( "inputerror" );
                    };
                });

                $( "#nobatterytheatro" ).click(function() {
                    if ($(this).hasClass('statusselected')) {
                        $(this).toggleClass( "statusselected" );
                        $("#poststatus").val("");
                    }else{
                        $( this ).addClass( "statusselected" );
                        $("#bat2yr").removeClass( "statusselected" );
                        $("#punctured").removeClass( "statusselected" );
                        $("#wrinkled").removeClass( "statusselected" );
                        $("#brokenwire").removeClass( "statusselected" );
                        $("#swollen").removeClass( "statusselected" );
                        $("#cantcharge").removeClass( "statusselected" );
                        $("#lockup").removeClass( "statusselected" );
                        $("#NFF").removeClass( "statusselected" );
                        $("#mfg").removeClass( "statusselected" );
                        $("#nobatterynca").removeClass( "statusselected" );
                        $("#poststatus").val("No Battery - Theatro");
                        $('#statustype').removeClass( "inputerror" );
                    };
                });

                $( "#cantcharge" ).click(function() {
                    if ($(this).hasClass('statusselected')) {
                        $(this).toggleClass( "statusselected" );
                        $("#poststatus").val("");
                    }else{
                        $( this ).addClass( "statusselected" );
                        $("#bat2yr").removeClass( "statusselected" );
                        $("#punctured").removeClass( "statusselected" );
                        $("#wrinkled").removeClass( "statusselected" );
                        $("#brokenwire").removeClass( "statusselected" );
                        $("#nobatterytheatro").removeClass( "statusselected" );
                        $("#nobatterynca").removeClass( "statusselected" );
                        $("#swollen").removeClass( "statusselected" );
                        $("#NFF").removeClass( "statusselected" );
                        $("#lockup").removeClass( "statusselected" );
                        $("#mfg").removeClass( "statusselected" );
                        $("#poststatus").val("Cannot Charge");
                        $('#statustype').removeClass( "inputerror" );
                    };
                });

                $( "#NFF" ).click(function() {
                    if ($(this).hasClass('statusselected')) {
                        $(this).toggleClass( "statusselected" );
                        $("#poststatus").val("");
                    }else{
                        $( this ).addClass( "statusselected" );
                        $("#bat2yr").removeClass( "statusselected" );
                        $("#punctured").removeClass( "statusselected" );
                        $("#wrinkled").removeClass( "statusselected" );
                        $("#brokenwire").removeClass( "statusselected" );
                        $("#nobatterytheatro").removeClass( "statusselected" );
                        $("#nobatterynca").removeClass( "statusselected" );
                        $("#swollen").removeClass( "statusselected" );
                        $("#cantcharge").removeClass( "statusselected" );
                        $("#mfg").removeClass( "statusselected" );
                        $("#lockup").removeClass( "statusselected" );
                        $("#poststatus").val("NFF");
                        $("#batrepdate").val("");
                        $('#statustype').removeClass( "inputerror" );
                    };
                });

                $( "#mfg" ).click(function() {
                    if ($(this).hasClass('statusselected')) {
                        $(this).toggleClass( "statusselected" );
                        $("#poststatus").val("");
                    }else{
                        $( this ).addClass( "statusselected" );
                        $("#bat2yr").removeClass( "statusselected" );
                        $("#punctured").removeClass( "statusselected" );
                        $("#wrinkled").removeClass( "statusselected" );
                        $("#brokenwire").removeClass( "statusselected" );
                        $("#nobatterytheatro").removeClass( "statusselected" );
                        $("#nobatterynca").removeClass( "statusselected" );
                        $("#swollen").removeClass( "statusselected" );
                        $("#cantcharge").removeClass( "statusselected" );
                        $("#lockup").removeClass( "statusselected" );
                        $("#NFF").removeClass( "statusselected" );
                        $("#poststatus").val("MFG");
                        $('#statustype').removeClass( "inputerror" );
                    };
                });

                $( "#lockup" ).click(function() {
                    if ($(this).hasClass('statusselected')) {
                        $(this).toggleClass( "statusselected" );
                        $("#poststatus").val("");
                    }else{
                        $( this ).addClass( "statusselected" );
                        $("#bat2yr").removeClass( "statusselected" );
                        $("#punctured").removeClass( "statusselected" );
                        $("#wrinkled").removeClass( "statusselected" );
                        $("#brokenwire").removeClass( "statusselected" );
                        $("#nobatterytheatro").removeClass( "statusselected" );
                        $("#nobatterynca").removeClass( "statusselected" );
                        $("#swollen").removeClass( "statusselected" );
                        $("#cantcharge").removeClass( "statusselected" );
                        $("#NFF").removeClass( "statusselected" );
                        $("#poststatus").val("Battery Lockup");
                        $('#statustype').removeClass( "inputerror" );
                        $("#mfg").removeClass( "statusselected" );
                    };
                });


                $('#inspectionform').submit(function(e){
                    
                    e.preventDefault();

                    //var valid = this.form.checkValidity();

                    var d = new Date(); 
                    var month = d.getMonth()+1;
                    var day = d.getDate();
                    
                    var datetime = d.getFullYear() + "-"  
                                + ((''+month).length<2 ? '0' : '') + month + "-" 
                                + ((''+day).lenth<2 ? '0' : '') + day + " "
                                + d.getHours() + ":"  
                                + d.getMinutes() + ":" 
                                + d.getSeconds();
                    
                    var serialnum = $.trim($('#serialnum').val());
                    var datecode = $.trim($('#datecode').val());
                    var batrepdate = $.trim($('#batrepdate').val());
                    var poststatus = $.trim($('#poststatus').val());
                    var postbattype = $.trim($('#postbattype').val());
                    var postbat2yr = $.trim($('#postbat2yr').val());
                    var creationdate = datetime;
                    
                    //$(".inputerror").remove();
                    if(datecode.substring(0, 3) == "A&S"){
                        datecode = datecode.substring(4,13);
                    }
                        
                    if((serialnum.length <1) || (serialnum.length > 5)){
                        document.getElementById("serialnum").focus();
                        $('#serialnum').addClass( "inputerror" );
                        alert("Please enter a valid Serial Number");
                    }else if(serialnum == datecode){
                        document.getElementById("datecode").focus();
                        $('#datecode').addClass( "inputerror" );
                        alert("Date Code cannot be the Serial Number");
                    }else if(serialnum == batrepdate){
                        document.getElementById("batrepdate").focus();
                        $('#batrepdate').addClass( "inputerror" );
                        alert("Replacement Date Code cannot be the Serial Number");
                    }else if(datecode.length <1 && poststatus != 'No Battery - NCA' && poststatus != 'No Battery - Theatro'){
                        document.getElementById("datecode").focus();
                        $('#datecode').addClass( "inputerror" );
                        alert("Please enter the Battery Date Code");
                    }else if(batrepdate.length <1 && poststatus != 'NFF' && poststatus != 'MFG'){
                        document.getElementById("batrepdate").focus();
                        $('#batrepdate').addClass( "inputerror" );
                        alert("Please enter the Replacement Date Code");
                    }else if(poststatus.length <1){
                        $('#statustype').addClass( "inputerror" );
                        alert("Please select the Battery Status");
                    }else if(postbattype.length <1) {
                        $('#batdetailrow').addClass( "inputerror" );
                        alert("Please select the Battery Type");
                    }else{

                        $.ajax({
                            type: 'POST',
                            url: 'jsbatinspection.php',
                            data: {
                                serialnum: serialnum, 
                                datecode: datecode, 
                                batrepdate: batrepdate, 
                                creationdate: creationdate,
                                poststatus: poststatus,
                                postbattype: postbattype,
                                postbat2yr: postbat2yr
                            },
                            success: function(data) {
                                if(data == 0){
                                    alert("Incorrect Date Code");
                                    document.getElementById("datecode").focus();
                                    $('#datecode').addClass( "inputerror" );
                                }else if (data == 2){
                                    alert("Incorrect Replacement Date Code");
                                    document.getElementById("batrepdate").focus();
                                    $('#batrepdate').addClass( "inputerror" );
                                }else{
                                //alert('SUCCESSFULLY ADDDED RECORD');
                                    $('#datecode').removeClass( "inputerror" );
                                    $('#batrepdate').removeClass( "inputerror" );
                                    document.getElementById("serialnum").focus();
                                    location.reload();
                                }
                            },
                            error: function(data){
                                alert('Data failed to post.');
                            }
                        });
                    }
                });
            });
        </script>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    </body>
</html>