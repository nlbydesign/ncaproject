<?php
    session_start();
    require_once('../config.php');

    $rmaorder = $_POST['rmaorder'];
    $rmarequestdate = strtotime($_POST['rmadate']);
    $rdate = date('Y-m-d', $rmarequestdate);
    $serialnum  = strtoupper($_POST['serialnum']);
    $creationdate=date('Y-m-d H:i:s');
    $client     ="Theatro";
    $uid        =$_SESSION['userlogin'];
    $testdate   ="";

    //$rmasql = "SELECT rmastartdate FROM nca_defaults";
    //$rmastmtselect = $db->prepare($rmasql);
    //$rmaresult = $rmastmtselect->execute();
    //$rmadata = $rmastmtselect->fetch(PDO::FETCH_ASSOC);
    //$testdate = $rmadata['rmastartdate'];

    $sql = "SELECT * FROM shippingdata WHERE serialnumber = ?";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute([$serialnum]);
    
    if($result){
        $data = $stmtselect->fetch(PDO::FETCH_ASSOC);
        $row = $stmtselect->rowCount();
        if($stmtselect->rowCount() > 0){
            $origshipdate = $data['datescanned'];
            $testdate = date('Y-m-d', strtotime($data['datescanned']. ' + 45 days'));
            if($rdate <= $testdate && $rdate >= $origshipdate){
                $rmastatus = "RMA";
            }else{
                $rmastatus = "Out Of Warranty";
            }
        }else {
            $rmastatus = "Non-Repaired Unit";
        }
    }
    //echo $origshipdate;
    $insertsql = "INSERT INTO rmaintake (workorder, datescanned, shipdate, rmarequestdate, userid, serialnum, status, client) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    $stmt= $db->prepare($insertsql);
    $stmt->execute([$rmaorder, $creationdate, $origshipdate, $rdate, $uid, $serialnum, $rmastatus, $client]);

?>