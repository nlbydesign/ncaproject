<?php
    session_start();
    require_once('../config.php');

    $sql = "SELECT * FROM shippingdata ORDER BY idshippingdata DESC LIMIT 500";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();
    $table_data = array();
    if($stmtselect->rowCount() > 0){
        while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) {
            $sdate = date_create($rowitems['shippingdate']);
            $sdate = date_format($sdate,"m/d/Y");
            $date = date_create($rowitems['datescanned']);
            $date = date_format($date,"m/d/Y");
            $table_data[] = array(
                'Row ID' => $rowitems['idshippingdata'],
                'Work Order'  => $rowitems['wo'],
                'Date Scanned'   => $date,
                'Shipping Date'   => $sdate,
                'Serial Number'    => $rowitems['serialnumber'],
                'User ID' => $rowitems['userid']
            );
        }
    }
    echo json_encode($table_data);
?>