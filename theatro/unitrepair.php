<?php
    session_start();
    require_once('../getdata.php');
    require_once('../header.php');

    if(!isset($_SESSION['userlogin'])){
        header("Location: ../login.php");
    }
?>
<!DOCTYPE html>
    <head>
    <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NCA - THEATRO DATA AQUISITION</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="../css/normalize.css">
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" async defer></script>
        
        <script src="https://kit.fontawesome.com/1e6ad500ad.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/circular-std/style.css" >
        <link rel="stylesheet" href="../assets/libs/css/style.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="../assets/vendor/vector-map/jqvmap.css">
        <link rel="stylesheet" href="../assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
        <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <!--<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
        <style>
            /* The container */
            .container {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-top: 12px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            
            }

            /* Hide the browser's default checkbox */
            .container input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
            
            }

            /* Create a custom checkbox */
            .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
            border-radius: 0.3rem 0.3rem 0.3rem 0.3rem !important;
            }

            /* On mouse-over, add a grey background color */
            .container:hover input ~ .checkmark {
            background-color: #ccc;
            }

            /* When the checkbox is checked, add a blue background */
            .container input:checked ~ .checkmark {
            background-color: #ee6534;
            }

            /* Create the checkmark/indicator (hidden when not checked) */
            .checkmark:after {
            content: "";
            position: absolute;
            display: none;
            }

            /* Show the checkmark when checked */
            .container input:checked ~ .checkmark:after {
            display: block;
            }

            /* Style the checkmark/indicator */
            .container .checkmark:after {
            left: 9px;
            top: 4px;
            width: 8px;
            height: 15px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
            
            }
        </style>
    </head>
    <body>
    <?php header2(); ?>
        <!-- BREADCRUM -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header" style="margin-top: 2%; padding-left: 25px;">
                    <h3 class="mb-2">Theatro</h3>
                    <p class="pageheader-text"></p>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="../index.php" class="breadcrumb-link">Theatro</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Repair</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div id="page-container">
            <div class="row">
                <div id="content-wrap" class="container-fluid batinspect">
                    <form id="inspectionform" autocomplete="off">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-10 d-flex justify-content-center">
                                <h1>THEATRO REPAIR</h1>
                            </div>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-4 sectiontype">
                                        <div class="row">
                                            <div class="col-md-12 sectionhdr">
                                                <label for="serialnum" class="d-flex justify-content-center inputlabel sectionhdrlabel">Serial Number</label>
                                            </div>
                                        </div>    
                                        <div class="row">
                                            <div class="col-md-10">
                                                <input type="text" name="serialnum" id="serialnum" class="form-control input_user serialnuminput">
                                            </div>
                                            <div class="col-md-2">
                                                <span id="eraseserialnum" class="clearbutton1"><i class="fa fa-eraser seticon" aria-hidden="true"></i></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-10 d-flex justify-content-center">
                                                <div class='pgbtn4'>
                                                    <button id='submitrecord' name='submitrecord' class='submitrecord'>SUBMIT</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div id="statustype" class="col-md-12 sectiontype">
                                            <div class="row">
                                                <div class="col-md-12 sectionhdr">
                                                    <label for="status" class="d-flex justify-content-center inputlabel sectionhdrlabel">Status</label>
                                                    <input type="text" name="status" id="status" class="form-control input_user" hidden>
                                                </div>
                                            </div>
                                            <div id="status" class="row" style="margin:0 10px;">
                                                <?php
                                                    $itemsback = getRepairItems('repair','3','130','Theatro');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row d-flex justify-content-center" style="margin-top:20px;">
                            <div class="col-md-10">
                                <input id='poststatus' name='poststatus' style="width:150px;" hidden>
                                <input id='compID' name='compid' style="width:300px;" value="<?php echo $compVar;?>" hidden>
                            </div>
                        </div>
                    </form>   
                </div>
            </div>
            <div class="row d-flex justify-content-center batinspect">
                <div class="col-md-7">
                    <div id="shippedunits" class="table-responsive table-wrapper-scroll-y my-custom-scrollbar">
                        <table id="repairtbl" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr role="row">
                                    <th class="th-sm">Row ID</th>
                                    <th class="th-sm">Serial Number</th>
                                    <th class="th-sm">Status Code</th>
                                    <th class="th-sm">Rework</th>
                                    <th class="th-sm">Quantity</th>
                                    <th class="th-sm">Date Entered</th>
                                    <th class="th-sm">User ID</th>
                                </tr>
                            </thead>
                            
                            <tfoot>
                                <tr>
                                    <th>Row ID</th>
                                    <th>Serial Number</th>
                                    <th>Status Code</th>
                                    <th>Rework</th>
                                    <th>Quantity</th>
                                    <th>Date Entered</th>
                                    <th>User ID</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            Copyright © 2020 National Circuit Assembly. All rights reserved.
                        </div>
                    
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end main area -->
        <!-- ============================================================== -->
        <script>
            $( document ).ready(function() {
                $("#repairtbl").DataTable({
                    "scrollY"       : "400px",
                    "scrollCollapse": true,
                    "order"         : [[ 0, "desc" ]],
                    "pagingType"    : "first_last_numbers",
                    "ajax" : {
                        "url": "../theatro/jsrepairtable.php",
                        "dataSrc" : "" 
                    },
                    "columns" : [
                        {"data": "Row ID"},
                        {"data": "Serial Number"},
                        {"data": "Status Code"},
                        {"data": "Failed BurnIn"},
                        {"data": "Quantity"},
                        {"data": "Date Entered"},
                        {"data": "User ID"}
                    ]
                });
                $('.dataTables_length').addClass('bs-select');

                document.getElementById("serialnum").focus();
                $('form input').on('keypress', function(e) {
                    return e.which !== 13;
                });

                $( "#serialnum" ).change(function() {
                    $('#serialnum').removeClass( "inputerror" );
                });

                $( "#eraseserialnum" ).click(function() {
                    $('#serialnum').val("");
                    document.getElementById("serialnum").focus();
                });
                
                $(" input[type=checkbox] ").click(function() {
                    var thisid = $(this).attr('id');
                    var x = $('#text_' + thisid);
                    var y = $('#lbl_' + thisid);
                   
                });
                
                //$("#failedbrnin").click(function() {
                //    if($("#failedbrnin").is(":checked")){
                //        var val = $("#failedbrnin").val();
                //    }else{
                //        var val = "NO";
                //    }
                //    alert(val);
                //});
                

                $('form').submit(function(){
                    //var valid = this.form.checkValidity();
                    var selected = [];


                    $(" input[type=checkbox]:checked").each(function() {
                        //alert('here');
                        var x = this.value;
                        //alert(x);
                        var y = $('#text_' + $(this).attr('id')).val();
                        //alert(y);
                        selected.push({
                            "choice": x,
                            "qty" : y
                        });
                    });
                    var obj = JSON.stringify(selected);
                    //alert(obj);

                    if (selected.length > 0) {
                        var myJSONtext = selected;
                        //console.log(myJSONtext);
                    }
                    
                    var d = new Date(); 
                    var month = d.getMonth()+1;
                    var day = d.getDate();
                    
                    var datetime = d.getFullYear() + "-"  
                                + ((''+month).length<2 ? '0' : '') + month + "-" 
                                + ((''+day).lenth<2 ? '0' : '') + day + " "
                                + d.getHours() + ":"  
                                + d.getMinutes() + ":" 
                                + d.getSeconds();
                    
                    var serialnum = $.trim($('#serialnum').val());
                    var poststatus = obj;
                    var creationdate = datetime;

                    //$(".inputerror").remove();
                    
                    if(serialnum.length <1){
                        document.getElementById("serialnum").focus();
                        $('#serialnum').addClass( "inputerror" );
                        alert("Please enter a Serial Number");
                    }else if(poststatus.length <1){
                        alert("Please Select the Status");
                    }else {

                        $.ajax({
                            type: 'POST',
                            url: 'jsrepair.php',
                            data: {
                                serialnum: serialnum, 
                                creationdate: creationdate,
                                poststatus: poststatus,
                                failval: failval
                            },
                           success: function(data) {
                                document.getElementById("serialnum").focus();
                                $('#serialnum').val('');
                                $('#serialnum').removeClass( "inputerror" );
                                //$('#poststatus').val('');
                                //$('#shiptbl').DataTable().ajax.reload();
                            },
                            error: function(data){
                                alert('Data failed to post.');
                            }
                        });
                    }
                });
            });
        </script>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    </body>
</html>