<?php
    session_start();
    require_once('../getdata.php');
    require_once('../header.php');

    if(!isset($_SESSION['userlogin'])){
        header("Location: ../login.php");
    }

?>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NCA - THEATRO DATA AQUISITION</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/normalize.css">
        <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" async defer></script>
        
        <script src="https://kit.fontawesome.com/1e6ad500ad.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="../assets/vendor/fonts/circular-std/style.css" >
        <link rel="stylesheet" href="../assets/libs/css/style.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="../assets/vendor/vector-map/jqvmap.css">
        <link rel="stylesheet" href="../assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
        
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script>
            
        </script>
    </head>
    <body>
        <?php header2(); ?>
        <div id="page-container">
            <div class="row">
                <div id="content-wrap" class="container-fluid batinspect">
                    <div class="d-flex" style="margin-top:75px;"></div>
                        <form id="inspectionform" autocomplete="off">
                            <div class="row d-flex justify-content-center">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3 sectiontype">
                                            <div class="row">
                                                <div class="col-md-12 sectionhdr">
                                                    <label for="serialnum" class="d-flex justify-content-center inputlabel sectionhdrlabel">RMA Request #</label>
                                                </div>
                                            </div>    
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="text" name="workorder" id="workorder" class="form-control input_user serialnuminput">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 sectiontype">
                                            <div class="row">
                                                <div class="col-md-12 sectionhdr">
                                                    <label for="serialnum" class="d-flex justify-content-center inputlabel sectionhdrlabel">Serial Number</label>
                                                </div>
                                            </div>    
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <input type="text" name="serialnum" id="serialnum" class="form-control input_user serialnuminput">
                                                </div>
                                                <div class="col-md-2">
                                                    <span id="eraseserialnum" class="clearbutton1"><i class="fa fa-eraser seticon" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 sectiontype">
                                            <div class="row justify-content-center">
                                                <center class="col-md-12">
                                                    <span style="text-align: center;">Total Scanned</span>
                                                </center>
                                            </div>
                                            <div class="row justify-content-center">
                                                <center class="col-md-12">
                                                    <div id="output">0</div>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row d-flex justify-content-center" style="margin-top:20px;">
                                <div class="col-md-10">
                                    <div id="statustype" class="col-md-12 sectiontype">
                                            <div class="row">
                                                <div class="col-md-12 sectionhdr">
                                                    <label for="status" class="d-flex justify-content-center inputlabel sectionhdrlabel">Status</label>
                                                    <input type="text" name="status" id="status" class="form-control input_user" hidden>
                                                </div>
                                            </div>
                                            <div id="status" class="row" style="margin:0 10px;">
                                                <?php
                                                    $itemsback = getItems('triage','1','150');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row d-flex justify-content-center" style="margin-top:20px;">
                                <div class="col-md-10">
                                    <input id='poststatus' name='poststatus' style="width:150px;" hidden>
                                    <input id='compID' name='compid' style="width:300px;" value="<?php echo $compVar;?>" hidden>
                                </div>
                            </div>
                        </form>   
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            Copyright © 2020 National Circuit Assembly. All rights reserved.
                        </div>
                    
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end main area -->
        <!-- ============================================================== -->
        <script>
            $( document ).ready(function() {
                document.getElementById("workorder").focus();
                $('form input').on('keypress', function(e) {
                    return e.which !== 13;
                });

                $( "#serialnum" ).change(function() {
                    $('#serialnum').removeClass( "inputerror" );
                });

                $( "#eraseserialnum" ).click(function() {
                    $('#serialnum').val("");
                    document.getElementById("serialnum").focus();
                });

                $( "#passed, #sw1, #sw2, #sw3, #sw4, #sw24, #audio, #led, #power, #flr, #bat, #wifi, #ff, #flashgreen" ).click(function() {
                    if ($(this).hasClass('statusselected')) {
                        $(this).toggleClass( "statusselected" );
                        $("#poststatus").val("");
                    }else{
                        var $var = $(this).attr('id');
                        if ($var != "sw24" && $var != "flashgreen"){
                            $var = $var.toUpperCase();
                        }else if ($var == "sw24") {
                            $var = "SW2/SW4";
                        }else if ($var == "flashgreen") {
                            $var = "FLASH GREEN";
                        }
                        $("#passed, #sw1, #sw2, #sw3, #sw4, #sw24, #audio, #led, #power, #flr, #bat, #wifi, #ff, #flashgreen").removeClass( "statusselected" );
                        $( this ).toggleClass( "statusselected" );
                        $("#poststatus").val($var);
                        document.getElementById("serialnum").focus();
                    };
                });

                $('#serialnum').keypress(function(e){
                    if(e.which == 13){
                    
                        e.preventDefault();

                        //var valid = this.form.checkValidity();

                        var d = new Date(); 
                        var month = d.getMonth()+1;
                        var day = d.getDate();
                        
                        var datetime = d.getFullYear() + "-"  
                                    + ((''+month).length<2 ? '0' : '') + month + "-" 
                                    + ((''+day).lenth<2 ? '0' : '') + day + " "
                                    + d.getHours() + ":"  
                                    + d.getMinutes() + ":" 
                                    + d.getSeconds();
                        
                        var serialnum = $.trim($('#serialnum').val());
                        var poststatus = $.trim($('#poststatus').val());
                        var creationdate = datetime;
                        var workorder = $.trim($('#workorder').val());
                        //$(".inputerror").remove();
                        
                        if(workorder.length <1){
                            document.getElementById("workorder").focus();
                            $('#workorder').addClass( "inputerror" );
                            alert("Please enter a RMA Number");
                        }else if(serialnum.length != 5){
                            document.getElementById("serialnum").focus();
                            $('#serialnum').addClass( "inputerror" );
                            alert("Please enter a Correct Serial Number");
                        }else if(poststatus.length <1){
                            alert("Please Select the Status");
                        }else {

                            $.ajax({
                                type: 'POST',
                                url: '../theatro/jsrmatriage.php',
                                data: {
                                    serialnum: serialnum, 
                                    workorder: workorder,
                                    creationdate: creationdate,
                                    poststatus: poststatus
                                },
                                success: function(data) {
                                    document.getElementById("serialnum").focus();
                                    $('#serialnum').val('');
                                    $('#serialnum').removeClass( "inputerror" );
                                    $('#output').html(function(i, val) { return val*1+1 });
                                    $("#passed, #sw1, #sw2, #sw3, #sw4, #sw24, #audio, #led, #power, #flr, #bat, #wifi, #ff, #flashgreen").removeClass( "statusselected" );
                                    $("#poststatus").val("");
                                },
                                error: function(data){
                                    alert('Data failed to post.');
                                }
                            });
                        }
                    }
                });
            });
        </script>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    </body>
</html>