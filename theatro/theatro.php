<?php
    session_start();

    if(!isset($_SESSION['userlogin'])){
        header("Location: login.php");
    }

?>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NCA - THEATRO DATA AQUISITION</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" async defer></script>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/1e6ad500ad.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
    <body>
        <div class="container-fluid headerdiv">
            <div class="row">
                <div class="col-10">
                <img src="img/nca_main_logo.png" class="header_logo" alt="National Circuit Assembly Logo">
                </div>
                <div class="col-2 header_text">
                <a href="theatro.php?logout=true">Logout</a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <!-- <div class="d-flex h-100" > -->
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-5 sectiontype">
                        <div class="row">
                            <div class="col-lg-12 sectionhdr">
                                <label for="serialnum" class="d-flex justify-content-center inputlabel sectionhdr">Serial Number</label>
                            </div>
                        </div>    
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="text" name="serialnum" id="serialnum" class="form-control input_user serialnuminput" required>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 sectiontype">
                        <div class="row">
                            <div class="col-lg-12 sectionhdr">
                                <label for="batterytype" class="d-flex justify-content-center inputlabel ">Battery Type</label>
                                <input type="text" name="batterytype" id="batterytype" class="form-control input_user" hidden required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 justify-content-center">
                                <button class='pgtypebtn'>
                                    <div class="col-lg-12 yok">
                                    </div>
                                    <div class="d-flex justify-content-center">YOK</div>
                                </button>
                            </div>
                            <div class="col-lg-6 justify-content-center">
                                <button class='pgtypebtn'>
                                    <div class="col-lg-12 as">
                                    </div>
                                    <div class="d-flex justify-content-center">A&S</div>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
                <div class="row status">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-10 sectiontype">
                        <div class="row">
                            <div class="col-lg-12 sectionhdr">
                                <label for="status" class="d-flex justify-content-center inputlabel sectionhdr">Status</label>
                                <input type="text" name="status" id="status" class="form-control input_user" hidden required>
                            </div>
                        </div>
                        <div class="row statusline">
                            <div class="col-lg-2 d-flex justify-content-center" style="padding: 0 2px;">
                                <button class='pgbtn'>
                                    <div class="col-lg-12">
                                        <img src="img/battery_swollen_500x500.png" class="pgimg">
                                    </div>
                                    <div class="d-flex justify-content-center">SWOLLEN</div>
                                </button>
                            </div>
                            <div class="col-lg-2 d-flex justify-content-center" style="padding: 0 2px;">
                                <button class='pgbtn'>
                                    <div class="col-lg-12">
                                        <img src="img/battery_punctured_500x500.png" class="pgimg">
                                    </div>
                                    <div class="d-flex justify-content-center">PUNCTURED</div>
                                </button>
                            </div>
                            <div class="col-lg-2 d-flex justify-content-center" style="padding: 0 2px;">
                                <button class='pgbtn'>
                                    <div class="col-lg-12">
                                        <img src="img/battery_wrinkled_500x500.png" class="pgimg">
                                    </div>
                                    <div class="d-flex justify-content-center">WRINKLED</div>
                                </button>
                            </div>
                            <div class="col-lg-2 d-flex justify-content-center" style="padding: 0 2px;">
                                <button id="bat2yr" name="bat2yr" class='pgbtn'>BATTERY<br>2+ YRS</button>
                            </div>
                            <div class="col-lg-4 d-flex justify-content-center">
                                    <div class='pgbtn'>
                                        <div class="justify-content-center labeltag">Replacement Date</div>
                                        <div class="col-lg-12 justify-content-center"><input type="text" name="batrepdate" id="batrepdate" class="form-control input_user batrepdateinput" required></div>
                                    </div>
                            </div>
                        </div>
                        <div class="row statusline">
                            <div class="col-lg-2 d-flex justify-content-center" style="padding: 0 2px;">
                                <button class='pgbtn'>BROKEN<br>WIRE</button>
                            </div>
                            <div class="col-lg-2 d-flex justify-content-center" style="padding: 0 2px;">
                                <button class='pgbtn'>NO<br>BATTERY</button>
                            </div>
                            <div class="col-lg-2 d-flex justify-content-center" style="padding: 0 2px;">
                                <button class='pgbtn'>CANNOT<br>CHARGE</button>
                            </div>
                            <div class="col-lg-2 d-flex justify-content-center" style="padding: 0 2px;">
                                <button class='pgbtn'>STATUS</button>
                            </div>
                            <div class="col-lg-4 d-flex justify-content-center">
                                <button class='pgbtn submitrecord'>SUBMIT</button>
                            </div>
                        </div>
                </div>
                
            <!--</div>-->
        </div>
        <script>
            window.onload = function() {
                document.getElementById("serialnum").focus();
            };

            $( "#bat2yr" ).click(function() {
                document.getElementById("batrepdate").focus();
            });
        </script>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    </body>
</html>