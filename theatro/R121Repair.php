<?php
    session_start();
    require_once('../getdata.php');
    require_once('../header.php');

    if(!isset($_SESSION['userlogin'])){
        header("Location: ../login.php");
    }

?>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NCA - THEATRO DATA AQUISITION</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/normalize.css">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" async defer></script>
        
        <script src="https://kit.fontawesome.com/1e6ad500ad.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="../assets/vendor/fonts/circular-std/style.css" >
        <link rel="stylesheet" href="../assets/libs/css/style.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="../assets/vendor/vector-map/jqvmap.css">
        <link rel="stylesheet" href="../assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <link href="../DataTables/datatables.min.css" rel="stylesheet">
        <script type="text/javascript" src="../DataTables/datatables.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script>
                
        </script>
    </head>
    <body>
        <?php header2(); ?>
        <!------------------------------------------------------->
        <!----              MAIN PAGE AREA                  ----->
        <!------------------------------------------------------->
        <div id="page-container">
            <div id="content-wrap" class="container-fluid batinspect">
                <div class="d-flex" style="margin-top:75px;"></div>
                <form id="intakeform" autocomplete="off" style="margin-bottom: 20px;">
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-5">
                            <div class="row justify-content-center">
                                <h1>R121 Modification</h1>
                                <div class="col-md-12 sectiontype">
                                    <div class="row">
                                        <div class="col-md-12 sectionhdr">
                                            <label for="serialnum" class="d-flex justify-content-center inputlabel sectionhdrlabel">Serial Number</label>
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="text" name="serialnum" id="serialnum" class="form-control input_user serialnuminput">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>   
                <div class="row d-flex justify-content-center">
                    <div class="col-md-6">
                        <div id="shippedunits" class="table-responsive table-wrapper-scroll-y my-custom-scrollbar">
                            <table id="R121tbl" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="th-sm">Date Inspected</th>
                                        <th class="th-sm">User ID</th>
                                        <th class="th-sm">Serial Number</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Date Inspected</th>
                                        <th>User ID</th>
                                        <th>Serial Number</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            Copyright © 2020 National Circuit Assembly. All rights reserved.
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!------------------------------------------------------->
        <!----            END MAIN PAGE AREA                ----->
        <!------------------------------------------------------->
        
        <script>
            $( document ).ready(function() {
                $("#R121tbl").DataTable({
                    "scrollY"       : "400px",
                    "scrollCollapse": true,
                    "order"         : [[ 0, "desc" ]],
                    "pagingType"    : "first_last_numbers",
                    "ajax" : {
                        "url": "../theatro/jsR121table.php",
                        "dataSrc" : "" 
                    },
                    "columns" : [
                        {"data": "Date Inspected"},
                        {"data": "User ID"},
                        {"data": "Serial Number"}
                    ]
                });
                $('.dataTables_length').addClass('bs-select');
                
                document.getElementById("serialnum").focus();
                $('form input').on('keypress', function(e) {
                    return e.which !== 13;
                });
                
                $( "#serialnum" ).bind("keypress", function() {
                    setTimeout(function(){
                        document.getElementById("datecode").focus();
                    }, 500);
                });

                $( "#serialnum" ).change(function() {
                    $('#serialnum').removeClass( "inputerror" );
                });


                $('#serialnum').keypress(function(e){
                    if(e.which == 13){

                        e.preventDefault();

                        //var valid = this.form.checkValidity();

                        var d = new Date(); 
                        var month = d.getMonth()+1;
                        var day = d.getDate();
                        
                        var datetime = d.getFullYear() + "-"  
                                    + ((''+month).length<2 ? '0' : '') + month + "-" 
                                    + ((''+day).lenth<2 ? '0' : '') + day + " "
                                    + d.getHours() + ":"  
                                    + d.getMinutes() + ":" 
                                    + d.getSeconds();
                        
                        var serialnum = $.trim($('#serialnum').val());
                        var creationdate = datetime;

                        //$(".inputerror").remove();

                        if(serialnum.length <1){
                            document.getElementById("serialnum").focus();
                            $('#serialnum').addClass( "inputerror" );
                            alert("Please enter a Serial Number");
                        }else if(serialnum.length >5){
                            document.getElementById("serialnum").focus();
                            $('#serialnum').addClass( "inputerror" );
                            alert("Please enter a Correct Serial Number");
                        }else{
                            
                            $.ajax({
                                type: 'POST',
                                url: '../theatro/jsr121.php',
                                data: {
                                    serialnum: serialnum, 
                                    creationdate: creationdate
                                },
                                success: function(data) {
                                    //alert('SUCCESSFULLY ADDDED RECORD');
                                    if(data.length >0){
                                        alert(data);
                                        document.getElementById("serialnum").focus();
                                    }else{
                                        document.getElementById("serialnum").focus();
                                        $('#serialnum').val('');
                                        $('#serialnum').removeClass( "inputerror" );
                                        $('#R121tbl').DataTable().ajax.reload();
                                    }
                                    
                                },
                                error: function(data){
                                    alert('Data failed to post.');
                                }
                            });
                        }
                        return false;
                    }
                });
            });
        </script>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    </body>
</html>