<?php
session_start();
require_once('../config.php');

    global $db;
    $batterytypes = ['NFF', 'Swollen', 'Punctured', 'Wrinkled', 'Broken Wire', 'No Battery - NCA', 'No Battery - Theatro', 'Cannot Charge', 'Battery 2+ yrs'];
    $arraylength = count($batterytypes);

    $data = array();

    $damage = "";
    $yok = "";
    $as = "";

    for ($i = 0; $i < count($batterytypes); $i++) {
        //echo $batterytypes[$i];
        $batstat = $batterytypes[$i];
        $ASbattype = "A&S";
        $YOKbattype = "YOK";

        //if($batstat != "Battery 2+ yrs"){

            $YOKsql = "SELECT * FROM battery_inspection WHERE batterytype = ? and batstatus = ?";
            $stmtselect = $db->prepare($YOKsql);
            $result1 = $stmtselect->execute([$YOKbattype, $batstat]);

            $yok = $stmtselect->rowCount();

            $ASsql = "SELECT * FROM battery_inspection WHERE batterytype = ? and batstatus = ?";
            $stmtselect = $db->prepare($ASsql);
            $result2 = $stmtselect->execute([$ASbattype, $batstat]);

            $as = $stmtselect->rowCount();

        $data[$i] = ['type'=>$batstat, 'yok'=>$yok, 'as'=>$as];
    }


    print json_encode($data);

    ?>
