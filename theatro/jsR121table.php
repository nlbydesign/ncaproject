<?php
    session_start();
    require_once('../config.php');

    $sql = "SELECT * FROM unit_r121 ORDER BY datescanned DESC LIMIT 2000";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();
    $table_data = array();
    if($stmtselect->rowCount() > 0){
        while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) {
            $table_data[] = array(
                'Date Inspected' => $rowitems['datescanned'],
                'User ID'  => $rowitems['userid'],
                'Serial Number'   => $rowitems['serialnum']
            );
        }
    }
    echo json_encode($table_data);
?>