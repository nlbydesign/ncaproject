<?php
    session_start();

    if(!isset($_SESSION['userlogin'])){
        header("Location: ../login.php");
    }

    if(isset($_GET['logout'])){
        session_destroy();
        unset($_SESSION);
        header("Location: ../login.php");
    }
?>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NCA - THEATRO DATA AQUISITION</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
        <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <!--and back again<script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>-->
        <script src="https://kit.fontawesome.com/1e6ad500ad.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script>
            $(function() {
                $( "#datepicker" ).datepicker();
                $( "#datepicker2" ).datepicker();
            });
        </script>
    </head>
    <body>
        <div class="container-fluid headerdiv">
            <div class="row">
                <div class="col-md-10">
                <img src="img/nca_main_logo.png" class="header_logo" alt="National Circuit Assembly Logo">
                </div>
                <div class="col-md-2 header_text">
                <a href="admin.php?logout=true">Logout</a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2" style="background:blue;">
                    <a href="" class="btn pull-right">Export Data</a>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="startdate">Start Date</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" id='datepicker' name='startdate'>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="enddate">End Date</label>
                        </div>
                        <div class="col-md-8">    
                            <input type="text" id='datepicker2' name='enddate'>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div style="margin-left: 25px">
                        <a href="exportData.php" class="btn btn-success pull-right">Export Data</a>
                    </div>
                </div>
            </div>
        </div>


    </body>
</html>