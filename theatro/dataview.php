<?php
session_start();
require_once('../config.php');


$startdate = strtotime($_POST['start_date']);
$enddate = strtotime($_POST['end_date']);
$tablename = $_POST['gettablename'];

$sdate = date('Y-m-d', $startdate);
$edate = date('Y-m-d', $enddate);

$data = array();

$sql = "SELECT * FROM ? WHERE datescanned BETWEEN ? AND ? ORDER BY datescanned ASC";
$stmtselect = $db->prepare($sql);
$result = $stmtselect->execute([$tablename, $sdate, $edate]);

if($result){
    $data1 = $stmtselect->fetch(PDO::FETCH_ASSOC);
    $totrows = $stmtselect->rowCount();
    if($stmtselect->rowCount() > 0){
        while($row=$stmtselect->fetch()){
            $data[]  = ['inspectionid'=>$row['inspectionid'], 
                        'dateinspected'=>$row['datescanned'], 
                        'userid'=>$row['userid'], 
                        'serialnum'=>$row['serialnum'], 
                        'batstatus'=>$row['batstatus'], 
                        'batterytype'=>$row['batterytype'], 
                        'battery2yrs'=>$row['battery2yrs'], 
                        'datecode'=>$row['datecode'], 
                        'rpmtdatecode'=>$row['rpmtdatecode'], 
                        'rpmtdatecode'=>$row['rpmtdatecode']
        ];
        }
        
    }
}

print json_encode($data);

?>