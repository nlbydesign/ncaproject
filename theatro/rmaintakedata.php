<?php
    session_start();
    require_once('../getdata.php');
    require_once('../header.php');

    if(!isset($_SESSION['userlogin'])){
        header("Location: ../login.php");
    }

?>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NCA - THEATRO DATA AQUISITION</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" async defer></script>
        
        <script src="https://kit.fontawesome.com/1e6ad500ad.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="../assets/vendor/fonts/circular-std/style.css">
        <link rel="stylesheet" href="../assets/libs/css/style.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="../assets/vendor/vector-map/jqvmap.css">
        <link rel="stylesheet" href="../assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <!--<link href="DataTables/datatables.min.css" rel="stylesheet">
        <script type="text/javascript" src="DataTables/datatables.min.js"></script>-->
        <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script>
                
        </script>
    </head>
    <body>
        <?php header2(); ?>
        <!------------------------------------------------------->
        <!----              MAIN PAGE AREA                  ----->
        <!------------------------------------------------------->
        <div id="page-container">
            <div id="content-wrap" class="container-fluid batinspect">
                <div class="d-flex" style="margin-top:75px;"></div>
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-9">
                            <div class="row justify-content-center">
                                <h1>RMA INTAKE</h1>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-3 sectiontype">
                                    <div class="row">
                                        <div class="col-md-12 sectionhdr">
                                            <label for="serialnum" class="d-flex justify-content-center inputlabel sectionhdrlabel">RMA Request #</label>
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="text" name="workorder" id="workorder" class="form-control input_user serialnuminput">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 sectiontype">
                                    <div class="row">
                                        <div class="col-md-12 sectionhdr">
                                            <label for="sdate" class="d-flex justify-content-center inputlabel sectionhdrlabel">RMA Request Date</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="text" id='rmadate' name='rmadate' class="form-control input_user serialnuminput input-daterange" value="<?php //$results = getDefaultData(); echo $results[0];?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 sectiontype">
                                    <div class="row">
                                        <div class="col-md-12 sectionhdr">
                                            <label for="serialnum" class="d-flex justify-content-center inputlabel sectionhdrlabel">Serial Number</label>
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form id="intakeform" style="margin-bottom: 20px;" autocomplete="off">
                                                <input type="text" name="serialnum" id="serialnum" class="form-control input_user serialnuminput">
                                                <input type="text" name="rmadate1" id="rmadate1" class="form-control input_user serialnuminput" hidden>
                                            </form>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="row d-flex justify-content-center">
                    <div class="col-md-8">
                        <div id="shippedunits" class="table-responsive table-wrapper-scroll-y my-custom-scrollbar">
                            <table id="rmatbl" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr role="row">
                                        <th class="th-sm">RMA Request #</th>
                                        <th class="th-sm">RMA Request Date</th>
                                        <th class="th-sm">Date Received</th>
                                        <th class="th-sm">Ship Date</th>
                                        <th class="th-sm">Serial Number</th>
                                        <th class="th-sm">Status</th>
                                        <th class="th-sm">User ID</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>RMA Request #</th>
                                        <th>RMA Request Date</th>
                                        <th>Date Received</th>
                                        <th>Ship Date</th>
                                        <th>Serial Number</th>
                                        <th>Status</th>
                                        <th>User ID</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            Copyright © 2020 National Circuit Assembly. All rights reserved.
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!------------------------------------------------------->
        <!----            END MAIN PAGE AREA                ----->
        <!------------------------------------------------------->
        
        <script>
            $( document ).ready(function() {
                $("#rmatbl").DataTable({
                    "scrollY"       : "400px",
                    "scrollCollapse": true,
                    "order"         : [[ 0, "desc" ]],
                    "pagingType"    : "first_last_numbers",
                    "ajax" : {
                        "url": "../theatro/jsrmatable.php",
                        "dataSrc" : "" 
                    },
                    "columns" : [
                        {"data": "Work Order"},
                        {"data": "RMA Date"},
                        {"data": "Input Date"},
                        {"data": "Ship Date"},
                        {"data": "Serial Num"},
                        {"data": "Status"},
                        {"data": "User ID"}
                    ]
                });

                $('.dataTables_length').addClass('bs-select');

                $('.input-daterange').datepicker({
                    todayBtn:'linked',
                    format: "yyyy-mm-dd",
                    autoclose:true
                });

                document.getElementById("workorder").focus();

                $('form input').on('keypress', function(e) {
                    return e.which !== 13;
                });
            
                $( "#serialnum" ).change(function() {
                    $('#serialnum').removeClass( "inputerror" );
                });

                $( "#serialnum" ).change(function() {
                    $('#serialnum').removeClass( "inputerror" );
                });

                $( "#rmadate" ).change(function() {
                    $rma = $("#rmadate").val();
                    $("#rmadate1").val($rma);
                    $('#rmadate').removeClass( "inputerror" );
                    document.getElementById("serialnum").focus();
                });

                $('#serialnum').keypress(function(e){
                    if(e.which == 13){

                        e.preventDefault();

                        //var valid = this.form.checkValidity();

                        var d = new Date(); 
                        var month = d.getMonth()+1;
                        var day = d.getDate();
                        
                        var datetime = d.getFullYear() + "-"  
                                    + ((''+month).length<2 ? '0' : '') + month + "-" 
                                    + ((''+day).lenth<2 ? '0' : '') + day + " "
                                    + d.getHours() + ":"  
                                    + d.getMinutes() + ":" 
                                    + d.getSeconds();
                        
                        var rmaorder = $.trim($('#workorder').val());
                        var rmadate = $.trim($('#rmadate1').val());
                        var serialnum = $.trim($('#serialnum').val());
                        var creationdate = datetime;
                        
                        //$(".inputerror").remove();
                        if(rmaorder.length <1){
                            document.getElementById("workorder").focus();
                            $('#workorder').addClass( "inputerror" );
                            alert("Please enter a RMA Request Number");
                        }else if(rmadate.length <1){
                            document.getElementById("rmadate").focus();
                            $('#rmadate').addClass( "inputerror" );
                            alert("Please enter a RMA Request Date");
                        }else if(serialnum.length <1){
                            document.getElementById("serialnum").focus();
                            $('#serialnum').addClass( "inputerror" );
                            alert("Please enter a Serial Number");
                        }else{
                            
                            $.ajax({
                                type: 'POST',
                                url: '../theatro/jsintake.php',
                                data: {
                                    rmaorder: rmaorder,
                                    rmadate: rmadate,
                                    serialnum: serialnum, 
                                    creationdate: creationdate
                                },
                                success: function(data) {
                                    //alert('SUCCESSFULLY ADDDED RECORD');
                                    //$("#shippedunits").load(location.href+" #shippedunits>*","");
                                    document.getElementById("serialnum").focus();
                                    $('#serialnum').val('');
                                    $('#serialnum').removeClass( "inputerror" );
                                    $('#rmatbl').DataTable().ajax.reload();
                                },
                                error: function(data){
                                    alert('Data failed to post.');
                                }
                            });
                        }
                        return false;
                    }
                });
            });
        </script>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    </body>
</html>