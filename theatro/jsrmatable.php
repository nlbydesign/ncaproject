<?php
    session_start();
    require_once('../config.php');

    $sql = "SELECT * FROM rmaintake ORDER BY datescanned DESC LIMIT 2000";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();
    $table_data = array();
    if($stmtselect->rowCount() > 0){
        while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) {
            $inputdate = date_create($rowitems['datescanned']); 
            $inputdate = date_format($inputdate, 'm/d/Y');
            if($rowitems['shipdate'] != ''){
                $shipdate = date_create($rowitems['shipdate']); 
                $shipdate = date_format($shipdate, 'm/d/Y');
            }else{
                $shipdate = "";
            }
            $rmarequestdate = date_create($rowitems['rmarequestdate']); 
            $rmarequestdate = date_format($rmarequestdate, 'm/d/Y');
            
            $table_data[] = array(
                'Work Order' => $rowitems['workorder'],
                'RMA Date'   => $rmarequestdate,
                'Input Date' => $inputdate,
                'Ship Date'  => $shipdate,
                'Serial Num' => $rowitems['serialnum'],
                'Status'    => $rowitems['status'],
                'User ID'    => $rowitems['userid']
            );
        }
    }
    echo json_encode($table_data);
?>