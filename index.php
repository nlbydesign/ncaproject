<?php
    session_start();
    require_once('header.php');
    require_once('getdata.php');

    if(!isset($_SESSION['userlogin'])){
        header("Location: login.php");
    }

    if(isset($_GET['logout'])){
        session_unset();
        session_destroy();
        header("Location: login.php");
    }

?>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NCA - THEATRO DATA AQUISITION</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="css/normalize.css">
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" async defer></script>
        
        <script src="https://kit.fontawesome.com/1e6ad500ad.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/libs/css/style.css">
        <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="assets/vendor/vector-map/jqvmap.css">
        <link rel="stylesheet" href="assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
        <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script>
            
        </script>
    </head>
    <body>
        <?php header1(); ?>
        <div class="container h-100">
            <div class="d-flex justify-content-center h-100">
                <div class="row" style="margin-top:15%;">
                    <?php 
                        $getprodline = $_SESSION['prodline'];
                        $getcomp1 = "Theatro";
                        $getcomp2 = "UltraVision";
                        $getcomp3 = "Eagle";
                        $getcomp4 = "All";
                        $getstring1 = strpos($getprodline, $getcomp1);
                        $getstring2 = strpos($getprodline, $getcomp2);
                        $getstring3 = strpos($getprodline, $getcomp3);
                        $getstring4 = strpos($getprodline, $getcomp4);

                        if($getstring1 !== false || $getstring4 !== false) { ?>
                        <div class="col-sm-12 col-md-3 col-lg-3" style="margin: 0 10px;">
                            <div class="user_card">
                                <div class="row">
                                    <div class="d-flex justify-content-center col-md-12">
                                        <h2>THEATRO</h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php getTheatroprodmenu(); ?>
                                </div>
                            </div>
                        </div>
                        <?php 
                        }
                        if($getstring2 !== false || $getstring4 !== false) { ?>
                        <div class="col-sm-12 col-md-3 col-lg-3" style="margin: 0 10px;">
                            <div class="user_card">
                                <div class="row">
                                    <div class="d-flex justify-content-center col-md-12">
                                        <h2>ULTRAVISION</h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php getUltraVisionprodmenu(); ?>
                                </div>
                            </div>
                        </div>
                        <?php 
                        } if($getstring3 !== false || $getstring4 !== false) { ?>
                        <div class="col-sm-12 col-md-3 col-lg-3" style="margin: 0 10px;">
                            <div class="user_card">
                                <div class="row">
                                    <div class="d-flex justify-content-center col-md-12">
                                        <h2>EAGLE</h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php getEagleprodmenu(); ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </body>
</html>