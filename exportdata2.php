<?php
//include database configuration file
require_once('config.php');

$startdate = strtotime($_POST['start_date']);
$enddate = strtotime($_POST['end_date']);
$tablename = $_POST['tablename'];
$type = $_POST['radioValue'];

$sdate = date('Y-m-d', $startdate);
$edate = date('Y-m-d', $enddate);

if ($tablename == 'shippingdata'){
    $datefield = "shippingdate";
}else{
    $datefield = "datescanned";
}

$columns = "SELECT `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='ncadata' AND `TABLE_NAME`='".$tablename."'";
$stmt = $db->prepare($columns);
$r = $stmt->execute();
$table = array();
if($stmt->rowCount() > 0){
    while ( $row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $table[] = $row['COLUMN_NAME'];
    }
}

//get records from database
if($type === "all"){
    $query = "SELECT * FROM ".$tablename." WHERE ".$datefield." BETWEEN '".$sdate." 00:00:00' AND '".$edate." 23:59:59' ORDER BY ".$datefield." ASC";
}else{
    $query = "SELECT ".$tablename.".* FROM (SELECT serialnum, MAX(datescanned) AS datescanned FROM ".$tablename." WHERE ".$datefield." BETWEEN '".$sdate." 00:00:00' AND '".$edate." 23:59:59'  GROUP BY serialnum) AS latest_orders INNER JOIN ".$tablename." ON ".$tablename.".serialnum = latest_orders.serialnum AND ".$tablename.".datescanned = latest_orders.datescanned ORDER BY ".$datefield." ASC";
}

$stmtselect = $db->prepare($query);
$result = $stmtselect->execute();
$table_data = array();
if($stmtselect->rowCount() > 0){
    while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) {
        for ($i = 0; $i < count($table); $i++) {
            $table_data[] = array(
                $table[$i] => $rowitems[$table[$i]],
            );
        }
    }
}
echo json_encode($table_data);
?>