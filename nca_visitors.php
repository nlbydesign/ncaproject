<?php
    session_start();
    require_once('getdata.php');

    //if(isset($_POST["submit"])){
    //    $var = $_POST["productline"];
    //    $newvar = implode(",", $var);
    //}
?>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NCA - VISITOR SIGN IN</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" async defer></script>
        
        <script src="https://kit.fontawesome.com/1e6ad500ad.js" crossorigin="anonymous"></script>
        <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
        <style>
            /* The container */
            .uscontainer {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-top: 12px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 1.25rem;
            color: lightgray;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            font-weight: 500;
            line-height: 1.2;
            }

            /* Hide the browser's default checkbox */
            .uscontainer input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
            }

            /* Create a custom checkbox */
            .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
            border-radius: 0.3rem 0.3rem 0.3rem 0.3rem !important;
            }

            /* On mouse-over, add a grey background color */
            .uscontainer:hover input ~ .checkmark {
            background-color: #ccc;
            }

            /* When the checkbox is checked, add a blue background */
            .uscontainer input:checked ~ .checkmark {
            background-color: #ee6534;
            }

            /* Create the checkmark/indicator (hidden when not checked) */
            .checkmark:after {
            content: "";
            position: absolute;
            display: none;
            }

            /* Show the checkmark when checked */
            .uscontainer input:checked ~ .checkmark:after {
            display: block;
            }

            /* Style the checkmark/indicator */
            .uscontainer .checkmark:after {
            left: 9px;
            top: 4px;
            width: 8px;
            height: 15px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
            
            }
            
            .hide{
                display: none !important;
            }

            .optbtn {
                height: 75px;
                width: 250px;
                background: white;
            }
        </style>
    </head>
    <body>
        <div class="container h-100">
            <div class="d-flex justify-content-center h-100">
                <div class="user_card2">
                    <div class="d-flex justify-content-center">
                        <div class="brand_logo_container">
                            <img src="img/nca_main_logo.png" class="brand_logo" alt="National Circuit Assembly Logo">
                        </div>
                    </div>
                    <div id="options" class="d-flex justify-content-center form_container" style="margin-bottom:75px;">
                        <div class="row col-12">
                            <div class="col-md-6 d-flex justify-content-center"><button id="checkin" class="optbtn">SIGN IN</button></div>
                            
                            <div class="col-md-6 d-flex justify-content-center"><button id="checkout" class="optbtn">SIGN OUT</button></div>
                        </div>
                            
                    </div>
                    <div id="signout" class="col-md-12 d-flex justify-content-center">
                        Good Bye
                    </div>
                    <div id="formdata" class="col-md-12 d-flex justify-content-center" style="margin-top:75px;">
                        <div class="col-md-10">
                            <label class="errorcode"></label>
                            <form autocomplete="off">
                                <div class="input-group mb-3 signupinput sulabel">
                                    <h5 style="color: lightgray;">Full Name</h5>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" name="name" id="name" class="form-control input_user" style="margin-right:2px;" required>
                                </div>
                                <div class="input-group mb-3 signupinput sulabel">
                                    <h5 style="color: lightgray;">Company Name</h5>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="text" name="company" id="company" class="form-control input_user" style="margin-right:2px;" required>
                                </div>

                                <div class="input-group mb-3 signupinput sulabel">
                                    <h5 style="color: lightgray;">Reason For Visit</h5>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="col-lg-6 col-md-6 col-sm-6" style="padding: 0 0 !important;">
                                        <?php getvisittype(); ?>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="row" style="margin: 0 0 10px 0;">
                                            <label class="uscontainer">US Citizen?
                                                <input type="checkbox" name="uscitizen" id="uscitizen" value="Yes">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-group mb-3 signupinput sulabel">
                                        <h5 style="color: lightgray;">Badge ID</h5>
                                    </div>
                                    <div class="input-group mb-3">
                                        <input type="text" name="badge" id="badge" class="form-control input_user" style="margin-right:2px;" required>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-center login_container" style="padding-bottom: 25px;">
                                    <div class="row" style="width:100%">
                                        
                                        <div class="col-md-8 d-flex justify-content-center">
                                            <button type="button" name="button" id="signup" class="btn login_btn" style="height:75px;">
                                                CHECK IN
                                            </button>
                                        </div>
                                        <div class="col-md-3 d-flex justify-content-center">
                                            <a href='nca_visitors.php' style="margin:25px;">BACK</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div id="checkouttbl" style="margin-top: 50px;">
                        <center><h2 style="color:lightgray;">Currently Checked In Visitors:</h2></center>
                        <div class="col-md-12">
                            <div id="shippedunits" class="table-responsive table-wrapper-scroll-y my-custom-scrollbar">
                                <table id="guesttbl" class="table table-striped table-bordered" cellspacing="0" width="100%" style="background:white; margin-bottom: 0px !important;" hidden>
                                    <thead>
                                        <tr>
                                            <th class="th-sm">ID</th>
                                            <th class="th-sm">Name</th>
                                            <th class="th-sm">Time In</th>
                                            <th class="th-sm">Time Out</th>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Time In</th>
                                        <th>Time Out</th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $( '#signout' ).addClass('hide');
                $( '#checkouttbl' ).addClass('hide');
                $( '#formdata' ).addClass('hide');
                $('#checkin').click(function(){
                        $('#formdata').removeClass('hide');
                        $( '#options' ).addClass('hide');
                        $('#checkouttbl').addClass('hide');
                });
                
                $('#checkout').click(function(){
                        $('#checkouttbl').removeClass('hide');
                        $( '#options' ).addClass('hide');
                        $('#formdata').addClass('hide');

                        $('#guesttbl').removeAttr('hidden'); 

                        var table = $("#guesttbl").DataTable({
                            //"scrollY"       : "400px",
                            "paging":   false,
                            "ordering": false,
                            "info":     false,
                            "scrollCollapse": false,
                            "order"         : [[ 2, "asc" ]],
                            "ajax" : {
                                "url": "jsguesttable.php",
                                "dataSrc" : "" 
                            },
                            "columns" : [
                                {"data": "ID"},
                                {"data": "Name"},
                                {"data": "Time In"},
                                {"data": "Time Out"},
                            ],
                            rowId: 'ID',
                            select: true,
                            searching: false,
                            
                        });
                        $('.dataTables_length').addClass('bs-select');
                

                        $('#guesttbl tbody').on( 'click', 'tr', function () {
                            //$(this).toggleClass('selected');
                            var act = $(this).toggleClass('active').hasClass('active');
                            var linedata = table.row( this ).data();
                            //var wo = $.trim($('#workordernum').val());
                            var id = linedata.ID;
                            var visitor = linedata.Name;
                            
                            var d = new Date(); 
                            var month = d.getMonth()+1;
                            var day = d.getDate();
                            
                            var datetime = d.getFullYear() + "-"  
                                        + ((''+month).length<2 ? '0' : '') + month + "-" 
                                        + ((''+day).lenth<2 ? '0' : '') + day + " "
                                        + d.getHours() + ":"  
                                        + d.getMinutes() + ":" 
                                        + d.getSeconds();

                            var creationdate = datetime;
                            
                            //alert('Guest ID: ' + id + ', Visitor: ' + visitor);

                            $.ajax({
                                type: 'POST',
                                url: 'jsvisitorout.php',
                                data: {
                                    id: id, 
                                    visitor: visitor,
                                    cdate: creationdate
                                },
                                success: function(data) {
                                    //alert(data);
                                    window.location.href = "nca_visitors.php";
                                },
                                error: function(data){
                                    //alert(data);
                                }
                            });


                        });
                        
                });

                $('#signup').click(function(e){
                    
                    var valid = this.form.checkValidity();

                    var name = $.trim($('#name').val());
                    var company = $.trim($('#company').val());
                    var reason1 = $.trim($('#reason1').val());
                    var badge = $.trim($('#badge').val());
                    
                    if ($("#uscitizen").is(":checked")) {
                        var failval = $("#uscitizen").val();
                    }else{
                        var failval = "No";
                    }
                    
                    if(name.length <1){
                        document.getElementById("name").focus();
                        $('#name').addClass( "inputerror" );
                        alert("Please Enter Your Name");
                    }else if(company.length <1){
                        document.getElementById("company").focus();
                        $('#company').addClass( "inputerror" );
                        alert("Please Enter Your Company Name");
                    }else if(reason1.length <1){
                        document.getElementById("reason1").focus();
                        $('#reason1').addClass( "inputerror" );
                        alert("Please Enter The Reason For Your Visit");
                    }else if(badge.length <1){
                        document.getElementById("badge").focus();
                        $('#badge').addClass( "inputerror" );
                        alert("Please Enter Your Badge ID");
                    }else{
                    
                        e.preventDefault();

                        $.ajax({
                            type: 'POST',
                            url: 'jsvisitors.php',
                            data: {
                                name: name, 
                                company: company, 
                                reason1: reason1, 
                                uscitizen: failval,
                                badge: badge
                            },
                            success: function(data) {
                                //alert(data);
                                window.location.href = "nca_visitors.php";
                            },
                            error: function(data){
                                //alert(data);
                            }
                        });
                    }
                });
            });
        </script>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    </body>
</html>