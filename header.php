<?php 
require_once('getdata.php');

function header1() {
    ?>
    <div class="dashboard-header">
        <nav class="navbar navbar-expand-lg bg-white fixed-top">
            <img src="img/nca_main_logo.png" class="header_logo" alt="National Circuit Assembly Logo">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto navbar-right-top">
                    <li class="nav-item">
                        <div id="custom-search" class="top-search-bar">
                            <input class="form-control" type="text" placeholder="Search..">
                        </div>
                    </li>
                    <li class="nav-item dropdown nav-user">
                        <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="assets/images/avatar-1.jpg" alt="" class="user-avatar-md rounded-circle"></a>
                        <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                            <div class="nav-user-info">
                                <h5 class="mb-0 text-white nav-user-name"><?php $results = getUserInfo($_SESSION['userlogin']); echo $results[0] . " " . $results[1];?></h5>
                            </div>
                            <div>
                                <h5 style="padding-left:12px;"><?php $results = getUserInfo($_SESSION['userlogin']); echo $results[2];?></h5>
                            </div>
                            <?php //getprodmenu(); ?>
                            <a class="dropdown-item" href="logout.php"><i class="fas fa-power-off mr-2"></i>Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <?php 
}

function header2() {
    ?>
    <div class="dashboard-header">
        <nav class="navbar navbar-expand-lg bg-white fixed-top">
            <img src="../img/nca_main_logo.png" class="header_logo" alt="National Circuit Assembly Logo">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto navbar-right-top">
                    <li class="nav-item">
                        <div id="custom-search" class="top-search-bar">
                            <input class="form-control" type="text" placeholder="Search..">
                        </div>
                    </li>
                    <li class="nav-item dropdown nav-user">
                        <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/images/avatar-1.jpg" alt="" class="user-avatar-md rounded-circle"></a>
                        <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                            <div class="nav-user-info">
                                <h5 class="mb-0 text-white nav-user-name"><?php $results = getUserInfo($_SESSION['userlogin']); echo $results[0] . " " . $results[1];?></h5>
                            </div>
                            <div>
                                <h5 style="padding-left:12px;"><?php $results = getUserInfo($_SESSION['userlogin']); echo $results[2];?></h5>
                            </div>
                            <?php  
                            $results = getUserInfo($_SESSION['userlogin']); 
                            $tmenu = "Theatro";
                            $uvmenu = "UltraVision";
                            $emenu = "Eagle";
                            if( strpos( $results[3], $tmenu ) !== false) {
                                getTheatroprodmenu(); 
                            }
                            if( strpos( $results[3], $uvmenu ) !== false) {
                                getUltraVisionprodmenu(); 
                            }
                            if( strpos( $results[3], $emenu ) !== false) {
                                getEagleprodmenu(); 
                            }
                                
                            ?>
                            <a class="dropdown-item" href="../logout.php"><i class="fas fa-power-off mr-2"></i>Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <?php 
}


?>