<?php
    session_start();
    require_once('../getdata.php');
    require_once('../header.php');

    if(!isset($_SESSION['userlogin'])){
        header("Location: ../login.php");
    }

?>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NCA - SMD LINE DATA AQUISITION</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/normalize.css">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" async defer></script>
        
        <script src="https://kit.fontawesome.com/1e6ad500ad.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="../assets/vendor/fonts/circular-std/style.css" >
        <link rel="stylesheet" href="../assets/libs/css/style.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="../assets/vendor/vector-map/jqvmap.css">
        <link rel="stylesheet" href="../assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link href="../DataTables/datatables.min.css" rel="stylesheet">
        <script type="text/javascript" src="../DataTables/datatables.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script>
            

        </script>
    </head>
    <body>
        <?php header2(); ?>
        <!------------------------------------------------------->
        <!----              MAIN PAGE AREA                  ----->
        <!------------------------------------------------------->
        <div id="page-container">
            <div id="content-wrap" class="container-fluid batinspect">
                <div class="d-flex" style="margin-top:75px;"></div>
                <!-- DELETE MODAL -->
                <div id="dialog-confirm" title="REMOVE FROM LOT?" style="display:none;">
                    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span></p>
                </div>
                <!-------END MODAL--------------->
                <!--- ADD ITEM MODAL ------------>
                <div id="dialog-form" title="Add New Item">
                    <p class="validateTips">All form fields are required.</p>
                    
                    <form>
                        <fieldset>
                        <table style="width:100%;">
                            <tr hidden>
                                <td style="width:30%;">
                                    <label for="wonum">ID</label>
                                </td>
                                <td style="width:70%;">
                                    <input type="text" name="id" id="id" class="text ui-widget-content ui-corner-all" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;">
                                    <label for="wonum">Work Order #</label>
                                </td>
                                <td style="width:70%;">
                                    <input type="text" name="wonum" id="wonum" class="text ui-widget-content ui-corner-all" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;">
                                    <label for="mfgpartnum">Mfg Part #</label>
                                </td>
                                <td style="width:70%;">
                                    <input type="text" name="mfgpartnum" id="mfgpartnum" class="text ui-widget-content ui-corner-all" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;">
                                    <label for="ncapartnum">NCA Part #</label>
                                </td>
                                <td style="width:70%;">
                                    <input type="text" name="ncapartnum" id="ncapartnum" class="text ui-widget-content ui-corner-all" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;">
                                    <label for="datecode">Date Code</label>
                                </td>
                                <td style="width:70%;">
                                    <input type="text" name="datecode" id="datecode" class="text ui-widget-content ui-corner-all">
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;">
                                    <label for="lotcode">Lot Code</label>
                                </td>
                                <td style="width:70%;">
                                    <input type="text" name="lotcode" id="lotcode" class="text ui-widget-content ui-corner-all">
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;">
                                    <label for="mfg">Manufacturer</label>
                                </td>
                                <td style="width:70%;">
                                    <input type="text" name="mfg" id="mfg" class="text ui-widget-content ui-corner-all" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;">
                                    <label for="mfg">Active?</label>
                                </td>
                                <td style="width:70%;">
                                    <input type="text" name="active" id="active" class="text ui-widget-content ui-corner-all" value="active" >
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;">
                                    <label for="mfg">Rack</label>
                                </td>
                                <td style="width:70%;">
                                    <input type="text" name="rack" id="rack" class="text ui-widget-content ui-corner-all">
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;">
                                    <label for="mfg">Slot</label>
                                </td>
                                <td style="width:70%;">
                                    <input type="text" name="slot" id="slot" class="text ui-widget-content ui-corner-all">
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;">
                                    <label for="mfg">Side</label>
                                </td>
                                <td style="width:70%;" >
                                    <select id="side" style="margin-top:8px; width: 80%;" > 
                                        <option  value="">--Select--</option>
                                        <option  value="Top">Top</option>
                                        <option  value="Bottom">Bottom</option>
                                        <option  value="Both">Top/Bottom</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;" >
                                    <label for="client">Client</label>
                                </td>
                                <td style="width:70%;">
                                    <?php getclientlist('disabled'); ?>
                                </td>
                            </tr>
                            <tr hidden>
                                <td style="width:30%;">
                                    <label for="mfg">Action</label>
                                </td>
                                <td style="width:70%;">
                                    <input type="text" name="action" id="action" class="text ui-widget-content ui-corner-all">
                                </td>
                            </tr>
                            
                        </table>
                        
                        <!-- Allow form submission with keyboard without duplicating the dialog button -->
                        <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                        </fieldset>
                    </form>
                </div>
                <!------- END MODAL ------------->
                <!--<form id="intakeform" autocomplete="off" style="margin-bottom: 20px;">-->
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-10">
                            <div class="row justify-content-center">
                                <h1>STAGING</h1>
                            </div>
                            <div class="row justify-content-center">
                                
                                <div class="col-md-3 sectiontype">
                                    <div class="row">
                                        <div class="col-md-12 sectionhdr">
                                            <label for="workordernum" class="d-flex justify-content-center inputlabel sectionhdrlabel">Work Order Number</label>
                                        </div>
                                    </div>    
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-md-12 d-flex justify-content-center">
                                            <input type="text" name="workordernum" id="workordernum" class="form-control input_user serialnuminput">
                                        </div>
                                    </div>
                                </div>
                                
                                <!-- <div class="col-md-2 sectiontype">
                                    <div class="row">
                                        <div class="col-md-12 sectionhdr">
                                            <label for="side" class="d-flex justify-content-center inputlabel sectionhdrlabel">Add Item</label>
                                        </div>
                                    </div>   
                                    <div class="row">
                                        <div class="col-md-12 d-flex justify-content-center">
                                                <button id='create-item' name='create-item' class="addrecord" ><i class="fas fa-plus mr-2 "></i>ITEM</button>
                                        </div>
                                        
                                    </div>
                                </div> -->
                                
                            </div>
                        </div>
                    </div>
                <!--</form>   -->
                <div class="row d-flex justify-content-center">
                    <div class="col-md-10">
                        <div id="shippedunits" class="table-responsive table-wrapper-scroll-y my-custom-scrollbar">
                            <table id="linetbl" class="table table-striped table-bordered" cellspacing="0" width="100%" hidden>
                                <thead>
                                    <tr>
                                        <th class="th-sm"></th>
                                        <th class="th-sm">ID</th>
                                        <th class="th-sm">Mfg Part #</th>
                                        <th class="th-sm">NCA Part #</th>
                                        <th class="th-sm">Date Code</th>
                                        <th class="th-sm">Lot Code</th>
                                        <th class="th-sm">Manufacturer</th>
                                        <th class="th-sm">Work Order</th>
                                        <th class="th-sm">Active?</th>
                                        <th class="th-sm">Rack</th>
                                        <th class="th-sm">Slot</th>
                                        <th class="th-sm">Side</th>
                                        <th class="th-sm">Client</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>ID</th>
                                        <th>Mfg Part #</th>
                                        <th>NCA Part #</th>
                                        <th>Date Code</th>
                                        <th>Lot Code</th>
                                        <th>Manufacturer</th>
                                        <th>Work Order</th>
                                        <th>Active?</th>
                                        <th>Rack</th>
                                        <th>Slot</th>
                                        <th>Side</th>
                                        <th>Client</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            Copyright © 2020 National Circuit Assembly. All rights reserved.
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!------------------------------------------------------->
        <!----            END MAIN PAGE AREA                ----->
        <!------------------------------------------------------->
        
        <script>
            $( document ).ready(function() {
                $( "#dialog-confirm" ).dialog({   //DELETE MODAL
                    autoOpen: false
                });

                function updateItem() {
                    var valid = true;
                    
                    if ( valid ) {
                        var bomid    = $.trim($('#id').val());
                        var wonum = $.trim($('#wonum').val());
                        var mfgpartnum = $.trim($('#mfgpartnum').val());
                        var ncapartnum = $.trim($('#ncapartnum').val());
                        var datecode = $.trim($('#datecode').val());
                        var lotcode = $.trim($('#lotcode').val());
                        var mfg = $.trim($('#mfg').val());
                        var active = $.trim($('#active').val());
                        var rack = $.trim($('#rack').val());
                        var slot = $.trim($('#slot').val());
                        var side = $.trim($('#side').val());
                        var client = $.trim($('#client').val());
                        var type = $.trim($('#action').val());;
                        
                        alert(type);
                        
                        $.ajax({
                            type: 'POST',
                            url: '../smd/jsaddbomitem.php',
                            data: {
                                bomid        : bomid,
                                wonum       : wonum,
                                mfgpartnum  : mfgpartnum,
                                ncapartnum  : ncapartnum,
                                datecode    : datecode,
                                lotcode     : lotcode,
                                mfg         : mfg,
                                active      : active,
                                rack        : rack,
                                slot        : slot,
                                side        : side,
                                client      : client,
                                type        : type
                            },
                            success: function(data) {
                                //alert('SUCCESSFULLY ADDDED RECORD');
                                if(data){
                                    alert(data);
                                    $('#linetbl').DataTable().ajax.reload();
                                }else{
                                    alert('Item Did Not Post Successfully');
                                }
                                
                            },
                            error: function(data){
                                alert('Data failed to post.');
                            }
                        });


                        $( "#dialog-form" ).dialog( "close" );
                    }
                    //return valid;
                }
                $( function() {
                    $( "#dialog-form" ).dialog({  //ADD/EDIT MODAL
                    autoOpen: false,
                    height: 600,
                    width: 375,
                    dialogClass: "no-close",
                    modal: true,
                    buttons: {
                        "Submit": updateItem,
                        Cancel: function() {
                            $( "#dialog-form" ).dialog( "close" );
                        }
                    },
                    close: function() {
                        form[ 0 ].reset();
                        //allFields.removeClass( "ui-state-error" );
                    }
                    });
                
                    form = $( "#dialog-form" ).find( "form" ).on( "submit", function( event ) {
                        event.preventDefault();
                        //addUser();
                    });
                });
                
                $('#workordernum').focusout(function() {
                    if($.trim($('#workordernum').val()) > ""){
                        
                        $('#linetbl').removeAttr('hidden'); 
                        var workordernum = $.trim($('#workordernum').val());

                        var table = $("#linetbl").DataTable({
                            //destroy: true,
                            deferRender: true,
                            "scrollY"       : "100%",
                            "scrollCollapse": false,
                            "order"         : [[ 2, "desc" ]],
                            "pagingType"    : "first_last_numbers",
                            "pageLength": 20,
                            "ajax" : {
                                "url": "../smd/jsstagingtable.php",
                                "data" : {
                                        "wo": workordernum
                                    },
                                "dataSrc" : "" 
                            },
                            "columns" : [
                                {"data": ""},
                                {"data": "ID"},
                                {"data": "MfgPart"},
                                {"data": "NCAPart"},
                                {"data": "DateCode"},
                                {"data": "LotCode"},
                                {"data": "Manufacturer"},
                                {"data": "WorkOrder"},
                                {"data": "active"},
                                {"data": "rack"},
                                {"data": "slot"},
                                {"data": "side"},
                                {"data": "client"},
                            ],
                            "columnDefs": [
                            {
                                orderable: false,
                                className: 'select-checkbox',
                                targets:   0
                            },
                            {
                                "targets": [2, 3],
                                "width": "20%"
                            },
                            {
                                "targets": [4,5,6],
                                "width": "10%"
                            },
                            {
                                "targets": [1,7,8],
                                "width": "8%"
                            },
                            {
                                "targets": [0,1,9,10,11,12],
                                "width": "5%"
                            }
                            ],
                            rowId: 'ID',
                            select: true,
                            dom: 'Bfrtip',
                            buttons: [
                                {
                                    extend: 'copy',
                                    text: 'Copy',
                                    action: function () {
                                        var row = table.row( { selected: true } ).data();
                                        var id = row.ID;
                                        var mfgprt = row.MfgPart;
                                        var ncaprt = row.NCAPart;
                                        var dc = row.DateCode;
                                        var lc = row.LotCode;
                                        var mfg = row.Manufacturer;
                                        var wo = row.WorkOrder;
                                        var active = row.active;
                                        var rack = row.rack;
                                        var slot = row.slot;
                                        var side = row.side;
                                        var client = row.client;
                                        var action = 'copy';
                                        getinfo(id, mfgprt, ncaprt, dc, lc, mfg, wo, active, rack, slot, side, client, action);
                                       
                                    }
                                },
                                {
                                    text: 'Edit',
                                    action: function () {
                                        var row = table.row( { selected: true } ).data();
                                        var id = row.ID;
                                        var mfgprt = row.MfgPart;
                                        var ncaprt = row.NCAPart;
                                        var dc = row.DateCode;
                                        var lc = row.LotCode;
                                        var mfg = row.Manufacturer;
                                        var wo = row.WorkOrder;
                                        var active = row.active;
                                        var rack = row.rack;
                                        var slot = row.slot;
                                        var side = row.side;
                                        var client = row.client;
                                        var action = 'update';
                                        getinfo(id, mfgprt, ncaprt, dc, lc, mfg, wo, active, rack, slot, side, client, action);
                                       
                                    }
                                },
                                'excel',
                                'csv',
                                {
                                    extend: "pdf",
                                    title: "NCA Staging For Work Order #" + workordernum
                                },
                                {
                                    extend: "print",
                                    title: "NCA Staging For Work Order #" + workordernum,
                                    exportOptions: {
                                        columns: [ 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12 ]
                                    },
                                    customize: function(win)
                                    {
                        
                                        var last = null;
                                        var current = null;
                                        var bod = [];
                        
                                        var css = '@page { size: landscape; }',
                                            head = win.document.head || win.document.getElementsByTagName('head')[0],
                                            style = win.document.createElement('style');
                        
                                        style.type = 'text/css';
                                        style.media = 'print';
                        
                                        if (style.styleSheet)
                                        {
                                        style.styleSheet.cssText = css;
                                        }
                                        else
                                        {
                                        style.appendChild(win.document.createTextNode(css));
                                        }
                        
                                        head.appendChild(style);
                                    }
                                    
                                },
                            ]
                            /*select: {
                                style:    'os',
                                selector: 'td:first-child'
                            },*/
                            
                        });
                        $('.dataTables_length').addClass('bs-select');

                    var wo = $.trim($('#workordernum').val()); 
                    $('#wonum').val(wo);

                    

                    $('#linetbl tbody').on( 'click', 'tr', function () {
                        $(this).toggleClass('selected');
                        var act = $(this).toggleClass('active').hasClass('active');

                        var linedata = table.row( this ).data();
                        var wo = $.trim($('#workordernum').val());
                        var bomid = linedata.ID;
                        var clientid = linedata.Client;
                        var active = linedata.active
                        
                    });

                    function getinfo(id, mfgprt, ncaprt, dc, lc, mfg, wo, active, rack, slot, side, client, action){
                        alert(action);
                        //var count = table.rows( { selected: true } ).count();
                        $('#id').val(id);
                        $('#mfgpartnum').val(mfgprt);
                        $('#ncapartnum').val(ncaprt);
                        $('#datecode').val(dc);
                        $('#lotcode').val(lc);
                        $('#mfg').val(mfg);
                        $('#wonum').val(wo);
                        $('#active').val(active);
                        $('#rack').val(rack);
                        $('#slot').val(slot);
                        $('#side').val(side);
                        $('#client').val(client);
                        $('#action').val(action);
                        $( "#dialog-form" ).dialog( "open" );
                        

                    }

                    function myPush(wo, bomid, clientid, active) {
                        if(!active){
                                active = "active";
                            }else{
                                if(active == "active"){
                                    active = "";
                                }else{
                                    active = active;
                                }
                            }
                        //alert(active);
                        $.ajax({
                            type: 'POST',
                            url: '../smd/jsstaging.php',
                            data: {
                                wo : wo,
                                bomid : bomid,
                                client : clientid,
                                active : active
                            },
                            success: function(data) {
                                //alert('SUCCESSFULLY ADDDED RECORD');
                                if(data.length >0){
                                    alert(data);
                                    table.ajax.reload();
                                }else{
                                    table.ajax.reload();
                                }
                                
                            },
                            error: function(data){
                                alert('Data failed to post.');
                            }
                        });
                    }

                    function updateblock() {
                        
                        $( "#dialog-confirm" ).dialog({
                            resizable: false,
                            height: "auto",
                            width: 400,
                            dialogClass: "no-close",
                            modal: true,
                            autoOpen: false,
                            buttons: {
                                "Remove From Kit": function() {
                                    myPush(wo, bomid, clientid, active);
                                    $( this ).dialog( "close" );
                                    
                                    table.ajax.reload();
                                },
                                Cancel: function() {
                                    $( this ).dialog( "close" );
                                    
                                    table.ajax.reload();
                                }
                            }
                        });
                    }

                }
                
            });

             
            });
        </script>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    </body>
</html>