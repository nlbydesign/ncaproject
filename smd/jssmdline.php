<?php
    session_start();
    require_once('../config.php');

    $wo         = $_POST['wo'];
    $smtline    = $_POST['smtline'];
    $serialnum  = $_POST['serialnum'];
    $side       = $_POST['side'];
    $uid        =$_SESSION['userlogin'];
    $active     = "active";


    $sql = "SELECT * FROM bom WHERE workordernum = $wo AND side = '".$side."' AND active = '".$active."' ORDER BY mfgpartnum DESC";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();
    $table_data = array();
    if($stmtselect->rowCount() > 0){
        while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) {
            $table_data[] = array(
                'ID' => $rowitems['bomid'],
                'MfgPart'  => $rowitems['mfgpartnum'],
                'NCAPart'   => $rowitems['ncapartnum'],
                'DateCode'   => $rowitems['datecode'],
                'LotCode'   => $rowitems['lotcode'],
                'Mfg'    => $rowitems['mfg'],
                'Rack'   => $rowitems['rack'],
                'Slot'   => $rowitems['slot'],
            );
        }
    }
    $partsdata = json_encode($table_data);

    $insertsql = "INSERT INTO smt_lines (smtline, side, workordernum, serialnumber, components, userid) VALUES (?, ?, ?, ?, ?, ?)";
    $stmt= $db->prepare($insertsql);
    $stmt->execute([$smtline, $side, $wo, $serialnum, $partsdata, $uid]);
    
?>
