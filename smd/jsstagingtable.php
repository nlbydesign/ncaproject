<?php
    session_start();
    require_once('../config.php');

    $checkval = $_GET['wo']; 

    $sql = "SELECT * FROM bom WHERE workordernum = $checkval ORDER BY datecreated DESC";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();
    $table_data = array();
    if($stmtselect->rowCount() > 0){
        while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) {
            $table_data[] = array(
                '' => '',
                'ID' => $rowitems['bomid'],
                'MfgPart'  => $rowitems['mfgpartnum'],
                'NCAPart'   => $rowitems['ncapartnum'],
                'DateCode'   => $rowitems['datecode'],
                'LotCode'   => $rowitems['lotcode'],
                'Manufacturer'   => $rowitems['mfg'],
                'WorkOrder'   => $rowitems['workordernum'],
                'active' => $rowitems['active'],
                'rack' => $rowitems['rack'],
                'slot' => $rowitems['slot'],
                'side' => $rowitems['side'],
                'client' => $rowitems['client'],
            );
        }
    }
    echo json_encode($table_data);
?>
