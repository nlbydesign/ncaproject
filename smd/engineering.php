<?php
    session_start();
    require_once('../getdata.php');
    require_once('../header.php');

    if(!isset($_SESSION['userlogin'])){
        header("Location: ../login.php");
    }

?>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NCA - SMD LINE DATA AQUISITION</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/normalize.css">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" async defer></script>
        
        <script src="https://kit.fontawesome.com/1e6ad500ad.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="../assets/vendor/fonts/circular-std/style.css" >
        <link rel="stylesheet" href="../assets/libs/css/style.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="../assets/vendor/vector-map/jqvmap.css">
        <link rel="stylesheet" href="../assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link href="../DataTables/datatables.min.css" rel="stylesheet">
        <script type="text/javascript" src="../DataTables/datatables.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script>
            

        </script>
    </head>
    <body>
        <?php header2(); ?>
        <!------------------------------------------------------->
        <!----              MAIN PAGE AREA                  ----->
        <!------------------------------------------------------->
        <div id="page-container">
            <div id="content-wrap" class="container-fluid batinspect">
                <div class="d-flex" style="margin-top:75px;"></div>
                <!-- DELETE MODAL -->
                <div id="dialog-confirm" title="REMOVE FROM KIT?" style="display:none;">
                    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span></p>
                </div>
                <!-------END MODAL--------------->
                
                <!--<form id="intakeform" autocomplete="off" style="margin-bottom: 20px;">-->
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-10">
                            <div class="row justify-content-center">
                                <h1>ENGINEERING</h1>
                            </div>
                            <div class="row justify-content-center">
                                
                                <div class="col-md-3 sectiontype">
                                    <div class="row">
                                        <div class="col-md-12 sectionhdr">
                                            <label for="workordernum" class="d-flex justify-content-center inputlabel sectionhdrlabel">Work Order Number</label>
                                        </div>
                                    </div>    
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-md-12 d-flex justify-content-center">
                                            <input type="text" name="workordernum" id="workordernum" class="form-control input_user serialnuminput">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 sectiontype">
                                    <div class="row">
                                        <div class="col-md-12 sectionhdr">
                                            <label for="side" class="d-flex justify-content-center inputlabel sectionhdrlabel">Side</label>
                                        </div>
                                    </div>    
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-md-12 d-flex justify-content-center">
                                        <select id="side" style="margin-top:8px;"> 
                                            <option  value="">--Select--</option>
                                            <option  value="Top">Top</option>
                                            <option  value="Bottom">Bottom</option>
                                            <option  value="Both">Top/Bottom</option>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!--</form>   -->
                <div class="row d-flex justify-content-center">
                    <div class="col-md-10">
                        <div id="shippedunits" class="table-responsive table-wrapper-scroll-y my-custom-scrollbar">
                            <table id="linetbl" class="table table-striped table-bordered" cellspacing="0" width="100%" hidden>
                                <thead>
                                    <tr>
                                        <th class="th-sm">ID</th>
                                        <th class="th-sm">Mfg Part #</th>
                                        <th class="th-sm">NCA Part #</th>
                                        <th class="th-sm">Date Code</th>
                                        <th class="th-sm">Lot Code</th>
                                        <th class="th-sm">Manufacturer</th>
                                        <th class="th-sm">Work Order</th>
                                        <th class="th-sm">Client</th>
                                        <th class="th-sm">Board Side</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Mfg Part #</th>
                                        <th>NCA Part #</th>
                                        <th>Date Code</th>
                                        <th>Lot Code</th>
                                        <th>Manufacturer</th>
                                        <th>Work Order</th>
                                        <th>Client</th>
                                        <th>Board Side</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            Copyright © 2020 National Circuit Assembly. All rights reserved.
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!------------------------------------------------------->
        <!----            END MAIN PAGE AREA                ----->
        <!------------------------------------------------------->
        
        <script>
            $( document ).ready(function() {
                $( "#dialog-confirm" ).dialog({
                    autoOpen: false
                });

                $( "#side" ).change(function() {
                    $('#linetbl').DataTable().ajax.reload();
                });

                $('#workordernum').focusout(function() {
                    if($.trim($('#workordernum').val()) > ""){
                        
                        $('#linetbl').removeAttr('hidden'); 
                        var workordernum = $.trim($('#workordernum').val());
                        
                        $("#linetbl").DataTable({
                            destroy: true,
                            "scrollY"       : "800px",
                            "scrollCollapse": true,
                            "order"         : [[ 1, "desc" ], [ 3, "desc"]],
                            "pagingType"    : "first_last_numbers",
                            "pageLength": 25,
                            "ajax" : {
                                "url": "../smd/jsengineeringtable.php",
                                "data" : {
                                        "wo": workordernum
                                    },
                                "dataSrc" : "" 
                            },
                            "columns" : [
                                {"data": "ID"},
                                {"data": "MfgPart"},
                                {"data": "NCAPart"},
                                {"data": "DateCode"},
                                {"data": "LotCode"},
                                {"data": "Manufacturer"},
                                {"data": "WorkOrder"},
                                {"data": "Client"},
                                {"data": "side"}
                            ],
                            /*"columnDefs": [
                            {
                                "targets": [ 7, 8 ],
                                "visible": false,
                                "searchable": false,
                            },
                            {
                                "targets": [1,2,3,4,5],
                                "width": "8%"
                            },
                            {
                                "targets": [0,6,9],
                                "width": "4%"
                            }
                            ]*/
                        });
                        $('.dataTables_length').addClass('bs-select');

                    var wo = $.trim($('#workordernum').val()); 
                    $('#wonum').val(wo);

                    $('#linetbl tbody').on( 'click', 'tr', function () {
                        var table = $('#linetbl').DataTable();
                        $(this).toggleClass('selected');
                        var act = $(this).toggleClass('active').hasClass('active');
                        
                        var linedata = table.row( this ).data();
                        var wo = $.trim($('#workordernum').val());
                        var side = $.trim($('#side').val());
                        var bomid = linedata.ID;
                        var client = linedata.Client;
                        var sideval = linedata.side;
                        //alert("this " + bomid);
                        
                        $( function() {
                            $( "#dialog-confirm" ).dialog({
                                resizable: false,
                                height: "auto",
                                width: 400,
                                dialogClass: "no-close",
                                modal: true,
                                autoOpen: false,
                                buttons: {
                                    "Change": function() {
                                        myPush(wo, side, bomid, client, sideval);
                                        $( this ).dialog( "close" );
                                        $('#linetbl').DataTable().ajax.reload();
                                    },
                                    Cancel: function() {
                                        $( this ).dialog( "close" );
                                    }
                                }
                            });
                            
                        });
                        
                        if(act){
                            if(!sideval){
                                myPush( wo, side, bomid, client, sideval);
                                $('#linetbl').DataTable().ajax.reload();
                            }else {
                                var inputtext = "You are removing this item from the " + sideval + " side kit. Are you sure?"
                                $( "#dialog-confirm p").text(inputtext);
                                $( "#dialog-confirm" ).dialog("open");
                            }
                        }else{
                            $( "#dialog-confirm" ).dialog("open");
                        }
                        
                        
                        function myPush(wo, side, bomid, client, sideval) {
                            //alert("sideval = " + sideval + " and side = " + side)
                            if(!sideval){
                                side = side;
                            }else{
                                if(sideval == side){
                                    side = "";
                                }else{
                                    side = side;
                                }
                            }
                            $.ajax({
                                type: 'POST',
                                url: '../smd/jsengineering.php',
                                data: {
                                    wo : wo,
                                    side : side,
                                    bomid : bomid,
                                    client : client
                                },
                                success: function(data) {
                                    //alert('SUCCESSFULLY ADDDED RECORD');
                                    if(data.length >0){
                                        //alert(data);
                                    }else{
                                    }
                                    
                                },
                                error: function(data){
                                    alert('Data failed to post.');
                                }
                            });
                            
                        }
                    });

                }
                
            });

             
            });
        </script>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    </body>
</html>