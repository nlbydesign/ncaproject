<?php
    session_start();
    require_once('../config.php');

    $checkval = $_GET['wo']; 
    $checkside = $_GET['side'];

    $sql = "SELECT * FROM bom WHERE workordernum = $checkval AND side = '".$checkside."' OR side = 'Both' ORDER BY mfgpartnum DESC";
    $stmtselect = $db->prepare($sql);
    $result = $stmtselect->execute();
    $table_data = array();
    if($stmtselect->rowCount() > 0){
        while ( $rowitems = $stmtselect->fetch(PDO::FETCH_ASSOC)) {
            $table_data[] = array(
                'ID' => $rowitems['bomid'],
                'MfgPart'  => $rowitems['mfgpartnum'],
                'NCAPart'   => $rowitems['ncapartnum'],
                'DateCode'   => $rowitems['datecode'],
                'LotCode'   => $rowitems['lotcode'],
                'Active'    => $rowitems['active'],
                'rack'    => $rowitems['rack'],
                'slot'    => $rowitems['slot'],
                'side'    => $rowitems['side'],
            );
        }
    }
    echo json_encode($table_data);
?>