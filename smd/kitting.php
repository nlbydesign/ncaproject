<?php
    session_start();
    require_once('../getdata.php');
    require_once('../header.php');

    if(!isset($_SESSION['userlogin'])){
        header("Location: ../login.php");
    }

?>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NCA - SMD LINE DATA AQUISITION</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/normalize.css">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" async defer></script>
        
        <script src="https://kit.fontawesome.com/1e6ad500ad.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="../assets/vendor/fonts/circular-std/style.css" >
        <link rel="stylesheet" href="../assets/libs/css/style.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="../assets/vendor/vector-map/jqvmap.css">
        <link rel="stylesheet" href="../assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="../assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link href="../DataTables/datatables.min.css" rel="stylesheet">
        <script type="text/javascript" src="../DataTables/datatables.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script>
            

        </script>
    </head>
    <body>
        <?php header2(); ?>
        <!------------------------------------------------------->
        <!----              MAIN PAGE AREA                  ----->
        <!------------------------------------------------------->
        <div id="page-container">
            <div id="content-wrap" class="container-fluid batinspect">
                <div class="d-flex" style="margin-top:75px;"></div>
                <!-- DELETE MODAL -->
                <div id="dialog-confirm" title="REMOVE FROM LOT?" style="display:none;">
                    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span></p>
                </div>
                <!-------END MODAL--------------->
                <!--- ADD ITEM MODAL ------------>
                <div id="dialog-form" title="Add New Item">
                    <p class="validateTips">All form fields are required.</p>
                    
                    <form>
                        <fieldset>
                        <table style="width:100%;">
                            <tr>
                                <td style="width:30%;">
                                    <label for="wonum">Work Order #</label>
                                </td>
                                <td style="width:70%;">
                                    <input type="text" name="wonum" id="wonum" class="text ui-widget-content ui-corner-all" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;">
                                    <label for="mfgpartnum">Mfg Part #</label>
                                </td>
                                <td style="width:70%;">
                                    <input type="text" name="mfgpartnum" id="mfgpartnum" class="text ui-widget-content ui-corner-all" >
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;">
                                    <label for="ncapartnum">NCA Part #</label>
                                </td>
                                <td style="width:70%;">
                                    <input type="text" name="ncapartnum" id="ncapartnum" class="text ui-widget-content ui-corner-all">
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;">
                                    <label for="datecode">Date Code</label>
                                </td>
                                <td style="width:70%;">
                                    <input type="text" name="datecode" id="datecode" class="text ui-widget-content ui-corner-all">
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;">
                                    <label for="lotcode">Lot Code</label>
                                </td>
                                <td style="width:70%;">
                                    <input type="text" name="lotcode" id="lotcode" class="text ui-widget-content ui-corner-all">
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;">
                                    <label for="mfg">Manufacturer</label>
                                </td>
                                <td style="width:70%;">
                                    <input type="text" name="mfg" id="mfg" class="text ui-widget-content ui-corner-all">
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;">
                                    <label for="client">Client</label>
                                </td>
                                <td style="width:70%;">
                                    <?php getclientlist(''); ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;">
                                    <label for="kit">Kit Item?</label>
                                </td>
                                <td style="width:70%;">
                                    <select id="kit" style="margin-top:10px;"> 
                                        <option  value="">--Select--</option>
                                        <option  value="Yes">Yes</option>
                                        <option  value="No">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr hidden>
                                <td style="width:30%;">
                                    <label for="action">action</label>
                                </td>
                                <td style="width:70%;">
                                <input type="text" name="action" id="action" class="text ui-widget-content ui-corner-all">
                                </td>
                            </tr>
                        </table>
                        
                        <!-- Allow form submission with keyboard without duplicating the dialog button -->
                        <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                        </fieldset>
                    </form>
                </div>
                <!------- END MODAL ------------->
                <!--<form id="intakeform" autocomplete="off" style="margin-bottom: 20px;">-->
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-10">
                            <div class="row justify-content-center">
                                <h1>KITTING</h1>
                            </div>
                            <div class="row justify-content-center">
                                
                                <div class="col-md-3 sectiontype">
                                    <div class="row">
                                        <div class="col-md-12 sectionhdr">
                                            <label for="workordernum" class="d-flex justify-content-center inputlabel sectionhdrlabel">Work Order Number</label>
                                        </div>
                                    </div>    
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-md-12 d-flex justify-content-center">
                                            <input type="text" name="workordernum" id="workordernum" class="form-control input_user serialnuminput">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-2 sectiontype">
                                    <div class="row">
                                        <div class="col-md-12 sectionhdr">
                                            <label for="side" class="d-flex justify-content-center inputlabel sectionhdrlabel">Add Item</label>
                                        </div>
                                    </div>   
                                    <div class="row">
                                        <div class="col-md-12 d-flex justify-content-center">
                                                <button id='create-item' name='create-item' class="addrecord" ><i class="fas fa-plus mr-2 "></i>ITEM</button>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                <!--</form>   -->
                <div class="row d-flex justify-content-center">
                    <div class="col-md-10">
                        <div id="shippedunits" class="table-responsive table-wrapper-scroll-y my-custom-scrollbar">
                            <table id="linetbl" class="table table-striped table-bordered" cellspacing="0" width="100%" hidden>
                                <thead>
                                    <tr>
                                        <th class="th-sm"></th>
                                        <th class="th-sm">ID</th>
                                        <th class="th-sm">Mfg Part #</th>
                                        <th class="th-sm">NCA Part #</th>
                                        <th class="th-sm">Date Code</th>
                                        <th class="th-sm">Lot Code</th>
                                        <th class="th-sm">Manufacturer</th>
                                        <th class="th-sm">Work Order</th>
                                        <th class="th-sm">Client</th>
                                        <th class="th-sm">Kit Item</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>ID</th>
                                        <th>Mfg Part #</th>
                                        <th>NCA Part #</th>
                                        <th>Date Code</th>
                                        <th>Lot Code</th>
                                        <th>Manufacturer</th>
                                        <th>Work Order</th>
                                        <th>Client</th>
                                        <th>Kit Item</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            Copyright © 2020 National Circuit Assembly. All rights reserved.
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!------------------------------------------------------->
        <!----            END MAIN PAGE AREA                ----->
        <!------------------------------------------------------->
        
        <script>
            $( document ).ready(function() {
                $( "#dialog-confirm" ).dialog({
                    autoOpen: false
                });

                function addItem() {
                    var valid = true;
                
                    if ( valid ) {
                        var wonum = $.trim($('#wonum').val());
                        var mfgpartnum = $.trim($('#mfgpartnum').val());
                        var ncapartnum = $.trim($('#ncapartnum').val());
                        var datecode = $.trim($('#datecode').val());
                        var lotcode = $.trim($('#lotcode').val());
                        var mfg = $.trim($('#mfg').val());
                        var client = $.trim($('#client').val());
                        var kit = $.trim($('#kit').val());
                        var type = $.trim($('#action').val());
                        $.ajax({
                            type: 'POST',
                            url: '../smd/jsaddbomitem.php',
                            data: {
                                wonum : wonum,
                                mfgpartnum : mfgpartnum,
                                ncapartnum : ncapartnum,
                                datecode : datecode,
                                lotcode : lotcode,
                                mfg     : mfg,
                                client  : client,
                                kit     : kit,
                                type    : type
                            },
                            success: function(data) {
                                //alert('SUCCESSFULLY ADDDED RECORD');
                                if(data === "Success"){
                                    //alert('Item Added Successfully');
                                    $('#linetbl').DataTable().ajax.reload();
                                }else{
                                    alert('Item Did Not Post Successfully');
                                }
                                
                            },
                            error: function(data){
                                alert('Data failed to post.');
                            }
                        });


                        $( "#dialog-form" ).dialog( "close" );
                    }
                    //return valid;
                }
                $( function() {
                    $( "#dialog-form" ).dialog({
                    autoOpen: false,
                    height: 450,
                    width: 375,
                    dialogClass: "no-close",
                    modal: true,
                    buttons: {
                        "Add Item": addItem,
                        Cancel: function() {
                            $( "#dialog-form" ).dialog( "close" );
                        }
                    },
                    close: function() {
                        form[ 0 ].reset();
                        //allFields.removeClass( "ui-state-error" );
                    }
                    });
                
                    form = $( "#dialog-form" ).find( "form" ).on( "submit", function( event ) {
                        event.preventDefault();
                        //addUser();
                    });
                });
                $( "#create-item" ).button().on( "click", function() {
                    var workordernum = $.trim($('#workordernum').val());
                    if( !workordernum ) {
                        alert('Please Enter a Work Order Number.');
                    }else{
                        
                        $( "#dialog-form" ).dialog( "open" );
                    }
                });


                $('#workordernum').focusout(function() {
                    if($.trim($('#workordernum').val()) > ""){
                        
                        $('#linetbl').removeAttr('hidden'); 
                        var workordernum = $.trim($('#workordernum').val());

                        var table = $("#linetbl").DataTable({
                            destroy: true,
                            deferRender: true,
                            "scrollY"       : "1000px",
                            "scrollCollapse": true,
                            "order"         : [[ 2, "desc" ]],
                            "pagingType"    : "first_last_numbers",
                            "pageLength": 20,
                            "ajax" : {
                                "url": "../smd/jskittingtable.php",
                                "data" : {
                                        "wo": workordernum
                                    },
                                "dataSrc" : "" 
                            },
                            "columns" : [
                                {"data": ""},
                                {"data": "ID"},
                                {"data": "MfgPart"},
                                {"data": "NCAPart"},
                                {"data": "DateCode"},
                                {"data": "LotCode"},
                                {"data": "Manufacturer"},
                                {"data": "WorkOrder"},
                                {"data": "Client"},
                                {"data": "kit"}
                            ],
                            "columnDefs": [
                            {
                                orderable: false,
                                className: 'select-checkbox',
                                targets:   0
                            },
                            {
                                "targets": [ 8 ],
                                "visible": false,
                                "searchable": false,
                            },
                            {
                                "targets": [2, 3],
                                "width": "20%"
                            },
                            {
                                "targets": [4,5,6],
                                "width": "10%"
                            },
                            {
                                "targets": [1,7],
                                "width": "8%"
                            },
                            {
                                "targets": [0],
                                "width": "5%"
                            }
                            ],
                            rowId: 'ID',
                            select: true,
                            dom: 'Bfrtip',
                            buttons: [
                                {
                                    extend: 'copy',
                                    text: 'Copy',
                                    action: function () {
                                        var row = table.row( { selected: true } ).data();
                                        var mfgprt = row.MfgPart;
                                        var ncaprt = row.NCAPart;
                                        var dc = row.DateCode;
                                        var lc = row.LotCode;
                                        var mfg = row.Manufacturer;
                                        var wo = row.WorkOrder;
                                        var client = row.Client;
                                        var kit = row.kit;
                                        var action = 'add';
                                        getinfo(mfgprt, ncaprt, dc, lc, mfg, wo, client, kit, action);
                                       
                                    }
                                },
                                'excel',
                                'csv',
                                {
                                    extend: "pdf",
                                    title: "NCA Kitting For Work Order #" + workordernum
                                },
                                {
                                    extend: "print",
                                    title: "NCA Kitting For Work Order #" + workordernum,
                                    exportOptions: {
                                        columns: [ 1, 2, 3, 4, 5, 6, 7, 9 ]
                                    },
                                    customize: function(win)
                                    {
                                        var last = null;
                                        var current = null;
                                        var bod = [];
                        
                                        var css = '@page { size: landscape; }',
                                            head = win.document.head || win.document.getElementsByTagName('head')[0],
                                            style = win.document.createElement('style');
                        
                                        style.type = 'text/css';
                                        style.media = 'print';
                        
                                        if (style.styleSheet)
                                        {
                                        style.styleSheet.cssText = css;
                                        }
                                        else
                                        {
                                        style.appendChild(win.document.createTextNode(css));
                                        }
                        
                                        head.appendChild(style);
                                    }
                                },
                            ]
                            /*select: {
                                style:    'os',
                                selector: 'td:first-child'
                            },*/
                            
                        });
                        $('.dataTables_length').addClass('bs-select');

                    var wo = $.trim($('#workordernum').val()); 
                    $('#wonum').val(wo);

                    $('#linetbl tbody').on( 'click', 'tr', function () {
                        $(this).toggleClass('selected');
                        var act = $(this).toggleClass('active').hasClass('active');

                        var linedata = table.row( this ).data();
                        var wo = $.trim($('#workordernum').val());
                        var bomid = linedata.ID;
                        var clientid = linedata.Client;
                        var active = linedata.kit;

                        $( function() {
                            $( "#dialog-confirm" ).dialog({
                                resizable: false,
                                height: "auto",
                                width: 400,
                                dialogClass: "no-close",
                                modal: true,
                                autoOpen: false,
                                buttons: {
                                    "Remove From Kit": function() {
                                        myPush(wo, bomid, clientid, active);
                                        $( this ).dialog( "close" );
                                        table.rows('.selected').deselect();
                                        table.ajax.reload();
                                    },
                                    Cancel: function() {
                                        $( this ).dialog( "close" );
                                        table.rows('.selected').deselect();
                                        table.ajax.reload();
                                    }
                                }
                            });
                            
                        });
                        
                        if(act){
                            
                            if(!active && table.row( { selected: true } )){
                                
                                myPush( wo, bomid, clientid, active);
                                
                            }else{
                                var inputtext = "You are removing this item from the Kit. Are you sure?"
                                $( "#dialog-confirm p").text(inputtext);
                                $( "#dialog-confirm" ).dialog("open");
                                table.ajax.reload();
                            }
                        }else{
                            //$( "#dialog-confirm" ).dialog("open");
                        }
                    });
                            
                    function getinfo(mfgprt, ncaprt, dc, lc, mfg, wo, client, kit, action){
                        //alert('hello');
                        //var count = table.rows( { selected: true } ).count();
                        //alert(kit);
                        $('#mfgpartnum').val(mfgprt);
                        $('#ncapartnum').val(ncaprt);
                        $('#datecode').val(dc);
                        $('#lotcode').val(lc);
                        $('#mfg').val(mfg);
                        $('#wonum').val(wo);
                        $('#client').val(client);
                        $('#kit').val(kit);
                        $('#mfgpartnum').removeAttr('disabled'); 
                        $('#ncapartnum').removeAttr('disabled'); 
                        $('#mfg').removeAttr('disabled'); 
                        $('#client').removeAttr('disabled');
                        $('#action').val(action);
                        $( "#dialog-form" ).dialog( "open" );
                    }

                    function myPush(wo, bomid, clientid, active) {
                        if(!active){
                                active = "Yes";
                            }else{
                                if(active == "Yes"){
                                    active = "";
                                }else{
                                    active = active;
                                }
                            }
                        //alert(active);
                        $.ajax({
                            type: 'POST',
                            url: '../smd/jskitting.php',
                            data: {
                                wo : wo,
                                bomid : bomid,
                                client : clientid,
                                active : active
                            },
                            success: function(data) {
                                //alert('SUCCESSFULLY ADDDED RECORD');
                                if(data.length >0){
                                    alert(data);
                                    table.ajax.reload();
                                }else{
                                    //alert('fail');
                                    table.ajax.reload();
                                }
                            },
                            error: function(data){
                                alert('Data failed to post.');
                            }
                        });
                    }

                }
                
            });

            });
        </script>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    </body>
</html>