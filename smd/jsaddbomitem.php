<?php
    session_start();
    require_once('../config.php');
    
    $wonum      = $_POST['wonum'];
    $mfgpartnum = $_POST['mfgpartnum'];
    $ncapartnum = $_POST['ncapartnum'];
    $datecode   = $_POST['datecode'];
    $lotcode    = $_POST['lotcode'];
    $mfg        = $_POST['mfg'];
    $client     = $_POST['client'];
    $kit        = $_POST['kit'];
    $type       = $_POST['type'];
    $uid        =$_SESSION['userlogin'];

    if($type === 'update' || $type === 'copy'){
        $bomid  = $_POST['bomid'];
        $active = $_POST['active'];
        $rack   = $_POST['rack'];
        $slot   = $_POST['slot'];
        $side   = $_POST['side'];
        
    }

    if($type === 'update'){
        $sql = "UPDATE bom SET workordernum = '".$wonum."', mfgpartnum = '".$mfgpartnum."', ncapartnum = '".$ncapartnum."', datecode = '".$datecode."', lotcode = '".$lotcode."', mfg = '".$mfg."', client = '".$client."', active = '".$active."', rack = '".$rack."', slot = '".$slot."', side = '".$side."', userid = '".$uid."' WHERE bomid = '".$bomid."'";
        $stmt= $db->prepare($sql);
        $stmt->execute();

    }else if($type === 'copy'){
        $insertsql = "INSERT INTO bom (workordernum, mfgpartnum, ncapartnum, datecode, lotcode, mfg, client, active, rack, slot, side, userid) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt= $db->prepare($insertsql);
        $stmt->execute([$wonum, $mfgpartnum, $ncapartnum, $datecode, $lotcode, $mfg, $client, $active, $rack, $slot, $side, $uid]);
      
    }else if($type === 'add'){
        $insertsql = "INSERT INTO bom (workordernum, mfgpartnum, ncapartnum, datecode, lotcode, mfg, client, kit, userid) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt= $db->prepare($insertsql);
        $stmt->execute([$wonum, $mfgpartnum, $ncapartnum, $datecode, $lotcode, $mfg, $client, $kit, $uid]);
      
    }
   
    echo("Success");
?>