<?php
//include database configuration file
require_once('config.php');

$startdate = strtotime($_GET['sdate']);
$enddate = strtotime($_GET['edate']);
$tablename = $_GET['gettablename'];
$type = $_GET['downloadtype'];

$sdate = date('Y-m-d', $startdate);
$edate = date('Y-m-d', $enddate);
//get records from database
if($type === "all"){
    if ($tablename == 'shippingdata'){
        $query = $db->query("SELECT * FROM ".$tablename." WHERE shippingdate BETWEEN '".$sdate." 00:00:00' AND '".$edate." 23:59:59' ORDER BY shippingdate ASC");
    }else{
        $query = $db->query("SELECT * FROM ".$tablename." WHERE datescanned BETWEEN '".$sdate." 00:00:00' AND '".$edate." 23:59:59' ORDER BY datescanned ASC");
    }
}else{
    if ($tablename == 'shippingdata'){
        //$query = $db->query("SELECT * FROM ".$tablename." WHERE shippingdate BETWEEN '".$sdate." 00:00:00' AND '".$edate." 23:59:59' ORDER BY datescanned ASC");
        $query = $db->query("SELECT ".$tablename.".* FROM (SELECT serialnumber, MAX(datescanned) AS datescanned FROM ".$tablename." WHERE shippingdate BETWEEN '".$sdate." 00:00:00' AND '".$edate." 23:59:59'  GROUP BY serialnumber) AS latest_orders INNER JOIN ".$tablename." ON ".$tablename.".serialnumber = latest_orders.serialnumber AND ".$tablename.".datescanned = latest_orders.datescanned ORDER BY shippingdate ASC");
    }else{
        //$query = $db->query("SELECT * FROM ".$tablename." WHERE datescanned BETWEEN '".$sdate." 00:00:00' AND '".$edate." 23:59:59' ORDER BY datescanned ASC");
        $query = $db->query("SELECT ".$tablename.".* FROM (SELECT serialnum, MAX(datescanned) AS datescanned FROM ".$tablename." WHERE datescanned BETWEEN '".$sdate." 00:00:00' AND '".$edate." 23:59:59'  GROUP BY serialnum) AS latest_orders INNER JOIN ".$tablename." ON ".$tablename.".serialnum = latest_orders.serialnum AND ".$tablename.".datescanned = latest_orders.datescanned");
    }
}
$rows = $query->rowCount();
if($query->rowCount() > 0){
    $delimiter = ",";

    if ($tablename == "battery_inspection") {

        $filename = $tablename."_" . date('Y-m-d') . ".csv";
        
        //create a file pointer
        $f = fopen('php://memory', 'w');
        
        //set column headers
        $fields = array('RowID', 'DateInspected', 'UserID', 'SerialNum', 'BatteryStatus', 'BatteryType', 'DateCode', 'ReplacementDateCode','Client');
        fputcsv($f, $fields, $delimiter);
        
        //output each row of the data, format line as csv and write to file pointer
    
        while($row = $query->fetch(PDO::FETCH_ASSOC)){
            //$status = ($row['status'] == '1')?'Active':'Inactive';
            $lineData = array($row['id'], $row['datescanned'], $row['userid'], $row['serialnum'], $row['batstatus'], $row['batterytype'], $row['datecode'], $row['rpmtdatecode'], $row['client']);
            fputcsv($f, $lineData, $delimiter);
        }


    }elseif ($tablename == "battery_triage") {

        $filename = $tablename."_" . date('Y-m-d') . ".csv";
        
        //create a file pointer
        $f = fopen('php://memory', 'w');
        
        //set column headers
        $fields = array('RowID', 'User ID', 'SerialNum', 'Status', 'Date Inspected', 'Client');
        fputcsv($f, $fields, $delimiter);
        
        //output each row of the data, format line as csv and write to file pointer
    
        while($row = $query->fetch(PDO::FETCH_ASSOC)){
            //$status = ($row['status'] == '1')?'Active':'Inactive';
            $lineData = array($row['triageid'], $row['userid'], $row['serialnum'], $row['status'], $row['datescanned'], $row['client']);
            fputcsv($f, $lineData, $delimiter);
        }
    
    }elseif ($tablename == "shippingdata") {

        $filename = $tablename."_" . date('Y-m-d') . ".csv";
        
        //create a file pointer
        $f = fopen('php://memory', 'w');
        
        //set column headers
        $fields = array('RowID', 'Work Order', 'Date Scanned', 'Ship Date', 'Serial Number', 'Client', 'User');
        fputcsv($f, $fields, $delimiter);
        
        //output each row of the data, format line as csv and write to file pointer
    
        while($row = $query->fetch(PDO::FETCH_ASSOC)){
            //$status = ($row['status'] == '1')?'Active':'Inactive';
            $lineData = array($row['idshippingdata'], $row['wo'], $row['datescanned'], $row['shippingdate'], $row['serialnumber'], $row['client'], $row['userid']);
            fputcsv($f, $lineData, $delimiter);
        }
    
    }elseif ($tablename == "rmaintake") {

        $filename = $tablename."_" . date('Y-m-d') . ".csv";
        
        //create a file pointer
        $f = fopen('php://memory', 'w');
        
        //set column headers
        $fields = array('RowID', 'Date Scanned', 'Ship Date', 'RMA Request Date', 'User ID', 'Serial Number', 'Status', 'Client');
        fputcsv($f, $fields, $delimiter);
        
        //output each row of the data, format line as csv and write to file pointer
    
        while($row = $query->fetch(PDO::FETCH_ASSOC)){
            //$status = ($row['status'] == '1')?'Active':'Inactive';
            $lineData = array($row['id'], $row['datescanned'], $row['shipdate'], $row['rmarequestdate'], $row['userid'], $row['serialnum'], $row['status'], $row['client']);
            fputcsv($f, $lineData, $delimiter);
        }
    
    }elseif ($tablename == "unit_r121") {

        $filename = $tablename."_" . date('Y-m-d') . ".csv";
        
        //create a file pointer
        $f = fopen('php://memory', 'w');
        
        //set column headers
        $fields = array('RowID', 'Date Scanned', 'User ID', 'Serial Number', 'Status', 'Client');
        fputcsv($f, $fields, $delimiter);
        
        //output each row of the data, format line as csv and write to file pointer
    
        while($row = $query->fetch(PDO::FETCH_ASSOC)){
            //$status = ($row['status'] == '1')?'Active':'Inactive';
            $lineData = array($row['id'], $row['datescanned'], $row['userid'], $row['serialnum'], $row['status'], $row['client']);
            fputcsv($f, $lineData, $delimiter);
        }
    
    }


    //move back to beginning of file
    fseek($f, 0);

    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="'.$filename.'";');
    
    //output all remaining data on a file pointer
    fpassthru($f);
    
}
exit;
?>