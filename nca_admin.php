<?php
    session_start();
    require_once('getdata.php');
    require_once('header.php');

    if(!isset($_SESSION['userlogin'])){
        header("Location: login.php");
    }

?>
    <!doctype html>
    <html lang="en">


    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/vendor/fonts/circular-std/style.css" >
        <link rel="stylesheet" href="assets/libs/css/style.css">
        <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="assets/vendor/vector-map/jqvmap.css">
        <link rel="stylesheet" href="assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
        
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
        <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
        <!-- <script type="text/javascript" src="DataTables/datatables.min.js"></script> -->
        <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script src = "https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css">
     
        
        <title>National Circuit Assembly Dashboard</title>
    </head>

    <body>
        <!-- ============================================================== -->
        <!-- main wrapper -->
        <!-- ============================================================== -->
        <div class="dashboard-main-wrapper">
            <!-- ============================================================== -->
            <!-- navbar -->
            <!-- ============================================================== -->
            <?php header1(); ?>
            <!-- ============================================================== -->
            <!-- end navbar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- left sidebar -->
            <!-- ============================================================== -->
            <?php leftnav(); ?>
            <!-- ============================================================== -->
            <!-- end left sidebar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- wrapper  -->
            <!-- ============================================================== -->
            <div class="dashboard-wrapper">
                <div class="container-fluid  dashboard-content">
                    <!-- ============================================================== -->
                    <!-- pagehader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h3 class="mb-2">Admin Dashboard</h3>
                                <p class="pageheader-text"></p>
                                <div class="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Admin</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- pagehader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <!-- metric -->
                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="text-muted">Add A New Client</h5>
                                    <form id="admindata" autocomplete="off">
                                        
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label for="client">Enter Client Name</label>
                                                <!-- <input type="text" id='client' name='client' class="form-control input-daterange" value="<?php //$results = getDefaultData(); echo $results[0];?>"/> -->
                                                <input type="text" id='client' name='client' class="form-control"/>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class='pgbtn3'>
                                                    <input type="submit" id='submitdata' name='submitdata' class='form-control exprecord' value="Submit Data"/>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                    <!-- ============================================================== -->
                    <!-- footer -->
                    <!-- ============================================================== -->
                    <div class="footer">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    Copyright © 2020 National Circuit Assembly. All rights reserved.
                                </div>
                            
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end footer -->
                    <!-- ============================================================== -->
                </div>
                <!-- ============================================================== -->
                <!-- end wrapper  -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- end main wrapper  -->
            <!-- ============================================================== -->
            <!-- Optional JavaScript -->
            <!-- jquery 3.3.1 js-->
            <!--<script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>-->
            <!-- bootstrap bundle js-->
            <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
            <!-- slimscroll js-->
            <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
            <!-- chartjs js-->
            <script src="assets/vendor/charts/charts-bundle/Chart.bundle.js"></script>

            <!-- main js-->
            <script src="assets/libs/js/main-js.js"></script>
            
            <script type="text/javascript" language="javascript">
                $(document).ready(function() {

                    $('.input-daterange').datepicker({
                        todayBtn:'linked',
                        format: "yyyy-mm-dd",
                        autoclose:true
                    });

                    //fetch_data('no');
      
                    $('#admindata').submit(function(e){
                    
                        e.preventDefault();

                        //var valid = this.form.checkValidity();

                        var client = $.trim($('#client').val());
                        //var creationdate = datetime;

                            
                        $.ajax({
                            type: 'POST',
                            url: 'jsDefaultData.php',
                            data: {
                                client: client
                            },
                            success: function(data) {
                                //alert('SUCCESSFULLY ADDDED RECORD');
                                alert('Data Posted Successfully.');
                                location.reload();
                            },
                            error: function(data){
                                alert('Data failed to post.');
                            }
                        });
                    });        
                });
            </script>
    </body>

    </html>